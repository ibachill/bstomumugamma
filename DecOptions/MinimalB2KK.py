from Gaudi.Configuration import *

from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence, FilterSelection, Selection, CombineSelection
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, PhysConf, TupleToolVeto, FilterInTrees, TupleToolGamma, TupleToolTrackIsolationBDT


#DecayTreeFitter
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple, FilterDecays
from Configurables import LoKi__Hybrid__DTFDict as DTFDict


dv = DaVinci()

if dv.Simulation:
    StrippingStream = "AllStreams"    
else:
    StrippingStream = "BhadronCompleteEvent"



##### Inputs
from StandardParticles import StdLooseKaons as Kaons


Muon_cuts = "(MIPCHI2DV(PRIMARY)>16) & (TRCHI2DOF<3) & (TRGHOSTPROB<0.4) & (MAXTREE(TRGHOSTPROB, HASTRACK) < 0.4) & (P > 3000.0) & (PT > 500.0)"
Comb_cuts = "(AMAXDOCA('')<0.5*mm) & (ADOCACHI2CUT(30, '')) & (AM<7000*MeV) & (AM>3500*MeV) & (APT>300*MeV)"
Mother_cuts = "(VFASPF(VCHI2/VDOF)<15) & (ADMASS('B_s0')<1500*MeV) & (BPVDIRA>0.995) & (PT>350*MeV) & (BPVIPCHI2()<20)"

B2KK = CombineSelection(
    "B2KK",
    [Kaons],
    DecayDescriptor = "B0 -> K+ K-",
    DaughtersCuts = {"K-":		Muon_cuts, 
                     "K+":		Muon_cuts},
    CombinationCut = Comb_cuts,
    MotherCut = Mother_cuts, 
    )

##########################
######### DecayTreeTuple
##########################
Bs0_Tuple = DecayTreeTuple("B2KKTuple")
Bs0_Tuple.Decay = "[B0 -> ^K+ ^K-]CC"
Bs0_Tuple.Inputs = ["Phys/B2KK"]

# Give names to the particles
Bs0_Tuple.addBranches({'B'	: '[B0 -> K+ K-]',
                       'muplus'	: '[B0 -> ^K+ K-]',
                       'muminus': '[B0 -> K+ ^K-]'})


#DecayTreeFitter
DictTuple = Bs0_Tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 20

DictTuple.DTF.constrainToOriginVertex = True

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.DTF.dict.Variables = {
    "DTF_pt"            : "PT",
    "DTF_pe"            : "E",
    "DTF_m"             : "M",
    "DTF_vchi2"		: "BPV(VCHI2)",
    "DTF_vchi2dof"	: "VFASPF(VCHI2/VDOF)",
    "DTF_log_doca"	: "log(DOCA(1,2))",
    "DTF_log_dira"	: "log(1-BPVDIRA)",
    "DTF_t"		: "BPVLTIME()",
    "DTF_t_chi2"	: "BPVLTCHI2()",
    "DTF_M_mumu"	: "MASS(1,2)",
    "DTF_ip"		: "BPVIP()",
    "DTF_ipchi2"	: "BPVIPCHI2()",
    "DTF_ips"		: "sqrt(BPVIPCHI2())",
    "DTF_eta"		: "ETA",
    "DTF_phi"		: "PHI",
    "DTF_muplus_pt"	: "CHILD(PT,1)",
    "DTF_muminus_pt"	: "CHILD(PT,2)",
    "DTF_muplus_pe"	: "CHILD(E,1)",
    "DTF_muminus_pe"	: "CHILD(E,2)",
   }

Bs0_Tuple.ToolList += [
    "TupleToolEventInfo",
    "TupleToolRecoStats"
    ]

BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]


Bs0_Tuple.B.ToolList += BToolList
Bs0_Tuple.muplus.ToolList += MuToolList
Bs0_Tuple.muminus.ToolList += MuToolList



#### MC Truth
MCTruth = Bs0_Tuple.addTupleTool("TupleToolMCTruth")
MCTruth.addTupleTool("MCTupleToolHierarchy")
MCTruth.addTupleTool("MCTupleToolAngles")




###### LoKi Variables
Bs0_LoKi = Bs0_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/Bs0_LoKi')
#Bs0_LoKi.Preambulo = []
Bs0_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    }


LoKi_All = Bs0_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


####### Trigger
TrigVerb = True
TTT = Bs0_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon",
          "L0DiMuon",
          "L0Photon",
          "L0Hadron"
]
Hlt1list = ['Hlt1TrackMVA',
            'Hlt1TwoTrackMVA',
            'Hlt1TrackMVALoose',
            'Hlt1TwoTrackMVALoose',
            'Hlt1TrackMuon',
            'Hlt1TrackMuonMVA',
            'Hlt1DiMuonHighMass',
            'Hlt1DiMuonLowMass',
            'Hlt1SingleMuonHighPT',
            'Hlt1DiMuonNoL0',
            "Hlt1TrackPhoton",
            "Hlt1SingleMuonNoIPD",
]
Hlt2list = ["Hlt2DiMuon",
            "Hlt2DiMuonDY",
            "Hlt2DiMuonZ",
            "Hlt2DiMuonLowMass",
            "Hlt2DiMuonB",
            "Hlt2DiMuonBTurbo",
            "Hlt2DiMuonDetached",
            "Hlt2DiMuonDetachedHeavy",
            "Hlt2DiMuonDetachedJPsi",
            "Hlt2DiMuonDetachedPsi2S",
            "Hlt2DiMuonJPsi",
            "Hlt2DiMuonJPsiHighPT",
            "Hlt2DiMuonJPsiTurbo",
            "Hlt2DiMuonPsi2S",
            "Hlt2DiMuonPsi2SHighPT",
            "Hlt2DiMuonPsi2STurbo",
            "Hlt2DiMuonSoft",
            "Hlt2EWDiMuonDY1",
            "Hlt2EWSingleMuonHighPt",
            "Hlt2EWSingleMuonLowPt",
            "Hlt2EWSingleMuonVHighPt",
            "Hlt2SingleMuon",
            "Hlt2SingleMuonHighPT",
            "Hlt2SingleMuonLowPT",
            "Hlt2SingleMuonRare",
            "Hlt2Topo2Body",
            "Hlt2Topo3Body",
            "Hlt2Topo4Body",
            "Hlt2TopoE2Body",
            "Hlt2TopoE3Body",
            "Hlt2TopoE4Body",
            "Hlt2TopoMu2Body",
            "Hlt2TopoMu3Body",
            "Hlt2TopoMu4Body",
            "Hlt2TopoMuE2Body",
            "Hlt2TopoMuE3Body",
            "Hlt2TopoMuE4Body",
            "Hlt2TopoMuMu2Body",
            "Hlt2TopoMuMu3Body",
            "Hlt2TopoMuMu4Body",
            "Hlt2TopoMuMuDD",
            "Hlt2Bs2PhiGamma",
            "Hlt2RadiativeBs2PhiGammaUnbiased",
            "Hlt2RadiativeIncHHGamma" 
]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList



##########################
######### Sequence & algo
##########################

Bs0_seq = SelectionSequence('Bs0_seq', TopSelection = B2KK)
seq = GaudiSequencer('Bs0_Tuple_seq')

seq.Members += [Bs0_seq.sequence()]
seq.Members += [Bs0_Tuple]

dv.UserAlgorithms += [seq]
