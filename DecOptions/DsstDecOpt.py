from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as Units


##########################
######### CombineParticles
##########################

from GaudiConfUtils import ConfigurableGenerators
from PhysSelPython.Wrappers import SimpleSelection, DataOnDemand, MultiSelectionSequence, SelectionSequence
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, MCTupleToolHierarchy, MCTupleToolAngles, MCTupleToolReconstructed, MCDecayTreeTuple, FitDecayTrees, LoKi__HDRFilter, TupleToolVeto, TupleToolTISTOS, TupleToolGamma
from Configurables import PhysConf
PhysConf().CaloReProcessing = True

Dsplus = DataOnDemand("Phys/StdLooseDsplus2KKPi/Particles")
Gamma = DataOnDemand("Phys/StdLooseAllPhotons/Particles")

dv = DaVinci()

######### Bs0 reconstruction
Dsst = SimpleSelection(
    "Dsst",
    ConfigurableGenerators.CombineParticles,
    [Dsplus, Gamma],
    DecayDescriptor = "[D*_s+ -> D_s+ gamma]cc",
    DaughtersCuts = {"gamma":	"(M < 0.00001*MeV) & (PT > 500*MeV) & (E > 1000*MeV)"},
    CombinationCut =  "ATRUE",
    MotherCut= "(ADMASS('D*_s+') < 500*MeV)" "& mcMatch('[D*_s+]cc')",
    Preambulo = ["from LoKiPhysMC.functions import mcMatch" ],
    )


##########################e
######### DecayTreeTuple
##########################

Dsst_Tuple = DecayTreeTuple("MyDsstr2DsGamma_Tuple")
Dsst_Tuple.Decay = "[D*_s+ -> ^(D_s+ -> K+ K- pi+) ^gamma]CC"
Dsst_Tuple.Inputs = ["Phys/Dsst"]

# Give names to the particles
Dsst_Tuple.addBranches({'D_s_str_plus':	'[D*_s+ -> (D_s+ -> K+ K- pi+) gamma]CC',
                       'D_s_plus':	'[D*_s+ -> ^(D_s+ -> K+ K- pi+) gamma]CC',
                       'gamma':		'[D*_s+ -> (D_s+ -> K+ K- pi+) ^gamma]CC',
                       })

Dsst_Tuple.D_s_str_plus.ToolList+=[
    #    "TupleToolMCBackgroundInfo",
    #    "TupleToolTrackInfo",
    #    "TupleToolRecoStats",
    #    "TupleToolPrimaries",
    #    "TupleToolEventInfo",
    #    "TupleToolConeIsolation",
    #    "TupleToolTrackIsolation",
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]


Dsst_Tuple.D_s_plus.ToolList += [
    "TupleToolTrackInfo",
    #    "TupleToolTrackIsolation",
    #    "TupleToolGeometry",
    #    "TupleToolDira",
    ]


Dsst_Tuple.gamma.ToolList+=[
    "TupleToolTrackIsolation",
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
    "TupleToolVeto/Pi0Veto",
    "TupleToolGamma/GammaMVA",
    ]

##### Gamma Selection
Dsst_Tuple.gamma.addTool(TupleToolVeto,name="Pi0Veto")
Dsst_Tuple.gamma.Pi0Veto.Particle= "gamma"
Dsst_Tuple.gamma.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Dsst_Tuple.gamma.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Dsst_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Dsst_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Dsst_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Dsst_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"


#### MC Truth
MCTruth = Dsst_Tuple.addTupleTool("TupleToolMCTruth")
MCTruth.addTupleTool("MCTupleToolHierarchy")
#MCTruth.addTupleTool("MCTupleToolAngles")
#MCTruth.addTupleTool("MCTupleToolReconstructed")


###### LoKi Variables
Dsst_LoKi = Dsst_Tuple.D_s_str_plus.addTupleTool('LoKi::Hybrid::TupleTool/Dsst_LoKi')
#Dsst_LoKi.Preambulo = []
Dsst_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "m":	"M",
    "pt":	"PT",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_KK":	"CHILD(M, 1)",
    }

#Kplus_LoKi = Dsst_Tuple.Kplus.addTupleTool('LoKi::Hybrid::TupleTool/Kplus_LoKi')
#Kminus_LoKi = Dsst_Tuple.Kminus.addTupleTool('LoKi::Hybrid::TupleTool/Kminus_LoKi')
#KLoKiVar = {
#    "BSKKTRACKPLUSISO" : "RELINFO('/Event/MyStream/Phys/MyLine/MyLocation', 'BSKKTRACKPLUSISO', -1.)"
#    }
#Kplus_LoKi.Variables = KLoKiVar
#Kminus_LoKi.Variables = KLoKiVar

#LoKi_All = Dsst_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
#LoKi_All.Variables = {
#    "ip":	"BPVIP()",
#    "ipchi2":	"BPVIPCHI2()",
#    "ips":	"sqrt(BPVIPCHI2())",
#    "eta":	"ETA",
#    "phi":	"PHI"
#    }
#
######## Refitted Variables
#Dsst_LoKi_DTF = Dsst_Tuple.B_s0.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_DTF')
#Dsst_LoKi_DTF.Variables = {
#    "DTF_m":	"DTF_FUN(M,True,'B_s0')",
#    "DTF_pt":	"DTF_FUN(PT,True,'B_s0')",
#    "DTF_dira":	"DTF_FUN(log(1-BPVDIRA),True,'B_s0')",
#    }                            
#

##########################
######### Sequence & algo
##########################

Dsst_seq = SelectionSequence('Dsst_seq', TopSelection = Dsst)

Sel = MultiSelectionSequence('Dsst_Seq', Sequences = [Dsst_seq])

seq = GaudiSequencer('Dsst_Tuple_seq')
seq.Members += [Sel.sequence()]
seq.Members += [Dsst_Tuple]

