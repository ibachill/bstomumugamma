from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, TupleToolGamma


dv = DaVinci()

StrippingStream = "BhadronCompleteEvent"
StrippingLineName = "Beauty2XGammaExclusiveBs2PhiGammaLine"


#Consider only matching events
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('Stripping"+StrippingLineName+"Decision')"
)
dv.EventPreFilters = fltrs.filters('Filters')


##########################
######### DecayTreeTuple
##########################
Bs0_Tuple = DecayTreeTuple("PhiGamma")
Bs0_Tuple.Decay = "[B_s0 -> ^(phi(1020) -> ^K+ ^K-) ^gamma]CC"
Bs0_Tuple.Inputs = ['/Event/{0}/Phys/{1}/Particles'.format(StrippingStream, StrippingLineName)]

# Give names to the particles
Bs0_Tuple.addBranches({'B'	: '[B_s0 -> (phi(1020) -> K+ K-) gamma]CC',
                       'kplus'	: '[B_s0 -> (phi(1020) -> ^K+ K-) gamma]CC',
                       'kminus'	: '[B_s0 -> (phi(1020) -> K+ ^K-) gamma]CC',
                       'gamma'	: '[B_s0 -> (phi(1020) -> K+ K-) ^gamma]CC'})


Bs0_Tuple.gamma.ToolList += ["TupleToolCaloHypo",
                             "TupleToolGamma/GammaMVA"
                             ]

Bs0_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Bs0_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs0_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Bs0_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"

Bs0_Tuple.gamma.addTupleTool("TupleToolProtoPData")
Bs0_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID" , "CaloNeutralSpd" ]


##########################
######### Sequence
##########################
seq = GaudiSequencer('Bs0_Tuple_seq')
seq.Members += [Bs0_Tuple]
dv.Simulation = False
dv.EvtMax = 1000#-1
dv.DataType = "2017"
dv.InputType = 'DST'
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/DataGPhi_2017_Up.dst'
    ]
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'cond-20170724'
dv.DDDBtag = 'dddb-20170721-3'
dv.TupleFile = "Ntuple_GPhi.root"
dv.UserAlgorithms += [seq]
