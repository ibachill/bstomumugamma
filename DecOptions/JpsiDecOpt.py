from Gaudi.Configuration import *
from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence, CombineSelection, Selection
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, TupleToolVeto, TupleToolTISTOS, TupleToolGamma, TupleToolMuonVariables, TupleToolTrackIsolationBDT, PhysConf, FilterInTrees


dv = DaVinci()

### Should I compute isolation variables?
Isolation = False
### Should I Reprocess Calo data?
CaloData = True
### Should I use B2XMuMu Muons?
B2XMuMu = False


#### Reprocess Calo data
PhysConf().CaloReProcessing = CaloData

##########################
######### Inputs
##########################
from StandardParticles import StdLooseEta2gg as Eta

if B2XMuMu:
    StrippingStream = "AllStreams"
    StrippingLineName = "B2XMuMu_Line"
    B_from_B2XMuMu = DataOnDemand(
        '/Event/{0}/Phys/{1}/Particles'.format(StrippingStream, StrippingLineName)
        )
    Muons_from_B = FilterInTrees('Muons_from_B_filter', Code="('mu+' == ABSID)")
    Muons = Selection("Muons_from_B_sel",
                      Algorithm=Muons_from_B,
                      RequiredSelections=[B_from_B2XMuMu]
                      )
else:
    from StandardParticles import StdLooseMuons as Muons





######### Bs0 -> Jpsi Eta reconstruction
Muon_cuts = "(MIPCHI2DV(PRIMARY)>9) & (TRCHI2DOF<3) & (TRGHOSTPROB<0.4)"
Gamma_cuts = "(PT>500*MeV) & (E>1000*MeV) & (CL>0.2)"
Comb12_cuts = "((AM<3033*MeV) | (AM>3916*MeV)) & (AM<7000*MeV) & (AMAXDOCA('')<0.5*mm)"
Comb_cuts = "(AM<5900*MeV) & (AM>4800*MeV) & (APT>200*MeV)"
Mother_cuts = "(VFASPF(VCHI2/VDOF)<15) & (ADMASS('B_s0')<800*MeV) & (BPVDIRA>0.995) & (PT>350*MeV) & (BPVIPCHI2()<20)"

if dv.Simulation == True:
    Jpsi = CombineSelection(
        "Jpsi",
        [Muons],
        DecayDescriptor = "J/psi(1S) -> mu+ mu-",
        DaughtersCuts = {"mu-":		Muon_cuts + "& (mcMatch('J/psi(1S) => mu+ ^mu-'))",
                         "mu+":		Muon_cuts + "& (mcMatch('J/psi(1S) => ^mu+ mu-'))"},
        CombinationCut = "(AM<6000*MeV) & (AMAXDOCA('')<0.5*mm)",
        MotherCut = "mcMatch('J/psi(1S)')",
        Preambulo = ["from LoKiPhysMC.functions import mcMatch" ], 
        )
    Bs2JpsiEta = CombineSelection(
        "Bs2JpsiEta",
        [Jpsi, Eta],
        DecayDescriptor = "B_s0 -> J/psi(1S) eta",
        DaughtersCuts = {"eta":		"(mcMatch('[B_s0]cc => (J/psi(1S) => mu+ mu-) ^(eta -> gamma gamma)'))", 
                         "J/psi(1S)":	"(mcMatch('[B_s0]cc => ^(J/psi(1S) => mu+ mu-) (eta -> gamma gamma)'))"}, 
        CombinationCut = Comb_cuts,
        MotherCut = Mother_cuts + "& mcMatch('[B_s0]cc')",
        Preambulo = ["from LoKiPhysMC.functions import mcMatch" ], 
        )
else:
    Jpsi = CombineSelection(#StdLooseJpsi2MuMu
        "Jpsi",
        [Muons],
        DecayDescriptor = "J/psi(1S) -> mu+ mu-",
        DaughtersCuts = {"mu-":		Muon_cuts,
                         "mu+":		Muon_cuts},
        CombinationCut = "(ADAMASS('J/psi(1S)') < 100.*MeV) & (ADOCACHI2CUT(30,''))",
        MotherCut = "VFASPF(VCHI2) < 25.",
        )
    Bs2JpsiEta = CombineSelection(
        "Bs2JpsiEta",
        [Jpsi, Eta],
        DecayDescriptor = "B_s0 -> J/psi(1S) eta",
        DaughtersCuts = {"eta":		"(ADMASS('eta') < 100.*MeV)", 
                         "J/psi(1S)":	"(ADMASS('J/psi(1S)') < 100.*MeV)"}, 
        CombinationCut = Comb_cuts,
        MotherCut = Mother_cuts,
        )



##########################
######### MCJpsiEtaTuple
##########################

MC_JpsiEta_Tuple = MCDecayTreeTuple("MCJpsiEtaTuple")
MC_JpsiEta_Tuple.Decay = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(eta -> ^gamma ^gamma)]CC"
MC_JpsiEta_Tuple.ToolList+=[
    "MCJpsiEtaTupleToolKinematic",
    ]


##########################
######### JpsiEta DecayTreeTuple
##########################

BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    "TupleToolTrackIsolation"
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    "TupleToolTrackIsolation",
    "TupleToolTrackIsolationBDT/IsoTool"
    "TupleToolTrackIsolationBDT/IsoToolVelo"
    ]
GammaToolList = [
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
    "TupleToolVeto/Pi0Veto",
    "TupleToolGamma/GammaMVA",
    "TupleToolTrackIsolation"
    ]


Bs2JpsiEta_Tuple = DecayTreeTuple("JpsiEta")
Bs2JpsiEta_Tuple.Decay = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(eta -> ^gamma ^gamma)]CC"
Bs2JpsiEta_Tuple.Inputs = ["Phys/Bs2JpsiEta"]

# Give names to the particles
Bs2JpsiEta_Tuple.addBranches({'B': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> gamma gamma)]CC',
                       'Jpsi': '[B_s0 -> ^(J/psi(1S) -> mu+ mu-) (eta -> gamma gamma)]CC',
                       'eta': '[B_s0 -> (J/psi(1S) -> mu+ mu-) ^(eta -> gamma gamma)]CC',
                       'muplus': '[B_s0 -> (J/psi(1S) -> ^mu+ mu-) (eta -> gamma gamma)]CC',
                       'muminus': '[B_s0 -> (J/psi(1S) -> mu+ ^mu-) (eta -> gamma gamma)]CC',
                       'gamma': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> ^gamma gamma)]CC'})

Bs2JpsiEta_Tuple.B.ToolList += BToolList
Bs2JpsiEta_Tuple.muplus.ToolList += MuToolList
Bs2JpsiEta_Tuple.muminus.ToolList += MuToolList
Bs2JpsiEta_Tuple.gamma.ToolList += GammaToolList


#### Isolations Variables

#### Long Track Bs2MuMu2017 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.
Bs2JpsiEta_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2JpsiEta_Tuple.muplus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2JpsiEta_Tuple.muplus.IsoTool.Variables = -1

Bs2JpsiEta_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2JpsiEta_Tuple.muminus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2JpsiEta_Tuple.muminus.IsoTool.Variables = -1

Bs2JpsiEta_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.Variables = -2
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.TrackType = 1

Bs2JpsiEta_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.Variables = -2
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.TrackType = 1


if Isolation:
    Bs2JpsiEta_Tuple.B.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2JpsiEta_Tuple.muplus.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2JpsiEta_Tuple.muminus.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2JpsiEta_Tuple.B.addTool(TupleToolMuonVariables,name="IsoTool")
    Bs2JpsiEta_Tuple.muplus.addTool(TupleToolMuonVariables,name="IsoTool")
    Bs2JpsiEta_Tuple.muminus.addTool(TupleToolMuonVariables,name="IsoTool")

##### Gamma Selection
Bs2JpsiEta_Tuple.gamma.addTool(TupleToolVeto,name="Pi0Veto")
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Particle= "gamma"
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs2JpsiEta_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Bs2JpsiEta_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Bs2JpsiEta_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"



#### MC Truth
MCJpsiEtaTruth = Bs2JpsiEta_Tuple.addTupleTool("TupleToolMCTruth")
MCJpsiEtaTruth.addTupleTool("MCTupleToolHierarchy")
MCJpsiEtaTruth.addTupleTool("MCTupleToolAngles")
#MCJpsiEtaTruth.addTupleTool("MCTupleToolReconstructed")



###### LoKi Variables
Bs2JpsiEta_LoKi = Bs2JpsiEta_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/Bs2JpsiEta_LoKi')
#Bs2JpsiEta_LoKi.Preambulo = []
Bs2JpsiEta_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "m":	"M",
    "pt":	"PT",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"CHILD(M,1)"
    }

LoKi_All = Bs2JpsiEta_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }

####### Refitted Variables
#Bs2JpsiEta_LoKi_DTF = Bs2JpsiEta_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_DTF')
#Bs2JpsiEta_LoKi_DTF.Variables = {
#    "DTF_m":	"DTF_FUN(M,True,'J/psi(1S)')",
#    "DTF_pt":	"DTF_FUN(PT,True,'J/psi(1S)')",
#    "DTF_dira":	"DTF_FUN(log(1-BPVDIRA),True,'J/psi(1S)')",
#    }                            

#DTF
Bs2JpsiEta_DTF = Bs2JpsiEta_Tuple.B.addTupleTool('TupleToolDecayTreeFitter/DTF')
Bs2JpsiEta_DTF.constrainToOriginVertex = True
Bs2JpsiEta_DTF.Verbose = False
Bs2JpsiEta_DTF.daughtersToConstrain = ['eta' , 'J/psi(1S)']










####### Trigger
TrigVerb = True
TTT = Bs2JpsiEta_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = [
    "L0Muon",
    "L0DiMuon",
    "L0Hadron",
    "L0Photon",
    "L0Photon,lowMult",
    "L0PhotonNoSPD",
    "L0PhotonHi"
    ]
Hlt1list = [
    "Hlt1TrackAllL0",
    "Hlt1TrackMuon",
    "Hlt1TrackPhoton",
    "Hlt1DiMuonLowMass",
    "Hlt1DiMuonHighMass",
    "Hlt1DiMuonHeavy"
    ]
Hlt2list = [
    "Hlt2SingleMuon",
    "Hlt2SingleMuonHighPT",
    "Hlt2DiMuonDetached",
    "Hlt2DiMuonDetachedHeavy",
    "Hlt2DiMuonDY1",
    "Hlt2DiMuonB",
    "Hlt2B2HH",
    "Hlt2Topo2BodyBBDT",
    "Hlt2Topo3BodyBBDT",
    "Hlt2TopoMu2BodyBBDT",
    "Hlt2TopoMu3BodyBBDT",
    "Hlt2TopoRad2BodyBBDT",
    "Hlt2TopoRad2plus1BodyBBDT"
    ]

tlist = L0list + Hlt1list + Hlt2list

tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList


##########################
######### Sequence & algo
##########################

Bs2JpsiEta_seq = SelectionSequence('Bs2JpsiEta_seq', TopSelection = Bs2JpsiEta)
seq = GaudiSequencer('Bs2JpsiEta_Tuple_seq')
seq.Members += [Bs2JpsiEta_seq.sequence()]
seq.Members += [Bs2JpsiEta_Tuple]

if dv.Simulation:
    seq.Members += [MC_JpsiEta_Tuple]

