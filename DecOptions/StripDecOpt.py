from Gaudi.Configuration import *
from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence, Combine3BodySelection
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, EventNodeKiller, ProcStatusCheck

from PhysConf.Selections import FilterSelection

#DecayTreeFitter
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple, FilterDecays
from Configurables import LoKi__Hybrid__DTFDict as DTFDict


# Node killer: remove the previous Stripping
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

#Stripping
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

stripping='stripping29r2'
config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams(stripping=config, archive=archive) 

# Select my line
MyStrippingStream = "MyStream"
StrippingLineName = 'Bs2MuMuGammaLine'
MyStream = StrippingStream(MyStrippingStream)
MyLines = [ 'Stripping'+StrippingLineName]

for stream in streams: 
    for line in stream.lines:
        if line.name() in MyLines:
            MyStream.appendLines( [ line ] ) 

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )


dv = DaVinci()

##########################
######### Inputs
##########################
BsfromSL = DataOnDemand(
    Location = 'Phys/Bs2MuMuGammaLine/Particles'
    )


######### Bs0 -> Mu Mu Gamma reconstruction
MCDecay = "[B_s0 -> ^mu+ ^mu- ^gamma]CC"
Bs2MuMuGammaF = FilterSelection(
    "Bs2MuMuGammaF",
    [BsfromSL],
    Code = "(SUMTREE(ABSID==22, PT)>1000) & ((SUMTREE(ABSID==531, M)<2950) | (SUMTREE(ABSID==531, M)>3920))"
    )


##########################
######### MC Tuples
##########################

MC_Tuple = MCDecayTreeTuple("MCTuple")
MC_Tuple.Decay = MCDecay
MC_Tuple.ToolList+=[
    "MCTupleToolKinematic",
    ]


##########################
######### MuMuGamma DecayTreeTuple
##########################
BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]
GammaToolList = [
    "TupleToolPhotonInfo",
#    "TupleToolCaloHypo"
#    "TupleToolTrackIsolation"
    ]

Bs2MuMuGamma_Tuple = DecayTreeTuple("MuMuGamma")
Bs2MuMuGamma_Tuple.Decay = "[B_s0 -> ^mu+ ^mu- ^gamma]CC"
Bs2MuMuGamma_Tuple.Inputs = ["Phys/Bs2MuMuGammaF"]

# Give names to the particles
Bs2MuMuGamma_Tuple.addBranches({'B': '[B_s0 -> mu+ mu- gamma]CC',
                       'muplus': '[B_s0 -> ^mu+ mu- gamma]CC',
                       'muminus': '[B_s0 -> mu+ ^mu- gamma]CC',
                       'gamma': '[B_s0 -> mu+ mu- ^gamma]CC'})


#DecayTreeFitter

DictTuple = Bs2MuMuGamma_Tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"

DictTuple.NumVar = 20

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.DTF.dict.Variables = {
    "DTF_pt"            : "PT",
    "DTF_pe"            : "E",
    "DTF_m"             : "M",
    "DTF_vchi2"		: "BPV(VCHI2)",
    "DTF_vchi2dof"	: "VFASPF(VCHI2/VDOF)",
    "DTF_log_doca"	: "log(DOCA(1,2))",
    "DTF_log_dira"	: "log(1-BPVDIRA)",
    "DTF_t"		: "BPVLTIME()",
    "DTF_t_chi2"	: "BPVLTCHI2()",
    "DTF_M_mumu"	: "MASS(1,2)",
    "DTF_M_mugamma"	: "MASS(1,3)",
    "DTF_ip"		: "BPVIP()",
    "DTF_ipchi2"	: "BPVIPCHI2()",
    "DTF_ips"		: "sqrt(BPVIPCHI2())",
    "DTF_eta"		: "ETA",
    "DTF_phi"		: "PHI",
    "DTF_muplus_pt"	: "CHILD(PT,1)",
    "DTF_muminus_pt"	: "CHILD(PT,2)",
    "DTF_gamma_pt"	: "CHILD(PT,3)",
    "DTF_muplus_pe"	: "CHILD(E,1)",
    "DTF_muminus_pe"	: "CHILD(E,2)",
    "DTF_gamma_pe"	: "CHILD(E,3)"
   }




Bs2MuMuGamma_Tuple.B.ToolList += BToolList
Bs2MuMuGamma_Tuple.muplus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.muminus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.gamma.ToolList += GammaToolList

Bs2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolProtoPData")
Bs2MuMuGamma_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID" , "CaloNeutralSpd" ] 


#### MC Truth
MCMuMuGammaTruth = Bs2MuMuGamma_Tuple.addTupleTool("TupleToolMCTruth")
MCMuMuGammaTruth.addTupleTool("MCTupleToolHierarchy")
MCMuMuGammaTruth.addTupleTool("MCTupleToolAngles")
#MCMuMuGammaTruth.addTupleTool("MCTupleToolReconstructed")



###### LoKi Variables
B_LoKi = Bs2MuMuGamma_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/B_LoKi')
B_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    "M_mugamma":"MASS(1,3)",

#RELINFO
    "gamma_n_pi0":	"RELINFO('Phys/{1}/{2}', 'GAMMAISONPI0', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaIsolation'),
    "gamma_n_eta":	"RELINFO('Phys/{1}/{2}', 'GAMMAISONETA', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaIsolation'),
    "gamma_dm_pi0":	"RELINFO('Phys/{1}/{2}', 'GAMMAISODMPI0', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaIsolation'),
    "gamma_dm_eta":	"RELINFO('Phys/{1}/{2}', 'GAMMAISODMETA', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaIsolation'),

    "gamma_CONEANGLE":	"RELINFO('Phys/{1}/{2}', 'CONEANGLE', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEMULT":	"RELINFO('Phys/{1}/{2}', 'CONEMULT', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPX":	"RELINFO('Phys/{1}/{2}', 'CONEPX', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPY":	"RELINFO('Phys/{1}/{2}', 'CONEPY', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPZ":	"RELINFO('Phys/{1}/{2}', 'CONEPZ', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEP":	"RELINFO('Phys/{1}/{2}', 'CONEP', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPT":	"RELINFO('Phys/{1}/{2}', 'CONEPT', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPXASYM":	"RELINFO('Phys/{1}/{2}', 'CONEPXASYM', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPYASYM":	"RELINFO('Phys/{1}/{2}', 'CONEPYASYM', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPZASYM":	"RELINFO('Phys/{1}/{2}', 'CONEPZASYM', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPASYM":	"RELINFO('Phys/{1}/{2}', 'CONEPASYM', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPTASYM":	"RELINFO('Phys/{1}/{2}', 'CONEPTASYM', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEDELTAETA":	"RELINFO('Phys/{1}/{2}', 'CONEDELTAETA', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEDELTAPHI":	"RELINFO('Phys/{1}/{2}', 'CONEDELTAPHI', -1.)".format(MyStrippingStream,StrippingLineName, 'GammaConeIsolation'),

    "BSMUMUCDFISO":	"RELINFO('Phys/{1}/{2}', 'BSMUMUCDFISO', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBMAG":	"RELINFO('Phys/{1}/{2}', 'BSMUMUOTHERBMAG', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBANGLE":	"RELINFO('Phys/{1}/{2}', 'BSMUMUOTHERBANGLE', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBBOOSTMAG":	"RELINFO('Phys/{1}/{2}', 'BSMUMUOTHERBBOOSTMAG', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBBOOSTANGLE":	"RELINFO('Phys/{1}/{2}', 'BSMUMUOTHERBBOOSTANGLE', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBTRACKS":	"RELINFO('Phys/{1}/{2}', 'BSMUMUOTHERBTRACKS', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUPARTID":	"RELINFO('Phys/{1}/{2}', 'BSMUMUPARTID', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUTOPID":	"RELINFO('Phys/{1}/{2}', 'BSMUMUTOPID', -1.)".format(MyStrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),

    "Muon1OldTRKISOBDTFIRSTVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo_Old'),
    "Muon1OldTRKISOBDTSECONDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo_Old'),
    "Muon1OldTRKISOBDTTHIRDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo_Old'),
    "Muon2OldTRKISOBDTFIRSTVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo_Old'),
    "Muon2OldTRKISOBDTSECONDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo_Old'),
    "Muon2OldTRKISOBDTTHIRDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo_Old'),

    "Muon1TRKISOBDTFIRSTVALUE":		"RELINFO('Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo'),
    "Muon1TRKISOBDTSECONDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo'),
    "Muon1TRKISOBDTTHIRDVALUE":		"RELINFO('Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo'),
    "Muon2TRKISOBDTFIRSTVALUE":		"RELINFO('Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo'),
    "Muon2TRKISOBDTSECONDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo'),
    "Muon2TRKISOBDTTHIRDVALUE":		"RELINFO('Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo'),

    "Muon1VeloTRKISOBDTFIRSTVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1VeloTrackIsoBDTInfo'),
    "Muon1VeloTRKISOBDTSECONDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1VeloTrackIsoBDTInfo'),
    "Muon1VeloTRKISOBDTTHIRDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon1VeloTrackIsoBDTInfo'),
    "Muon2VeloTRKISOBDTFIRSTVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2VeloTrackIsoBDTInfo'),
    "Muon2VeloTRKISOBDTSECONDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2VeloTrackIsoBDTInfo'),
    "Muon2VeloTRKISOBDTTHIRDVALUE":	"RELINFO('Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(MyStrippingStream,StrippingLineName, 'Muon2VeloTrackIsoBDTInfo'),

    }

LoKi_All = Bs2MuMuGamma_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


####### Trigger
TrigVerb = True
TTT = Bs2MuMuGamma_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon", "L0DiMuon", "L0Photon"]
Hlt1list = ["Hlt1TrackAllL0", "Hlt1TrackMuon", "Hlt1TrackPhoton", "Hlt1DiMuonLowMass"]
Hlt2list = ["Hlt2SingleMuon", "Hlt2DiMuonDetached", "Hlt2Topo2BodyBBDT", "Hlt2TopoMu2BodyBBDT",
    "Hlt2TopoRad2plus1BodyBBDT"]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList


##########################
######### Sequence & algo
##########################

Bs2MuMuGamma_seq = SelectionSequence('Bs2MuMuGamma_seq', TopSelection = Bs2MuMuGammaF)
seq_MuMuGamma = GaudiSequencer('Bs2MuMuGamma_Tuple_seq')
seq_MuMuGamma.Members += [sc.sequence()]
seq_MuMuGamma.Members += [Bs2MuMuGamma_seq.sequence()]
seq_MuMuGamma.Members += [Bs2MuMuGamma_Tuple]
dv.UserAlgorithms += [event_node_killer, seq_MuMuGamma]


if dv.Simulation:
    dv.UserAlgorithms += [MC_Tuple]
