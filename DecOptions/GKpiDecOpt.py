from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as Units


##########################
######### CombineParticles
##########################

from GaudiConfUtils import ConfigurableGenerators
from PhysSelPython.Wrappers import SimpleSelection, DataOnDemand, MultiSelectionSequence, SelectionSequence
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, MCTupleToolHierarchy, MCTupleToolAngles, MCTupleToolReconstructed, MCDecayTreeTuple, FitDecayTrees, LoKi__HDRFilter, TupleToolVeto, TupleToolTISTOS, TupleToolGamma
from Configurables import PhysConf
PhysConf().CaloReProcessing = True

Pions = DataOnDemand("Phys/StdAllLoosePions/Particles")
Kaons = DataOnDemand("Phys/StdAllLooseKaons/Particles")
Gamma = DataOnDemand("Phys/StdLooseAllPhotons/Particles")

dv = DaVinci()

######### Bs0 reconstruction
if dv.Simulation:
    Bs0 = SimpleSelection(
        "Bs0",
        ConfigurableGenerators.CombineParticles,
        [Pions, Kaons, Gamma],
        DecayDescriptor = "[B0 -> K+ pi- gamma]cc",
        DaughtersCuts = {"pi-":		"mcMatch('[B0]cc => K+ ^pi- gamma')", 
                         "K+":		"mcMatch('[B0]cc => ^K+ pi- gamma')",
                         "gamma":	"(PT > 200*MeV) & (E > 500*MeV) & (CL > 0.2) & (mcMatch('[B0]cc => K+ pi- ^gamma'))"},
        CombinationCut =  "ATRUE",
        MotherCut= "(ADMASS('B0') < 500*MeV)" "& (BPVDIRA > 0.995)" "& mcMatch('[B0]cc')",
        Preambulo = ["from LoKiPhysMC.functions import mcMatch" ],
        )


##########################
######### DecayTreeTuple
##########################

Bs0_Tuple = DecayTreeTuple("direct")
Bs0_Tuple.Decay = "[B0 -> ^K+ ^pi- ^gamma]CC"
Bs0_Tuple.Inputs = ["Phys/Bs0"]

# Give names to the particles
Bs0_Tuple.addBranches({'B_s0': '[B0 -> K+ pi- gamma]CC',
                       'muplus': '[B0 -> ^K+ pi- gamma]CC',
                       'muminus': '[B0 -> K+ ^pi- gamma]CC',
                       'gamma': '[B0 -> K+ pi- ^gamma]CC'})

Bs0_Tuple.B_s0.ToolList+=[
    #    "TupleToolMCBackgroundInfo",
    #    "TupleToolTrackInfo",
    #    "TupleToolRecoStats",
    #    "TupleToolPrimaries",
    #    "TupleToolEventInfo",
    #    "TupleToolConeIsolation",
    "TupleToolTrackIsolation",
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]


muToolList=[
    "TupleToolTrackInfo",
    "TupleToolTrackIsolation",
    "TupleToolGeometry",
    ]
Bs0_Tuple.muplus.ToolList += muToolList
Bs0_Tuple.muminus.ToolList += muToolList

Bs0_Tuple.gamma.ToolList+=[
    "TupleToolTrackIsolation",
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
    "TupleToolVeto/Pi0Veto",
    "TupleToolGamma/GammaMVA",
    ]

##### Gamma Selection
Bs0_Tuple.gamma.addTool(TupleToolVeto,name="Pi0Veto")
Bs0_Tuple.gamma.Pi0Veto.Particle= "gamma"
Bs0_Tuple.gamma.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs0_Tuple.gamma.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs0_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Bs0_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs0_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Bs0_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"



#### MC Truth
MCTruth = Bs0_Tuple.addTupleTool("TupleToolMCTruth")
MCTruth.addTupleTool("MCTupleToolHierarchy")
MCTruth.addTupleTool("MCTupleToolAngles")
#MCTruth.addTupleTool("MCTupleToolReconstructed")



###### LoKi Variables
Bs0_LoKi = Bs0_Tuple.B_s0.addTupleTool('LoKi::Hybrid::TupleTool/Bs0_LoKi')
#Bs0_LoKi.Preambulo = []
Bs0_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "m":	"M",
    "pt":	"PT",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    "M_mugamma":"MASS(1,3)",
    }

#Kplus_LoKi = Bs0_Tuple.Kplus.addTupleTool('LoKi::Hybrid::TupleTool/Kplus_LoKi')
#Kminus_LoKi = Bs0_Tuple.Kminus.addTupleTool('LoKi::Hybrid::TupleTool/Kminus_LoKi')
#KLoKiVar = {
#    "BSKKTRACKPLUSISO" : "RELINFO('/Event/MyStream/Phys/MyLine/MyLocation', 'BSKKTRACKPLUSISO', -1.)"
#    }
#Kplus_LoKi.Variables = KLoKiVar
#Kminus_LoKi.Variables = KLoKiVar

LoKi_All = Bs0_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }

##########################
######### Sequence & algo
##########################

Bs0_seq = SelectionSequence('Bs0_seq', TopSelection = Bs0)

Sel = MultiSelectionSequence('Bs0_Seq', Sequences = [Bs0_seq])

seq = GaudiSequencer('Bs0_Tuple_seq')
seq.Members += [Sel.sequence()]
seq.Members += [Bs0_Tuple]

