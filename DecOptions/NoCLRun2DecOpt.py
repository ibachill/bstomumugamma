from Gaudi.Configuration import *
from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence, Combine3BodySelection
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, MCMatchObjP2MCRelator, TupleToolTrigger

from PhysConf.Selections import FilterSelection

#DecayTreeFitter
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple, FilterDecays
from Configurables import LoKi__Hybrid__DTFDict as DTFDict


dv = DaVinci()

##########################
######### Inputs
##########################
from StandardParticles import StdLooseAllPhotons as Gamma
from StandardParticles import StdLooseMuons as Muons
StrippingStream = "AllStreams"


######### Bs0 -> Mu Mu Gamma reconstruction
Muon_cuts = "(MIPCHI2DV(PRIMARY)>9) & (TRCHI2DOF<3) & (TRGHOSTPROB<0.4)"
Gamma_cuts = "(PT>1000*MeV) & (E>1500*MeV)"
Comb12_cuts = "((AM<2996*MeV) | (AM>4168*MeV)) & (AMAXDOCA('')<0.5*mm)"
Comb_cuts = "(AM<7000*MeV) & (AM>3500*MeV) & (APT>300*MeV)"
Mother_cuts = "(VFASPF(VCHI2/VDOF)<15) & (ADMASS('B_s0')<1500*MeV) & (BPVDIRA>0.995) & (PT>500*MeV) & (BPVIPCHI2()<20)"

MCDecay = "[B_s0 -> ^mu+ ^mu- ^gamma]CC"
Bs2MuMuGamma = Combine3BodySelection(
    "Bs2MuMuGamma",
    [Muons, Gamma],
    DecayDescriptor = "B_s0 -> mu+ mu- gamma",
    DaughtersCuts = {"mu-":		Muon_cuts, 
                     "mu+":		Muon_cuts, 
                     "gamma":		Gamma_cuts},
    Combination12Cut = Comb12_cuts,
    CombinationCut = Comb_cuts,
    MotherCut = Mother_cuts, 
    )




MCDecay = "[B_s0 -> ^mu+ ^mu- ^gamma]CC"


##########################
######### MC Tuples
##########################

MC_Tuple = MCDecayTreeTuple("MCTuple")
MC_Tuple.Decay = MCDecay
MC_Tuple.ToolList+=[
    "MCTupleToolKinematic",
    "TupleToolRecoStats",
    ]


##########################
######### MuMuGamma DecayTreeTuple
##########################
BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]
GammaToolList = [
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
#    "TupleToolL0Calo"
#    "TupleToolTrackIsolation"
    ]

Bs2MuMuGamma_Tuple = DecayTreeTuple("MuMuGamma")
Bs2MuMuGamma_Tuple.Decay = "[B_s0 -> ^mu+ ^mu- ^gamma]CC"
Bs2MuMuGamma_Tuple.Inputs = ["Phys/Bs2MuMuGamma"]

# Give names to the particles
Bs2MuMuGamma_Tuple.addBranches({'B': '[B_s0 -> mu+ mu- gamma]CC',
                       'muplus': '[B_s0 -> ^mu+ mu- gamma]CC',
                       'muminus': '[B_s0 -> mu+ ^mu- gamma]CC',
                       'gamma': '[B_s0 -> mu+ mu- ^gamma]CC'})


#DecayTreeFitter

DictTuple = Bs2MuMuGamma_Tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"

DictTuple.NumVar = 20

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.DTF.dict.Variables = {
    "DTF_pt"            : "PT",
    "DTF_pe"            : "E",
    "DTF_m"             : "M",
    "DTF_vchi2"		: "BPV(VCHI2)",
    "DTF_vchi2dof"	: "VFASPF(VCHI2/VDOF)",
    "DTF_log_doca"	: "log(DOCA(1,2))",
    "DTF_log_dira"	: "log(1-BPVDIRA)",
    "DTF_t"		: "BPVLTIME()",
    "DTF_t_chi2"	: "BPVLTCHI2()",
    "DTF_M_mumu"	: "MASS(1,2)",
    "DTF_M_mugamma"	: "MASS(1,3)",
    "DTF_ip"		: "BPVIP()",
    "DTF_ipchi2"	: "BPVIPCHI2()",
    "DTF_ips"		: "sqrt(BPVIPCHI2())",
    "DTF_eta"		: "ETA",
    "DTF_phi"		: "PHI",
    "DTF_muplus_pt"	: "CHILD(PT,1)",
    "DTF_muminus_pt"	: "CHILD(PT,2)",
    "DTF_gamma_pt"	: "CHILD(PT,3)",
    "DTF_muplus_pe"	: "CHILD(E,1)",
    "DTF_muminus_pe"	: "CHILD(E,2)",
    "DTF_gamma_pe"	: "CHILD(E,3)"
   }


Bs2MuMuGamma_Tuple.ToolList += [
    "TupleToolEventInfo",
    "TupleToolRecoStats",
    ]

for tuple_ in Bs2MuMuGamma_Tuple:
    tuple_.ToolList += [ #-- global event data
	    "TupleToolEventInfo",
	    "TupleToolPrimaries",
	    "TupleToolRecoStats",
	    "TupleToolL0Data",
	    "TupleToolCPU",
	    #-- particle data
	    "TupleToolKinematic",
	    "TupleToolDalitz",
	    "TupleToolPid",
	    "TupleToolTrackInfo",
	    "TupleToolAngles",
	    "TupleToolL0Calo/L0Hcal",
	    "TupleToolL0Calo/L0Ecal",
	    "TupleToolProtoPData",
	    "TupleToolPhotonInfo",
	    "TupleToolBremInfo",
            "TupleToolTrigger"
	    ]

Bs2MuMuGamma_Tuple.B.ToolList += BToolList
Bs2MuMuGamma_Tuple.muplus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.muminus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.gamma.ToolList += GammaToolList

Bs2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolProtoPData")
Bs2MuMuGamma_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID" , "CaloNeutralSpd" ] 

Bs2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolL0Calo")
Bs2MuMuGamma_Tuple.gamma.TupleToolL0Calo.WhichCalo = "ECAL"

ttt = Bs2MuMuGamma_Tuple.addTupleTool("TupleToolTrigger")
ttt.Verbose = True
ttt.TriggerList=['L0PhotonDecision', 'L0MuonDecision']

#### MC Truth

default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

MCMuMuGammaTruth = Bs2MuMuGamma_Tuple.addTupleTool("TupleToolMCTruth")

MCMuMuGammaTruth.addTool(MCMatchObjP2MCRelator)
MCMuMuGammaTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs 
MCMuMuGammaTruth.addTupleTool("MCTupleToolHierarchy")
MCMuMuGammaTruth.addTupleTool("MCTupleToolAngles")

#MCMuMuGammaTruth.addTupleTool("MCTupleToolReconstructed")





###### LoKi Variables
B_LoKi = Bs2MuMuGamma_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/B_LoKi')
B_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    "M_mugamma":"MASS(1,3)",
    }

LoKi_All = Bs2MuMuGamma_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


####### Trigger
TrigVerb = True
TTT = Bs2MuMuGamma_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon",
          "L0DiMuon",
          "L0Photon",
          "L0Hadron"
]
Hlt1list = ['Hlt1TrackMVA',
            'Hlt1TwoTrackMVA',
            'Hlt1TrackMVALoose',
            'Hlt1TwoTrackMVALoose',
            'Hlt1TrackMuon',
            'Hlt1TrackMuonMVA',
            'Hlt1DiMuonHighMass',
            'Hlt1DiMuonLowMass',
            'Hlt1SingleMuonHighPT',
            'Hlt1DiMuonNoL0',
            "Hlt1TrackPhoton",
            "Hlt1SingleMuonNoIPD",
]
Hlt2list = ["Hlt2DiMuon",
            "Hlt2DiMuonDY",
            "Hlt2DiMuonZ",
            "Hlt2DiMuonLowMass",
            "Hlt2DiMuonB",
            "Hlt2DiMuonBTurbo",
            "Hlt2DiMuonDetached",
            "Hlt2DiMuonDetachedHeavy",
            "Hlt2DiMuonDetachedJPsi",
            "Hlt2DiMuonDetachedPsi2S",
            "Hlt2DiMuonJPsi",
            "Hlt2DiMuonJPsiHighPT",
            "Hlt2DiMuonJPsiTurbo",
            "Hlt2DiMuonPsi2S",
            "Hlt2DiMuonPsi2SHighPT",
            "Hlt2DiMuonPsi2STurbo",
            "Hlt2DiMuonSoft",
            "Hlt2EWDiMuonDY1",
            "Hlt2EWSingleMuonHighPt",
            "Hlt2EWSingleMuonLowPt",
            "Hlt2EWSingleMuonVHighPt",
            "Hlt2SingleMuon",
            "Hlt2SingleMuonHighPT",
            "Hlt2SingleMuonLowPT",
            "Hlt2SingleMuonRare",
            "Hlt2Topo2Body",
            "Hlt2Topo3Body",
            "Hlt2Topo4Body",
            "Hlt2TopoE2Body",
            "Hlt2TopoE3Body",
            "Hlt2TopoE4Body",
            "Hlt2TopoMu2Body",
            "Hlt2TopoMu3Body",
            "Hlt2TopoMu4Body",
            "Hlt2TopoMuE2Body",
            "Hlt2TopoMuE3Body",
            "Hlt2TopoMuE4Body",
            "Hlt2TopoMuMu2Body",
            "Hlt2TopoMuMu3Body",
            "Hlt2TopoMuMu4Body",
            "Hlt2TopoMuMuDD",
]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList



##########################
######### Sequence & algo
##########################

Bs2MuMuGamma_seq = SelectionSequence('Bs2MuMuGamma_seq', TopSelection = Bs2MuMuGamma)
seq_MuMuGamma = GaudiSequencer('Bs2MuMuGamma_Tuple_seq')
seq_MuMuGamma.Members += [Bs2MuMuGamma_seq.sequence()]
seq_MuMuGamma.Members += [Bs2MuMuGamma_Tuple]
dv.UserAlgorithms += [seq_MuMuGamma]


if dv.Simulation:
    dv.UserAlgorithms += [MC_Tuple]
