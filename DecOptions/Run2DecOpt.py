from Gaudi.Configuration import *
from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, MCMatchObjP2MCRelator, TupleToolTrigger

from PhysConf.Selections import FilterSelection

#DecayTreeFitter
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple, FilterDecays
from Configurables import LoKi__Hybrid__DTFDict as DTFDict


dv = DaVinci()

##########################
######### Inputs
##########################

StrippingLineName = "Bs2MuMuGammaLine"

if dv.InputType == 'MDST' or dv.InputType == 'mDST':
    if dv.Simulation:
        StrippingStream = "Bs2MuMuGamma.Strip"
    else:
        StrippingStream = "Leptonic"
    dv.RootInTES = '/Event/{0}'.format(StrippingStream)
    BsfromSL = DataOnDemand(
        Location = 'Phys/{0}/Particles'.format(StrippingLineName)
    )
    ######### Consider only matching events
    from PhysConf.Filters import LoKi_Filters
    fltrs = LoKi_Filters (
        STRIP_Code = "HLT_PASS_RE('Stripping"+StrippingLineName+"Decision')"
    )
    dv.EventPreFilters = fltrs.filters('Filters')
else:
    StrippingStream = "AllStreams"
    BsfromSL = DataOnDemand(
        '/Event/{0}/Phys/{1}/Particles'.format(StrippingStream, StrippingLineName)
    )



######### Bs0 -> Mu Mu Gamma reconstruction
MCDecay = "[B_s0 -> ^mu+ ^mu- ^gamma]CC"
Bs2MuMuGamma = FilterSelection(
    "Bs2MuMuGamma",
    [BsfromSL],
    Code = "(SUMTREE(ABSID==22, PT)>1000) & ((SUMTREE(ABSID==531, MASS(1,2))<2950) | (SUMTREE(ABSID==531, MASS(1,2))>3920))"
    )


##########################
######### MC Tuples
##########################

MC_Tuple = MCDecayTreeTuple("MCTuple")
MC_Tuple.Decay = MCDecay
MC_Tuple.ToolList+=[
    "MCTupleToolKinematic",
    "TupleToolRecoStats",
    ]


##########################
######### MuMuGamma DecayTreeTuple
##########################
BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]
GammaToolList = [
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
#    "TupleToolL0Calo"
#    "TupleToolTrackIsolation"
    ]

Bs2MuMuGamma_Tuple = DecayTreeTuple("MuMuGamma")
Bs2MuMuGamma_Tuple.Decay = "[B_s0 -> ^mu+ ^mu- ^gamma]CC"
Bs2MuMuGamma_Tuple.Inputs = ["Phys/Bs2MuMuGamma"]

# Give names to the particles
Bs2MuMuGamma_Tuple.addBranches({'B': '[B_s0 -> mu+ mu- gamma]CC',
                       'muplus': '[B_s0 -> ^mu+ mu- gamma]CC',
                       'muminus': '[B_s0 -> mu+ ^mu- gamma]CC',
                       'gamma': '[B_s0 -> mu+ mu- ^gamma]CC'})


#DecayTreeFitter

DictTuple = Bs2MuMuGamma_Tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"

DictTuple.NumVar = 20

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.DTF.dict.Variables = {
    "DTF_pt"            : "PT",
    "DTF_pe"            : "E",
    "DTF_m"             : "M",
    "DTF_vchi2"		: "BPV(VCHI2)",
    "DTF_vchi2dof"	: "VFASPF(VCHI2/VDOF)",
    "DTF_log_doca"	: "log(DOCA(1,2))",
    "DTF_log_dira"	: "log(1-BPVDIRA)",
    "DTF_t"		: "BPVLTIME()",
    "DTF_t_chi2"	: "BPVLTCHI2()",
    "DTF_M_mumu"	: "MASS(1,2)",
    "DTF_M_mugamma"	: "MASS(1,3)",
    "DTF_ip"		: "BPVIP()",
    "DTF_ipchi2"	: "BPVIPCHI2()",
    "DTF_ips"		: "sqrt(BPVIPCHI2())",
    "DTF_eta"		: "ETA",
    "DTF_phi"		: "PHI",
    "DTF_muplus_pt"	: "CHILD(PT,1)",
    "DTF_muminus_pt"	: "CHILD(PT,2)",
    "DTF_gamma_pt"	: "CHILD(PT,3)",
    "DTF_muplus_pe"	: "CHILD(E,1)",
    "DTF_muminus_pe"	: "CHILD(E,2)",
    "DTF_gamma_pe"	: "CHILD(E,3)"
   }


Bs2MuMuGamma_Tuple.ToolList += [
    "TupleToolEventInfo",
    "TupleToolRecoStats",
    ]

for tuple_ in Bs2MuMuGamma_Tuple:
    tuple_.ToolList += [ #-- global event data
	    "TupleToolEventInfo",
	    "TupleToolPrimaries",
	    "TupleToolRecoStats",
	    "TupleToolL0Data",
	    "TupleToolCPU",
	    #-- particle data
	    "TupleToolKinematic",
	    "TupleToolDalitz",
	    "TupleToolPid",
	    "TupleToolTrackInfo",
	    "TupleToolAngles",
	    "TupleToolL0Calo/L0Hcal",
	    "TupleToolL0Calo/L0Ecal",
	    "TupleToolProtoPData",
	    "TupleToolPhotonInfo",
	    "TupleToolBremInfo",
            "TupleToolTrigger"
	    ]

Bs2MuMuGamma_Tuple.B.ToolList += BToolList
Bs2MuMuGamma_Tuple.muplus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.muminus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.gamma.ToolList += GammaToolList

Bs2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolProtoPData")
Bs2MuMuGamma_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID" , "CaloNeutralSpd" ] 

Bs2MuMuGamma_Tuple.gamma.addTupleTool("TupleToolL0Calo")
Bs2MuMuGamma_Tuple.gamma.TupleToolL0Calo.WhichCalo = "ECAL"

ttt = Bs2MuMuGamma_Tuple.addTupleTool("TupleToolTrigger")
ttt.Verbose = True
ttt.TriggerList=['L0PhotonDecision', 'L0MuonDecision']

#### MC Truth

default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

MCMuMuGammaTruth = Bs2MuMuGamma_Tuple.addTupleTool("TupleToolMCTruth")

MCMuMuGammaTruth.addTool(MCMatchObjP2MCRelator)
MCMuMuGammaTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs 
MCMuMuGammaTruth.addTupleTool("MCTupleToolHierarchy")
MCMuMuGammaTruth.addTupleTool("MCTupleToolAngles")

#MCMuMuGammaTruth.addTupleTool("MCTupleToolReconstructed")





###### LoKi Variables
B_LoKi = Bs2MuMuGamma_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/B_LoKi')
B_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    "M_mugamma":"MASS(1,3)",

#RELINFO
    "gamma_n_pi0":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'GAMMAISONPI0', -1.)".format(StrippingStream,StrippingLineName, 'GammaIsolation'),
    "gamma_n_eta":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'GAMMAISONETA', -1.)".format(StrippingStream,StrippingLineName, 'GammaIsolation'),
    "gamma_dm_pi0":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'GAMMAISODMPI0', -1.)".format(StrippingStream,StrippingLineName, 'GammaIsolation'),
    "gamma_dm_eta":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'GAMMAISODMETA', -1.)".format(StrippingStream,StrippingLineName, 'GammaIsolation'),

    "gamma_CONEANGLE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEANGLE', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEMULT":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEMULT', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPX":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPX', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPY":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPY', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPZ":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPZ', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEP":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEP', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPT":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPT', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPXASYM":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPXASYM', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPYASYM":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPYASYM', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPZASYM":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPZASYM', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPASYM":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPASYM', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEPTASYM":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEPTASYM', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEDELTAETA":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEDELTAETA', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),
    "gamma_CONEDELTAPHI":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'CONEDELTAPHI', -1.)".format(StrippingStream,StrippingLineName, 'GammaConeIsolation'),

    "BSMUMUCDFISO":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUCDFISO', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBMAG":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUOTHERBMAG', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBANGLE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUOTHERBANGLE', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBBOOSTMAG":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUOTHERBBOOSTMAG', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBBOOSTANGLE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUOTHERBBOOSTANGLE', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUOTHERBTRACKS":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUOTHERBTRACKS', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUPARTID":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUPARTID', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),
    "BSMUMUTOPID":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'BSMUMUTOPID', -1.)".format(StrippingStream,StrippingLineName, 'BSMUMUVARIABLES'),

    "Muon1OldTRKISOBDTFIRSTVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo_Old'),
    "Muon1OldTRKISOBDTSECONDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo_Old'),
    "Muon1OldTRKISOBDTTHIRDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo_Old'),
    "Muon2OldTRKISOBDTFIRSTVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo_Old'),
    "Muon2OldTRKISOBDTSECONDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo_Old'),
    "Muon2OldTRKISOBDTTHIRDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo_Old'),

    "Muon1TRKISOBDTFIRSTVALUE":		"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo'),
    "Muon1TRKISOBDTSECONDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo'),
    "Muon1TRKISOBDTTHIRDVALUE":		"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1TrackIsoBDTInfo'),
    "Muon2TRKISOBDTFIRSTVALUE":		"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo'),
    "Muon2TRKISOBDTSECONDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo'),
    "Muon2TRKISOBDTTHIRDVALUE":		"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2TrackIsoBDTInfo'),

    "Muon1VeloTRKISOBDTFIRSTVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1VeloTrackIsoBDTInfo'),
    "Muon1VeloTRKISOBDTSECONDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1VeloTrackIsoBDTInfo'),
    "Muon1VeloTRKISOBDTTHIRDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon1VeloTrackIsoBDTInfo'),
    "Muon2VeloTRKISOBDTFIRSTVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTFIRSTVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2VeloTrackIsoBDTInfo'),
    "Muon2VeloTRKISOBDTSECONDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTSECONDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2VeloTrackIsoBDTInfo'),
    "Muon2VeloTRKISOBDTTHIRDVALUE":	"RELINFO('/Event/{0}/Phys/{1}/{2}', 'TRKISOBDTTHIRDVALUE', -1.)".format(StrippingStream,StrippingLineName, 'Muon2VeloTrackIsoBDTInfo'),

    }

LoKi_All = Bs2MuMuGamma_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


####### Trigger
TrigVerb = True
TTT = Bs2MuMuGamma_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon",
          "L0DiMuon",
          "L0Photon",
          "L0Electron",
          "L0Hadron"
]
Hlt1list = ['Hlt1TrackMVA',
            'Hlt1TwoTrackMVA',
            'Hlt1TrackMVALoose',
            'Hlt1TwoTrackMVALoose',
            'Hlt1TrackMuon',
            'Hlt1TrackMuonMVA',
            'Hlt1DiMuonHighMass',
            'Hlt1DiMuonLowMass',
            'Hlt1SingleMuonHighPT',
            'Hlt1DiMuonNoL0',
            "Hlt1TrackPhoton",
            "Hlt1SingleMuonNoIPD",
]
Hlt2list = ["Hlt2DiMuon",
            "Hlt2DiMuonDY",
            "Hlt2DiMuonZ",
            "Hlt2DiMuonLowMass",
            "Hlt2DiMuonB",
            "Hlt2DiMuonBTurbo",
            "Hlt2DiMuonDetached",
            "Hlt2DiMuonDetachedHeavy",
            "Hlt2DiMuonDetachedJPsi",
            "Hlt2DiMuonDetachedPsi2S",
            "Hlt2DiMuonJPsi",
            "Hlt2DiMuonJPsiHighPT",
            "Hlt2DiMuonJPsiTurbo",
            "Hlt2DiMuonPsi2S",
            "Hlt2DiMuonPsi2SHighPT",
            "Hlt2DiMuonPsi2STurbo",
            "Hlt2DiMuonSoft",
            "Hlt2EWDiMuonDY1",
            "Hlt2EWSingleMuonHighPt",
            "Hlt2EWSingleMuonLowPt",
            "Hlt2EWSingleMuonVHighPt",
            "Hlt2SingleMuon",
            "Hlt2SingleMuonHighPT",
            "Hlt2SingleMuonLowPT",
            "Hlt2SingleMuonRare",
            "Hlt2Topo2Body",
            "Hlt2Topo3Body",
            "Hlt2Topo4Body",
            "Hlt2TopoE2Body",
            "Hlt2TopoE3Body",
            "Hlt2TopoE4Body",
            "Hlt2TopoMu2Body",
            "Hlt2TopoMu3Body",
            "Hlt2TopoMu4Body",
            "Hlt2TopoMuE2Body",
            "Hlt2TopoMuE3Body",
            "Hlt2TopoMuE4Body",
            "Hlt2TopoMuMu2Body",
            "Hlt2TopoMuMu3Body",
            "Hlt2TopoMuMu4Body",
            "Hlt2TopoMuMuDD",
]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList



##########################
######### Sequence & algo
##########################

Bs2MuMuGamma_seq = SelectionSequence('Bs2MuMuGamma_seq', TopSelection = Bs2MuMuGamma)
seq_MuMuGamma = GaudiSequencer('Bs2MuMuGamma_Tuple_seq')
seq_MuMuGamma.Members += [Bs2MuMuGamma_seq.sequence()]
seq_MuMuGamma.Members += [Bs2MuMuGamma_Tuple]
dv.UserAlgorithms += [seq_MuMuGamma]


if dv.Simulation:
    dv.UserAlgorithms += [MC_Tuple]
