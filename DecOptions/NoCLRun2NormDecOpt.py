from Gaudi.Configuration import *

from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence, CombineSelection, FilterSelection
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, TupleToolVeto, TupleToolTISTOS, TupleToolGamma, TupleToolTrackIsolationBDT, TupleToolTrigger


#DecayTreeFitter
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple, FilterDecays
from Configurables import LoKi__Hybrid__DTFDict as DTFDict



from Configurables import L0CaloFix2016
L0CaloFix2016(ApproximateMethod=False,
              Simulation=True,
              TCK='0x160F')

dv = DaVinci()

### Should I compute isolation variables?
Isolation = False
if dv.Simulation:
    StrippingStream = "AllStreams"    
else:
    StrippingStream = "Dimuon"

##########################
######### Inputs
##########################
from StandardParticles import StdLooseEta2gg as Eta0

######### Bs0 -> Mu Mu Gamma reconstruction
Comb_cuts = "(AM<7000*MeV) & (AM>3500*MeV) & (APT>300*MeV)"
Mother_cuts = "(VFASPF(VCHI2/VDOF)<15) & (ADMASS('B_s0')<1500*MeV) & (BPVDIRA>0.995) & (PT>350*MeV) & (BPVIPCHI2()<20)"

######### Bs0 -> Jpsi Eta reconstruction
MCDecay = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(eta -> ^gamma ^gamma)]CC"

Jpsi = DataOnDemand(
    '/Event/{0}/Phys/{1}/Particles'.format(StrippingStream, "FullDSTDiMuonJpsi2MuMuDetachedLine")
    )
Eta = FilterSelection(
    "Eta",
    [Eta0],
    Code = "(PT>1000*MeV) & (MINTREE(ABSID==22, PT)>1000*MeV) & (MINTREE(ABSID==22, E)>1500*MeV)",
    )
Bs2JpsiEta = CombineSelection(
    "Bs2JpsiEta",
    [Jpsi, Eta],
    DecayDescriptor = "B_s0 -> J/psi(1S) eta",
    DaughtersCuts = {"eta":		"(ADMASS('eta') < 105.*MeV)", 
                     "J/psi(1S)":	"(ADMASS('J/psi(1S)') < 100.*MeV)"}, 
    CombinationCut = Comb_cuts,
    MotherCut = Mother_cuts,
    )

##########################
######### MC Tuples
##########################

MC_Tuple = MCDecayTreeTuple("MCTuple")
MC_Tuple.Decay = MCDecay
MC_Tuple.ToolList+=[
    "MCTupleToolKinematic",
    "TupleToolRecoStats",
    ]

##########################
######### JpsiEta DecayTreeTuple
##########################
BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]
GammaToolList = [
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
    "TupleToolVeto/Pi0Veto",
    "TupleToolGamma/GammaMVA",
    "TupleToolTrackIsolation",
    ]


Bs2JpsiEta_Tuple = DecayTreeTuple("JpsiEta")
Bs2JpsiEta_Tuple.Decay = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(eta -> ^gamma ^gamma)]CC"
Bs2JpsiEta_Tuple.Inputs = ["Phys/Bs2JpsiEta"]

Bs2JpsiEta_Tuple.ToolList += [
    "TupleToolEventInfo",
    "TupleToolRecoStats"
    ]

for tuple_ in Bs2JpsiEta_Tuple:
    tuple_.ToolList += [ #-- global event data
	    "TupleToolEventInfo",
	    "TupleToolPrimaries",
	    "TupleToolRecoStats",
	    "TupleToolL0Data",
	    "TupleToolCPU",
	    #-- particle data
	    "TupleToolKinematic",
	    "TupleToolDalitz",
	    "TupleToolPid",
	    "TupleToolTrackInfo",
	    "TupleToolAngles",
	    "TupleToolL0Calo/L0Hcal",
	    "TupleToolL0Calo/L0Ecal",
	    "TupleToolProtoPData",
	    "TupleToolPhotonInfo",
	    "TupleToolBremInfo",
            "TupleToolTrigger"
	    ]

# Give names to the particles
Bs2JpsiEta_Tuple.addBranches({'B': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> gamma gamma)]CC',
                       'Jpsi': '[B_s0 -> ^(J/psi(1S) -> mu+ mu-) (eta -> gamma gamma)]CC',
                       'eta': '[B_s0 -> (J/psi(1S) -> mu+ mu-) ^(eta -> gamma gamma)]CC',
                       'muplus': '[B_s0 -> (J/psi(1S) -> ^mu+ mu-) (eta -> gamma gamma)]CC',
                       'muminus': '[B_s0 -> (J/psi(1S) -> mu+ ^mu-) (eta -> gamma gamma)]CC',
                       'gamma': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> ^gamma gamma)]CC',
                       'gamma0': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> gamma ^gamma)]CC'})

#DecayTreeFitter

DictTuple = Bs2JpsiEta_Tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 20

DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain = ["J/psi(1S)", "eta"]

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.DTF.dict.Variables = {
    "DTF_pt"            : "PT",
    "DTF_pe"            : "E",
    "DTF_m"             : "M",
    "DTF_vchi2"		: "BPV(VCHI2)",
    "DTF_vchi2dof"	: "VFASPF(VCHI2/VDOF)",
    "DTF_log_doca"	: "log(DOCA(1,2))",
    "DTF_log_dira"	: "log(1-BPVDIRA)",
    "DTF_t"		: "BPVLTIME()",
    "DTF_t_chi2"	: "BPVLTCHI2()",
    "DTF_M_mumu"	: "MASS(1,2)",
    "DTF_M_mugamma"	: "MASS(1,3)",
    "DTF_ip"		: "BPVIP()",
    "DTF_ipchi2"	: "BPVIPCHI2()",
    "DTF_ips"		: "sqrt(BPVIPCHI2())",
    "DTF_eta"		: "ETA",
    "DTF_phi"		: "PHI",
    "DTF_muplus_pt"	: "CHILD(CHILD(PT,1),1)",
    "DTF_muminus_pt"	: "CHILD(CHILD(PT,2),1)",
    "DTF_gamma_pt"	: "CHILD(CHILD(PT,1),2)",
    "DTF_gamma0_pt"	: "CHILD(CHILD(PT,2),2)",
    "DTF_muplus_pe"	: "CHILD(CHILD(E,1),1)",
    "DTF_muminus_pe"	: "CHILD(CHILD(E,2),1)",
    "DTF_gamma_pe"	: "CHILD(CHILD(E,1),2)",
    "DTF_gamma0_pe"	: "CHILD(CHILD(E,2),2)"
   }


Bs2JpsiEta_Tuple.B.ToolList += BToolList
Bs2JpsiEta_Tuple.muplus.ToolList += MuToolList
Bs2JpsiEta_Tuple.muminus.ToolList += MuToolList
Bs2JpsiEta_Tuple.gamma.ToolList += GammaToolList
Bs2JpsiEta_Tuple.gamma0.ToolList += GammaToolList

Bs2JpsiEta_Tuple.gamma.addTupleTool("TupleToolProtoPData")
Bs2JpsiEta_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID" , "CaloNeutralSpd" ] 
Bs2JpsiEta_Tuple.gamma0.addTupleTool("TupleToolProtoPData")
Bs2JpsiEta_Tuple.gamma0.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID" , "CaloNeutralSpd" ] 

Bs2JpsiEta_Tuple.gamma.addTupleTool("TupleToolL0Calo")
Bs2JpsiEta_Tuple.gamma.TupleToolL0Calo.WhichCalo = "ECAL"
Bs2JpsiEta_Tuple.gamma0.addTupleTool("TupleToolL0Calo")
Bs2JpsiEta_Tuple.gamma0.TupleToolL0Calo.WhichCalo = "ECAL"

ttt = Bs2JpsiEta_Tuple.addTupleTool("TupleToolTrigger")
ttt.Verbose = True
ttt.TriggerList=['L0PhotonDecision']


#### Isolations Variables

#### Long Track Bs2MuMu2017 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.
Bs2JpsiEta_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs2JpsiEta_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2JpsiEta_Tuple.muplus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2JpsiEta_Tuple.muplus.IsoTool.Variables = -1

Bs2JpsiEta_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs2JpsiEta_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2JpsiEta_Tuple.muminus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2JpsiEta_Tuple.muminus.IsoTool.Variables = -1

Bs2JpsiEta_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs2JpsiEta_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.Variables = -2
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.TrackType = 1

Bs2JpsiEta_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs2JpsiEta_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.Variables = -2
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.TrackType = 1

##### Gamma Selection
Bs2JpsiEta_Tuple.gamma.addTool(TupleToolVeto,name="Pi0Veto")
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Particle= "gamma"
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs2JpsiEta_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Bs2JpsiEta_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
#Bs2JpsiEta_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"

Bs2JpsiEta_Tuple.gamma0.addTool(TupleToolVeto,name="Pi0Veto")
Bs2JpsiEta_Tuple.gamma0.Pi0Veto.Particle= "gamma"
Bs2JpsiEta_Tuple.gamma0.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma0.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs2JpsiEta_Tuple.gamma0.addTool(TupleToolGamma,name="GammaMVA")
Bs2JpsiEta_Tuple.gamma0.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma0.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
#Bs2JpsiEta_Tuple.gamma0.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"



#### MC Truth
MCJpsiEtaTruth = Bs2JpsiEta_Tuple.addTupleTool("TupleToolMCTruth")
MCJpsiEtaTruth.addTupleTool("MCTupleToolHierarchy")
MCJpsiEtaTruth.addTupleTool("MCTupleToolAngles")
#MCJpsiEtaTruth.addTupleTool("MCTupleToolReconstructed")



###### LoKi Variables
Bs2JpsiEta_LoKi = Bs2JpsiEta_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/Bs2JpsiEta_LoKi')
#Bs2JpsiEta_LoKi.Preambulo = []
Bs2JpsiEta_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "log_doca":	"log(CHILD(DOCA(1,2),1))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"CHILD(M,1)"
    }

LoKi_All = Bs2JpsiEta_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


#DTF
Bs2JpsiEta_DTF = Bs2JpsiEta_Tuple.B.addTupleTool('TupleToolDecayTreeFitter/DTF')
Bs2JpsiEta_DTF.constrainToOriginVertex = True
Bs2JpsiEta_DTF.Verbose = False
Bs2JpsiEta_DTF.daughtersToConstrain = ['eta' , 'J/psi(1S)']


####### Trigger
TrigVerb = True
TTT = Bs2JpsiEta_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon",
          "L0DiMuon",
          "L0Photon",
          "L0Hadron"
]
Hlt1list = ['Hlt1TrackMVA',
            'Hlt1TwoTrackMVA',
            'Hlt1TrackMVALoose',
            'Hlt1TwoTrackMVALoose',
            'Hlt1TrackMuon',
            'Hlt1TrackMuonMVA',
            'Hlt1DiMuonHighMass',
            'Hlt1DiMuonLowMass',
            'Hlt1SingleMuonHighPT',
            'Hlt1DiMuonNoL0',
            "Hlt1TrackPhoton",
            "Hlt1SingleMuonNoIPD",
]
Hlt2list = ["Hlt2DiMuon",
            "Hlt2DiMuonLowMass",
            "Hlt2DiMuonB",
            "Hlt2DiMuonDetached",
            "Hlt2DiMuonDetachedHeavy",
            "Hlt2DiMuonDetachedJPsi",
            "Hlt2DiMuonDetachedPsi2S",
            "Hlt2DiMuonJPsi",
            "Hlt2DiMuonJPsiHighPT",
            "Hlt2DiMuonPsi2S",
            "Hlt2DiMuonPsi2SHighPT",
            "Hlt2DiMuonSoft",
            "Hlt2SingleMuon",
            "Hlt2SingleMuonHighPT",
            "Hlt2SingleMuonLowPT",
            "Hlt2SingleMuonRare",
            "Hlt2Topo2Body",
            "Hlt2Topo3Body",
            "Hlt2Topo4Body",
            "Hlt2TopoE2Body",
            "Hlt2TopoE3Body",
            "Hlt2TopoE4Body",
            "Hlt2TopoMu2Body",
            "Hlt2TopoMu3Body",
            "Hlt2TopoMu4Body",
            "Hlt2TopoMuE2Body",
            "Hlt2TopoMuE3Body",
            "Hlt2TopoMuE4Body",
            "Hlt2TopoMuMu2Body",
            "Hlt2TopoMuMu3Body",
            "Hlt2TopoMuMu4Body",
            "Hlt2TopoMuMuDD",
            ]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList




##########################
######### Sequence & algo
##########################

Bs2JpsiEta_seq = SelectionSequence('Bs2JpsiEta_seq', TopSelection = Bs2JpsiEta)
seq_JpsiEta = GaudiSequencer('Bs2JpsiEta_Tuple_seq')
seq_JpsiEta.Members += [Bs2JpsiEta_seq.sequence()]
seq_JpsiEta.Members += [Bs2JpsiEta_Tuple]
dv.UserAlgorithms += [seq_JpsiEta]

if dv.Simulation:
    dv.UserAlgorithms += [MC_Tuple]
