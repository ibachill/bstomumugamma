from Gaudi.Configuration import *
from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence, Combine3BodySelection, CombineSelection, Selection, FilterSelection
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, TupleToolVeto, TupleToolTISTOS, TupleToolGamma, TupleToolMuonVariables, TupleToolTrackIsolationBDT, PhysConf, FilterInTrees

#DecayTreeFitter
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict


dv = DaVinci()

### Should I compute isolation variables?
Isolation = False
### Should I Reprocess Calo data?
CaloData = True


#### Reprocess Calo data
PhysConf().CaloReProcessing = CaloData

##########################
######### Inputs
##########################
from StandardParticles import StdLooseAllPhotons as Gamma
from StandardParticles import StdLoosePions as Pions
from StandardParticles import StdNoPIDsMuons as Muons


######### Bs0 -> Mu Mu Gamma reconstruction
Muon_cuts = "(MIPCHI2DV(PRIMARY)>9) & (TRCHI2DOF<3) & (TRGHOSTPROB<0.4)"
Gamma_cuts = "(PT>500*MeV) & (E>1000*MeV) & (CL>0.2)"
Comb12_cuts = "((AM<3033*MeV) | (AM>3916*MeV)) & (AM<7000*MeV) & (AMAXDOCA('')<0.5*mm)"
Comb_cuts = "(AM<7000*MeV) & (AM>3500*MeV) & (APT>200*MeV)"
Mother_cuts = "(VFASPF(VCHI2/VDOF)<15) & (ADMASS('B_s0')<1500*MeV) & (BPVDIRA>0.995) & (PT>350*MeV) & (BPVIPCHI2()<20)"

MCDecay = "[B_s0 -> ^pi+ ^mu- ^gamma]CC"
Bs2MuMuGamma = Combine3BodySelection(
    "Bs2MuMuGamma",
    [Pions, Muons, Gamma],
    DecayDescriptor = "[B_s0 -> pi+ mu- gamma]cc",
    DaughtersCuts = {"mu-":		Muon_cuts, 
                     "pi+":		Muon_cuts, 
                     "gamma":	Gamma_cuts},
    Combination12Cut = Comb12_cuts,
    CombinationCut = Comb_cuts,
    MotherCut = Mother_cuts, 
    )


######### Bs0 -> Jpsi Eta reconstruction
Jpsi = CombineSelection(#StdLooseJpsi2MuMu
    "Jpsi",
    [Muons],
    DecayDescriptor = "J/psi(1S) -> mu+ mu-",
    DaughtersCuts = {"mu-":		Muon_cuts,
                     "mu+":		Muon_cuts},
    CombinationCut = "(ADAMASS('J/psi(1S)') < 100.*MeV) & (ADOCACHI2CUT(30,''))",
    MotherCut = "VFASPF(VCHI2) < 25.",
    )

from StandardParticles import StdLooseEta2gg as Eta
Eta = FilterSelection(
    "Eta",
    [Eta],
    Code = "(PT>1000*MeV) & (MINTREE(ABSID==22, PT)>500*MeV) & (MINTREE(ABSID==22, E)>1000*MeV) & (MINTREE(ABSID==22, CL)>0.2)",
    )
Bs2JpsiEta = CombineSelection(
    "Bs2JpsiEta",
    [Jpsi, Eta],
    DecayDescriptor = "B_s0 -> J/psi(1S) eta",
    DaughtersCuts = {"eta":		"(ADMASS('eta') < 100.*MeV)", 
                     "J/psi(1S)":	"(ADMASS('J/psi(1S)') < 100.*MeV)"}, 
    CombinationCut = Comb_cuts,
    MotherCut = Mother_cuts,
    )




##########################
######### MC Tuples
##########################

MC_Tuple = MCDecayTreeTuple("MCTuple")
MC_Tuple.Decay = MCDecay
MC_Tuple.ToolList+=[
    "MCTupleToolKinematic",
    ]


##########################
######### MuMuGamma DecayTreeTuple
##########################

BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]
GammaToolList = [
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
    "TupleToolVeto/Pi0Veto",
    "TupleToolGamma/GammaMVA",
    "TupleToolTrackIsolation"
    ]

Bs2MuMuGamma_Tuple = DecayTreeTuple("MuMuGamma")
Bs2MuMuGamma_Tuple.Decay = "[B_s0 -> ^pi+ ^mu- ^gamma]CC"
Bs2MuMuGamma_Tuple.Inputs = ["Phys/Bs2MuMuGamma"]

# Give names to the particles
Bs2MuMuGamma_Tuple.addBranches({'B': '[B_s0 -> pi+ mu- gamma]CC',
                       'muplus': '[B_s0 -> ^pi+ mu- gamma]CC',
                       'muminus': '[B_s0 -> pi+ ^mu- gamma]CC',
                       'gamma': '[B_s0 -> pi+ mu- ^gamma]CC'})


#DecayTreeFitter
DictTuple = Bs2MuMuGamma_Tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"

DictTuple.NumVar = 20

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

DictTuple.DTF.dict.Variables = {
    "DTF_pt"            : "PT",
    "DTF_pe"            : "E",
    "DTF_m"             : "M",
    "DTF_vchi2"		: "BPV(VCHI2)",
    "DTF_vchi2dof"	: "VFASPF(VCHI2/VDOF)",
    "DTF_log_doca"	: "log(DOCA(1,2))",
    "DTF_log_dira"	: "log(1-BPVDIRA)",
    "DTF_t"		: "BPVLTIME()",
    "DTF_t_chi2"	: "BPVLTCHI2()",
    "DTF_M_mumu"	: "MASS(1,2)",
    "DTF_M_mugamma"	: "MASS(1,3)",
    "DTF_ip"		: "BPVIP()",
    "DTF_ipchi2"	: "BPVIPCHI2()",
    "DTF_ips"		: "sqrt(BPVIPCHI2())",
    "DTF_eta"		: "ETA",
    "DTF_phi"		: "PHI",
    "DTF_muplus_pt"	: "CHILD(PT,1)",
    "DTF_muminus_pt"	: "CHILD(PT,2)",
    "DTF_gamma_pt"	: "CHILD(PT,3)",
    "DTF_muplus_pe"	: "CHILD(E,1)",
    "DTF_muminus_pe"	: "CHILD(E,2)",
    "DTF_gamma_pe"	: "CHILD(E,3)"
   }




Bs2MuMuGamma_Tuple.B.ToolList += BToolList
Bs2MuMuGamma_Tuple.muplus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.muminus.ToolList += MuToolList
Bs2MuMuGamma_Tuple.gamma.ToolList += GammaToolList


#### Isolations Variables

#### Long Track Bs2MuMu2017 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.
Bs2MuMuGamma_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs2MuMuGamma_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2MuMuGamma_Tuple.muplus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2MuMuGamma_Tuple.muplus.IsoTool.Variables = -1

Bs2MuMuGamma_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs2MuMuGamma_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2MuMuGamma_Tuple.muminus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2MuMuGamma_Tuple.muminus.IsoTool.Variables = -1

Bs2MuMuGamma_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs2MuMuGamma_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2MuMuGamma_Tuple.muplus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs2MuMuGamma_Tuple.muplus.IsoToolVelo.Variables = -2
Bs2MuMuGamma_Tuple.muplus.IsoToolVelo.TrackType = 1

Bs2MuMuGamma_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs2MuMuGamma_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2MuMuGamma_Tuple.muminus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs2MuMuGamma_Tuple.muminus.IsoToolVelo.Variables = -2
Bs2MuMuGamma_Tuple.muminus.IsoToolVelo.TrackType = 1


if Isolation:
    Bs2MuMuGamma_Tuple.B.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2MuMuGamma_Tuple.muplus.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2MuMuGamma_Tuple.muminus.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2MuMuGamma_Tuple.B.addTool(TupleToolMuonVariables,name="IsoTool")
    Bs2MuMuGamma_Tuple.muplus.addTool(TupleToolMuonVariables,name="IsoTool")
    Bs2MuMuGamma_Tuple.muminus.addTool(TupleToolMuonVariables,name="IsoTool")

#### Gamma Isolation
Bs2MuMuGamma_Tuple.gamma.addTool(TupleToolVeto,name="Pi0Veto")
Bs2MuMuGamma_Tuple.gamma.Pi0Veto.Particle= "gamma"
Bs2MuMuGamma_Tuple.gamma.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2MuMuGamma_Tuple.gamma.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs2MuMuGamma_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Bs2MuMuGamma_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2MuMuGamma_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Bs2MuMuGamma_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"



#### MC Truth
MCMuMuGammaTruth = Bs2MuMuGamma_Tuple.addTupleTool("TupleToolMCTruth")
MCMuMuGammaTruth.addTupleTool("MCTupleToolHierarchy")
MCMuMuGammaTruth.addTupleTool("MCTupleToolAngles")
#MCMuMuGammaTruth.addTupleTool("MCTupleToolReconstructed")



###### LoKi Variables
Bs2MuMuGamma_LoKi = Bs2MuMuGamma_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/Bs2MuMuGamma_LoKi')
#Bs2MuMuGamma_LoKi.Preambulo = []
Bs2MuMuGamma_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    "M_mugamma":"MASS(1,3)",
    }

LoKi_All = Bs2MuMuGamma_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


####### Trigger
TrigVerb = True
TTT = Bs2MuMuGamma_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon", "L0DiMuon", "L0Photon"]
Hlt1list = ["Hlt1TrackAllL0", "Hlt1TrackMuon", "Hlt1TrackPhoton", "Hlt1DiMuonLowMass"]
Hlt2list = ["Hlt2SingleMuon", "Hlt2DiMuonDetached", "Hlt2Topo2BodyBBDT", "Hlt2TopoMu2BodyBBDT",
    "Hlt2TopoRad2plus1BodyBBDT"]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList





##########################
######### JpsiEta DecayTreeTuple
##########################

Bs2JpsiEta_Tuple = DecayTreeTuple("JpsiEta")
Bs2JpsiEta_Tuple.Decay = "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(eta -> ^gamma ^gamma)]CC"
Bs2JpsiEta_Tuple.Inputs = ["Phys/Bs2JpsiEta"]

# Give names to the particles
Bs2JpsiEta_Tuple.addBranches({'B': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> gamma gamma)]CC',
                       'Jpsi': '[B_s0 -> ^(J/psi(1S) -> mu+ mu-) (eta -> gamma gamma)]CC',
                       'eta': '[B_s0 -> (J/psi(1S) -> mu+ mu-) ^(eta -> gamma gamma)]CC',
                       'muplus': '[B_s0 -> (J/psi(1S) -> ^mu+ mu-) (eta -> gamma gamma)]CC',
                       'muminus': '[B_s0 -> (J/psi(1S) -> mu+ ^mu-) (eta -> gamma gamma)]CC',
                       'gamma': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> ^gamma gamma)]CC',
                       'gamma0': '[B_s0 -> (J/psi(1S) -> mu+ mu-) (eta -> gamma ^gamma)]CC'})

Bs2JpsiEta_Tuple.B.ToolList += BToolList
Bs2JpsiEta_Tuple.muplus.ToolList += MuToolList
Bs2JpsiEta_Tuple.muminus.ToolList += MuToolList
Bs2JpsiEta_Tuple.gamma.ToolList += GammaToolList
Bs2JpsiEta_Tuple.gamma0.ToolList += GammaToolList


#### Isolations Variables

#### Long Track Bs2MuMu2017 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.
Bs2JpsiEta_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs2JpsiEta_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2JpsiEta_Tuple.muplus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2JpsiEta_Tuple.muplus.IsoTool.Variables = -1

Bs2JpsiEta_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs2JpsiEta_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs2JpsiEta_Tuple.muminus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs2JpsiEta_Tuple.muminus.IsoTool.Variables = -1

Bs2JpsiEta_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs2JpsiEta_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.Variables = -2
Bs2JpsiEta_Tuple.muplus.IsoToolVelo.TrackType = 1

Bs2JpsiEta_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs2JpsiEta_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.Variables = -2
Bs2JpsiEta_Tuple.muminus.IsoToolVelo.TrackType = 1

if Isolation:
    Bs2JpsiEta_Tuple.B.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2JpsiEta_Tuple.muplus.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2JpsiEta_Tuple.muminus.ToolList+=[
        "TupleToolMuonVariables/IsoTool"
        ]
    Bs2JpsiEta_Tuple.B.addTool(TupleToolMuonVariables,name="IsoTool")
    Bs2JpsiEta_Tuple.muplus.addTool(TupleToolMuonVariables,name="IsoTool")
    Bs2JpsiEta_Tuple.muminus.addTool(TupleToolMuonVariables,name="IsoTool")

##### Gamma Selection
Bs2JpsiEta_Tuple.gamma.addTool(TupleToolVeto,name="Pi0Veto")
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Particle= "gamma"
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs2JpsiEta_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Bs2JpsiEta_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Bs2JpsiEta_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"

Bs2JpsiEta_Tuple.gamma0.addTool(TupleToolVeto,name="Pi0Veto")
Bs2JpsiEta_Tuple.gamma0.Pi0Veto.Particle= "gamma"
Bs2JpsiEta_Tuple.gamma0.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma0.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs2JpsiEta_Tuple.gamma0.addTool(TupleToolGamma,name="GammaMVA")
Bs2JpsiEta_Tuple.gamma0.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs2JpsiEta_Tuple.gamma0.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Bs2JpsiEta_Tuple.gamma0.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"



#### MC Truth
MCJpsiEtaTruth = Bs2JpsiEta_Tuple.addTupleTool("TupleToolMCTruth")
MCJpsiEtaTruth.addTupleTool("MCTupleToolHierarchy")
MCJpsiEtaTruth.addTupleTool("MCTupleToolAngles")
#MCJpsiEtaTruth.addTupleTool("MCTupleToolReconstructed")



###### LoKi Variables
Bs2JpsiEta_LoKi = Bs2JpsiEta_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/Bs2JpsiEta_LoKi')
#Bs2JpsiEta_LoKi.Preambulo = []
Bs2JpsiEta_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "m":	"M",
    "pt":	"PT",
    "log_doca":	"log(CHILD(DOCA(1,2),1))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"CHILD(M,1)"
    }

LoKi_All = Bs2JpsiEta_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


#DTF
Bs2JpsiEta_DTF = Bs2JpsiEta_Tuple.B.addTupleTool('TupleToolDecayTreeFitter/DTF')
Bs2JpsiEta_DTF.constrainToOriginVertex = True
Bs2JpsiEta_DTF.Verbose = False
Bs2JpsiEta_DTF.daughtersToConstrain = ['eta' , 'J/psi(1S)']


####### Trigger
TrigVerb = True
TTT = Bs2JpsiEta_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon", "L0DiMuon", "L0Photon"]
Hlt1list = ["Hlt1TrackAllL0", "Hlt1TrackMuon", "Hlt1TrackPhoton", "Hlt1DiMuonLowMass"]
Hlt2list = ["Hlt2SingleMuon", "Hlt2DiMuonDetached", "Hlt2Topo2BodyBBDT", "Hlt2TopoMu2BodyBBDT",
    "Hlt2TopoRad2plus1BodyBBDT"]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList




##########################
######### Sequence & algo
##########################

Bs2MuMuGamma_seq = SelectionSequence('Bs2MuMuGamma_seq', TopSelection = Bs2MuMuGamma)
seq_MuMuGamma = GaudiSequencer('Bs2MuMuGamma_Tuple_seq')
seq_MuMuGamma.Members += [Bs2MuMuGamma_seq.sequence()]
seq_MuMuGamma.Members += [Bs2MuMuGamma_Tuple]
dv.UserAlgorithms += [seq_MuMuGamma]


Bs2JpsiEta_seq = SelectionSequence('Bs2JpsiEta_seq', TopSelection = Bs2JpsiEta)
seq_JpsiEta = GaudiSequencer('Bs2JpsiEta_Tuple_seq')
seq_JpsiEta.Members += [Bs2JpsiEta_seq.sequence()]
seq_JpsiEta.Members += [Bs2JpsiEta_Tuple]
dv.UserAlgorithms += [seq_JpsiEta]



if dv.Simulation:
    dv.UserAlgorithms += [MC_Tuple]
