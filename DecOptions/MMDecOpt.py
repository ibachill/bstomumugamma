from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as Units


##########################
######### CombineParticles
##########################

from GaudiConfUtils import ConfigurableGenerators
from PhysSelPython.Wrappers import SimpleSelection, DataOnDemand, MultiSelectionSequence, SelectionSequence
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, MCTupleToolHierarchy, MCTupleToolAngles, MCTupleToolReconstructed, MCDecayTreeTuple, FitDecayTrees, LoKi__HDRFilter, TupleToolVeto, TupleToolTISTOS, TupleToolGamma
from Configurables import PhysConf
PhysConf().CaloReProcessing = True

Muons = DataOnDemand("Phys/StdLooseMuons/Particles")
Gamma = DataOnDemand("Phys/StdLooseAllPhotons/Particles")

dv = DaVinci()

######### Bs0 reconstruction
Bs0 = SimpleSelection(
    "Bs0",
    ConfigurableGenerators.CombineParticles,
    [Muons, Gamma],
    DecayDescriptor = "B_s0 -> mu+ mu-",
    DaughtersCuts = {"mu-":		"mcMatch('B_s0 => ^mu+ ^mu- gamma')", 
                     "mu+":		"mcMatch('B_s0 => ^mu+ ^mu- gamma')"},
    CombinationCut = "(AM12>4300*MeV)",
    MotherCut= "(ADMASS('B_s0') < 300*MeV)" "& (BPVDIRA > 0.999)" "& mcMatch('[B_s0]cc')",
    Preambulo = ["from LoKiPhysMC.functions import mcMatch" ],
    )

##########################
######### DecayTreeTuple
##########################

Bs0_Tuple = DecayTreeTuple("direct")
Bs0_Tuple.Decay = "[B_s0 -> ^mu+ ^mu-]CC"
Bs0_Tuple.Inputs = ["Phys/Bs0"]

# Give names to the particles
Bs0_Tuple.addBranches({'B_s0': '[B_s0 -> mu+ mu-]CC',
                       'muplus': '[B_s0 -> ^mu+ mu-]CC',
                       'muminus': '[B_s0 -> mu+ ^mu-]CC'})

Bs0_Tuple.B_s0.ToolList+=[
    #    "TupleToolMCBackgroundInfo",
    #    "TupleToolTrackInfo",
    #    "TupleToolRecoStats",
    #    "TupleToolPrimaries",
    #    "TupleToolEventInfo",
    #    "TupleToolConeIsolation",
    "TupleToolTrackIsolation",
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]


MuToolList=[
    "TupleToolTrackInfo",
    "TupleToolTrackIsolation",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]
Bs0_Tuple.muplus.ToolList += MuToolList
Bs0_Tuple.muminus.ToolList += MuToolList


#### MC Truth
MCTruth = Bs0_Tuple.addTupleTool("TupleToolMCTruth")
MCTruth.addTupleTool("MCTupleToolHierarchy")
MCTruth.addTupleTool("MCTupleToolAngles")
#MCTruth.addTupleTool("MCTupleToolReconstructed")



###### LoKi Variables
Bs0_LoKi = Bs0_Tuple.B_s0.addTupleTool('LoKi::Hybrid::TupleTool/Bs0_LoKi')
#Bs0_LoKi.Preambulo = []
Bs0_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "m":	"M",
    "pt":	"PT",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    }

LoKi_All = Bs0_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }

####### Refitted Variables
Bs0_LoKi_DTF = Bs0_Tuple.B_s0.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_DTF')
Bs0_LoKi_DTF.Variables = {
    "DTF_m":	"DTF_FUN(M,True,'B_s0')",
    "DTF_pt":	"DTF_FUN(PT,True,'B_s0')",
    "DTF_dira":	"DTF_FUN(log(1-BPVDIRA),True,'B_s0')",
    }                            


##########################
######### Sequence & algo
##########################

Bs0_seq = SelectionSequence('Bs0_seq', TopSelection = Bs0)

Sel = MultiSelectionSequence('Bs0_Seq', Sequences = [Bs0_seq])

seq = GaudiSequencer('Bs0_Tuple_seq')
seq.Members += [Sel.sequence()]
seq.Members += [Bs0_Tuple]
