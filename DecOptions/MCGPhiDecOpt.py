from Gaudi.Configuration import *

from PhysSelPython.Wrappers import DataOnDemand, SelectionSequence, FilterSelection, Selection, Combine3BodySelection
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, GaudiSequencer, PhysConf, TupleToolVeto, FilterInTrees, TupleToolGamma, TupleToolTrackIsolationBDT, EventNodeKiller, ProcStatusCheck, TupleToolL0Calo


#DecayTreeFitter
from Configurables import LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__Dict2Tuple, FilterDecays
from Configurables import LoKi__Hybrid__DTFDict as DTFDict


dv = DaVinci()

if dv.Simulation:
    MyStrippingStream = "AllStreams"    
else:
    MyStrippingStream = "BhadronCompleteEvent"

StrippingLineName = "Beauty2XGammaExclusiveBs2PhiGammaLine"


###############
##### Restripping
###############
# Node killer: remove the previous Stripping
event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

#Stripping
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

if dv.DataType == '2016':
    stripping='stripping28r1'
elif dv.DataType == '2015':
    stripping='stripping24r1'
elif dv.DataType == '2017':
    stripping='stripping29r2'
elif dv.DataType == '2018':
    stripping='stripping34'



config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams(stripping=config, archive=archive) 

MyStream = StrippingStream(MyStrippingStream)
MyLines = [ 'Stripping'+StrippingLineName]

for stream in streams: 
    for line in stream.lines:
        if line.name() in MyLines:
            MyStream.appendLines( [ line ] ) 

# Configure Stripping
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )




##### Inputs
# Extract the particules from the Stripping Line
# to perform a 3-body-like reconstruction

BsfromSL = DataOnDemand(
    'Phys/{0}/Particles'.format(StrippingLineName)
    )
Kaons_from_B = FilterInTrees('Kaons_from_B_filter', Code="('K+' == ABSID)")
Kaons = Selection("Kaons_from_B_sel",
                  Algorithm=Kaons_from_B,
                  RequiredSelections=[BsfromSL]
                  )
Gamma_from_B = FilterInTrees('Gamma_from_B_filter', Code="('gamma' == ABSID)")
Gamma = Selection("Gamma_from_B_sel",
                  Algorithm=Gamma_from_B,
                  RequiredSelections=[BsfromSL]
                  )



## Apply gamma_pt filter
# Bs2PhiGamma = FilterSelection(
#     "Bs2PhiGamma",
#     [BsfromSL],
#     Code = "(SUMTREE(ABSID==22, PT)>1000)"
#     )

Muon_cuts = "(MIPCHI2DV(PRIMARY)>9) & (TRCHI2DOF<3) & (TRGHOSTPROB<0.4)"
Gamma_cuts = "(PT>500*MeV) & (E>1500*MeV) & (CL>0.2)"
Comb12_cuts = "((AM<2996*MeV) | (AM>4168*MeV)) & (AMAXDOCA('')<0.5*mm)"
Comb_cuts = "(AM<7000*MeV) & (AM>3500*MeV) & (APT>300*MeV)"
Mother_cuts = "(VFASPF(VCHI2/VDOF)<15) & (ADMASS('B_s0')<1500*MeV) & (BPVDIRA>0.995) & (PT>350*MeV) & (BPVIPCHI2()<20)"

Bs2PhiGamma = Combine3BodySelection(
    "Bs2PhiGamma",
    [Kaons, Gamma],
    DecayDescriptor = "B_s0 -> K+ K- gamma",
    DaughtersCuts = {"K-":		Muon_cuts, 
                     "K+":		Muon_cuts, 
                     "gamma":		Gamma_cuts},
    Combination12Cut = Comb12_cuts,
    CombinationCut = Comb_cuts,
    MotherCut = Mother_cuts, 
    )




# #Consider only matching events
# from PhysConf.Filters import LoKi_Filters
# fltrs = LoKi_Filters (
#     STRIP_Code = "HLT_PASS_RE('Stripping"+StrippingLineName+"Decision')"
# )
# dv.EventPreFilters = fltrs.filters('Filters')


##########################
######### DecayTreeTuple
##########################
Bs0_Tuple = DecayTreeTuple("PhiGamma")
Bs0_Tuple.Decay = "[B_s0 -> ^K+ ^K- ^gamma]CC"
Bs0_Tuple.Inputs = ["Phys/Bs2PhiGamma"]

# Give names to the particles
Bs0_Tuple.addBranches({'B'	: '[B_s0 -> K+ K- gamma]CC',
                       'muplus'	: '[B_s0 -> ^K+ K- gamma]CC',
                       'muminus': '[B_s0 -> K+ ^K- gamma]CC',
                       'gamma'	: '[B_s0 -> K+ K- ^gamma]CC'})


#DecayTreeFitter
DictTuple = Bs0_Tuple.B.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict,"DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 20

DictTuple.DTF.constrainToOriginVertex = True

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

if dv.DataType == '2018':
    DictTuple.DTF.dict.Variables = {
        "pt"            : "PT",
        "pe"            : "E",
        "m"             : "M",
        "vchi2"		: "BPV(VCHI2)",
        "vchi2dof"	: "VFASPF(VCHI2/VDOF)",
        "log_doca"	: "log(DOCA(1,2))",
        "log_dira"	: "log(1-BPVDIRA)",
        "t"		: "BPVLTIME()",
        "t_chi2"	: "BPVLTCHI2()",
        "M_mumu"	: "MASS(1,2)",
        "M_mugamma"	: "MASS(1,3)",
        "ip"		: "BPVIP()",
        "ipchi2"	: "BPVIPCHI2()",
        "ips"		: "sqrt(BPVIPCHI2())",
        "eta"		: "ETA",
        "phi"		: "PHI",
        "muplus_pt"	: "CHILD(PT,1)",
        "muminus_pt"	: "CHILD(PT,2)",
        "gamma_pt"	: "CHILD(PT,3)",
        "muplus_pe"	: "CHILD(E,1)",
        "muminus_pe"	: "CHILD(E,2)",
        "gamma_pe"	: "CHILD(E,3)"
    }
else:
    DictTuple.DTF.dict.Variables = {
        "DTF_pt"	: "PT",
        "DTF_pe"	: "E",
        "DTF_m"		: "M",
        "DTF_vchi2"	: "BPV(VCHI2)",
        "DTF_vchi2dof"	: "VFASPF(VCHI2/VDOF)",
        "DTF_log_doca"	: "log(DOCA(1,2))",
        "DTF_log_dira"	: "log(1-BPVDIRA)",
        "DTF_t"		: "BPVLTIME()",
        "DTF_t_chi2"	: "BPVLTCHI2()",
        "DTF_M_mumu"	: "MASS(1,2)",
        "DTF_M_mugamma"	: "MASS(1,3)",
        "DTF_ip"	: "BPVIP()",
        "DTF_ipchi2"	: "BPVIPCHI2()",
        "DTF_ips"	: "sqrt(BPVIPCHI2())",
        "DTF_eta"	: "ETA",
        "DTF_phi"	: "PHI",
        "DTF_muplus_pt"	: "CHILD(PT,1)",
        "DTF_muminus_pt": "CHILD(PT,2)",
        "DTF_gamma_pt"	: "CHILD(PT,3)",
        "DTF_muplus_pe"	: "CHILD(E,1)",
        "DTF_muminus_pe": "CHILD(E,2)",
        "DTF_gamma_pe"	: "CHILD(E,3)"
    }

Bs0_Tuple.ToolList += [
    "TupleToolEventInfo",
    "TupleToolRecoStats"
    ]

BToolList = [
    "TupleToolPropertime",
    "TupleToolGeometry",
    "TupleToolDira",
    ]
MuToolList = [
    "TupleToolTrackInfo",
    "TupleToolAngles",
    "TupleToolGeometry",
    ]
GammaToolList = [
    "TupleToolPhotonInfo",
    "TupleToolCaloHypo",
    "TupleToolVeto/Pi0Veto",
    "TupleToolGamma/GammaMVA",
    "TupleToolTrackIsolation"
    ]


Bs0_Tuple.B.ToolList += BToolList
Bs0_Tuple.muplus.ToolList += MuToolList
Bs0_Tuple.muminus.ToolList += MuToolList
Bs0_Tuple.gamma.ToolList += GammaToolList


#### Isolations Variables

#### Long Track Bs2MuMu2017 Isolations:
#       the 3 variables TRKISOBDTFIRSTVALUE, TRKISOBDTSECONDVALUE, TRKISOBDTTHIRDVALUE
#       are the highest BDT score of the tracks w.r.t the muon.
Bs0_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs0_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs0_Tuple.muplus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs0_Tuple.muplus.IsoTool.Variables = -1
          
Bs0_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoTool"]
Bs0_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoTool")
Bs0_Tuple.muminus.IsoTool.WeightsFile = "BsMuMu_TrackIsolationBDT9vars_v2.xml"
Bs0_Tuple.muminus.IsoTool.Variables = -1
          
Bs0_Tuple.muplus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs0_Tuple.muplus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs0_Tuple.muplus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs0_Tuple.muplus.IsoToolVelo.Variables = -2
Bs0_Tuple.muplus.IsoToolVelo.TrackType = 1
          
Bs0_Tuple.muminus.ToolList+=["TupleToolTrackIsolationBDT/IsoToolVelo"]
Bs0_Tuple.muminus.addTool(TupleToolTrackIsolationBDT, name="IsoToolVelo")
Bs0_Tuple.muminus.IsoToolVelo.WeightsFile = "BsMuMu_VeloTrackIsolationBDT6vars_v2.xml"
Bs0_Tuple.muminus.IsoToolVelo.Variables = -2
Bs0_Tuple.muminus.IsoToolVelo.TrackType = 1


##### Gamma Selection
Bs0_Tuple.gamma.addTool(TupleToolVeto,name="Pi0Veto")
Bs0_Tuple.gamma.Pi0Veto.Particle= "gamma"
Bs0_Tuple.gamma.Pi0Veto.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs0_Tuple.gamma.Pi0Veto.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]

Bs0_Tuple.gamma.addTool(TupleToolGamma,name="GammaMVA")
Bs0_Tuple.gamma.GammaMVA.Veto["Pi0"]=["Phys/StdLoosePi02gg/Particles", "Phys/StdLoosePi02gee/Particles", "Phys/StdLoosePi024e/Particles"]
Bs0_Tuple.gamma.GammaMVA.Veto["Eta"]= ["Phys/StdLooseEta2gg/Particles"]
Bs0_Tuple.gamma.GammaMVA.BDTFileName="GammaMVA_BDT.weights.xml"


Bs0_Tuple.gamma.addTupleTool("TupleToolProtoPData")
Bs0_Tuple.gamma.TupleToolProtoPData.DataList = ["IsPhoton", "IsNotE", "IsNotH" , "CellID" , "CaloNeutralID" , "CaloNeutralSpd" ]

Bs0_Tuple.gamma.addTupleTool("TupleToolL0Calo")
Bs0_Tuple.gamma.TupleToolL0Calo.WhichCalo = "ECAL"

ttt = Bs0_Tuple.addTupleTool("TupleToolTrigger")
ttt.Verbose = True
ttt.TriggerList=['L0PhotonDecision']



#### MC Truth
MCTruth = Bs0_Tuple.addTupleTool("TupleToolMCTruth")
MCTruth.addTupleTool("MCTupleToolHierarchy")
MCTruth.addTupleTool("MCTupleToolAngles")




###### LoKi Variables
Bs0_LoKi = Bs0_Tuple.B.addTupleTool('LoKi::Hybrid::TupleTool/Bs0_LoKi')
#Bs0_LoKi.Preambulo = []
Bs0_LoKi.Variables = {
    "vchi2":	"BPV(VCHI2)",
    "vchi2dof":	"VFASPF(VCHI2/VDOF)",
    "log_doca":	"log(DOCA(1,2))",
    "log_dira":	"log(1-BPVDIRA)",
    "t":	"BPVLTIME()",
    "t_chi2":	"BPVLTCHI2()",
    "M_mumu":	"MASS(1,2)",
    "M_mugamma":"MASS(1,3)",
    }


LoKi_All = Bs0_Tuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
LoKi_All.Variables = {
    "ip":	"BPVIP()",
    "ipchi2":	"BPVIPCHI2()",
    "ips":	"sqrt(BPVIPCHI2())",
    "eta":	"ETA",
    "phi":	"PHI"
    }


####### Trigger
TrigVerb = True
TTT = Bs0_Tuple.B.addTupleTool("TupleToolTISTOS/TTT")
TTT.VerboseL0 = TrigVerb
TTT.VerboseHlt1 = TrigVerb
TTT.VerboseHlt2 = TrigVerb
TTT.FillL0 = True
TTT.FillHlt1 = True
TTT.FillHlt2 = True
TTT.OutputLevel = INFO
L0list = ["L0Muon",
          "L0DiMuon",
          "L0Photon",
          "L0Hadron"
]
Hlt1list = ['Hlt1TrackMVA',
            'Hlt1TwoTrackMVA',
            'Hlt1TrackMVALoose',
            'Hlt1TwoTrackMVALoose',
            'Hlt1TrackMuon',
            'Hlt1TrackMuonMVA',
            'Hlt1DiMuonHighMass',
            'Hlt1DiMuonLowMass',
            'Hlt1SingleMuonHighPT',
            'Hlt1DiMuonNoL0',
            "Hlt1TrackPhoton",
            "Hlt1SingleMuonNoIPD",
]
Hlt2list = ["Hlt2DiMuon",
            "Hlt2DiMuonDY",
            "Hlt2DiMuonZ",
            "Hlt2DiMuonLowMass",
            "Hlt2DiMuonB",
            "Hlt2DiMuonBTurbo",
            "Hlt2DiMuonDetached",
            "Hlt2DiMuonDetachedHeavy",
            "Hlt2DiMuonDetachedJPsi",
            "Hlt2DiMuonDetachedPsi2S",
            "Hlt2DiMuonJPsi",
            "Hlt2DiMuonJPsiHighPT",
            "Hlt2DiMuonJPsiTurbo",
            "Hlt2DiMuonPsi2S",
            "Hlt2DiMuonPsi2SHighPT",
            "Hlt2DiMuonPsi2STurbo",
            "Hlt2DiMuonSoft",
            "Hlt2EWDiMuonDY1",
            "Hlt2EWSingleMuonHighPt",
            "Hlt2EWSingleMuonLowPt",
            "Hlt2EWSingleMuonVHighPt",
            "Hlt2SingleMuon",
            "Hlt2SingleMuonHighPT",
            "Hlt2SingleMuonLowPT",
            "Hlt2SingleMuonRare",
            "Hlt2Topo2Body",
            "Hlt2Topo3Body",
            "Hlt2Topo4Body",
            "Hlt2TopoE2Body",
            "Hlt2TopoE3Body",
            "Hlt2TopoE4Body",
            "Hlt2TopoMu2Body",
            "Hlt2TopoMu3Body",
            "Hlt2TopoMu4Body",
            "Hlt2TopoMuE2Body",
            "Hlt2TopoMuE3Body",
            "Hlt2TopoMuE4Body",
            "Hlt2TopoMuMu2Body",
            "Hlt2TopoMuMu3Body",
            "Hlt2TopoMuMu4Body",
            "Hlt2TopoMuMuDD",
            "Hlt2Bs2PhiGamma",
            "Hlt2RadiativeBs2PhiGammaUnbiased",
            "Hlt2RadiativeIncHHGamma" 
]
tlist = L0list + Hlt1list + Hlt2list
tList = map(lambda x : x + "Decision", tlist)
TTT.TriggerList = tList



##########################
######### Sequence & algo
##########################

Bs0_seq = SelectionSequence('Bs0_seq', TopSelection = Bs2PhiGamma)
seq = GaudiSequencer('Bs0_Tuple_seq')

seq.Members += [sc.sequence()]
seq.Members += [Bs0_seq.sequence()]
seq.Members += [Bs0_Tuple]

dv.UserAlgorithms += [event_node_killer, seq]
