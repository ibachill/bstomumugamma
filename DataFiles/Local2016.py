from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = False
dv.EvtMax = 10000#-1
dv.DataType = "2016"
dv.InputType = 'mDST'
#Mag Down
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/Data_2016_Up.mdst'
    ]
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'cond-20161004'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "Ntuple_Data_Down.root"
