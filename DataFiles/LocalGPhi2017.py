from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = False
dv.EvtMax = -1
dv.DataType = "2017"
dv.InputType = 'DST'
#Mag Down
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/DataGPhi_2017_Up.dst'
    ]
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'cond-20170724'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Ntuple_GPhi_Up.root"
