from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = False
dv.EvtMax = -1
dv.DataType = "2016"
dv.InputType = 'DST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'cond-20161004'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "Ntuple_Norm.root"
