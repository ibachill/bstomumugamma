from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = False
dv.EvtMax = -1
dv.DataType = "2018"
dv.InputType = 'DST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'cond-20180202'
dv.DDDBtag = 'dddb-20171030-3'

dv.TupleFile = "Ntuple_GPhi.root"
