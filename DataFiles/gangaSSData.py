import argparse
parser = argparse.ArgumentParser(description="Ganga job for Data")

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')

parser.add_argument('year', type = int,
                    choices = [2011, 2012, 2015, 2016, 2017],
                    help = 'Year of data-taking to run over')

parser.add_argument('cluster', type = int,
                    choices = range(15),
                    help = 'Will run jobs [cluster*500:(cluster+1)*500]')

args = parser.parse_args()

year = args.year
polarity = args.polarity
cl = args.cluster


j = Job(name='MyDaVinciNew')

j.name = 'SS Data_'+polarity+'_'+str(year)+'_'+str(cl)
j.comment = 'SS Data '+polarity+' '+str(year)+' '+str(cl)
myApp = GaudiExec()
myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v41r3"
j.application = myApp
j.application.options = ['Data'+str(year)+'.py', '/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DecOptions/SSDecOpt.py']
j.inputfiles += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/Weights/GammaMVA_BDT.weights.xml']

if year == 2012:
    data=BKQuery(
        '/LHCb/Collision12/Beam4000GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco14/Stripping21/90000000/DIMUON.DST',
        dqflag=['OK']).getDataset()
elif year == 2011:
    data=BKQuery(
        '/LHCb/Collision11/Beam3500GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST',
        dqflag=['OK']).getDataset()


j.inputdata = data[500*cl:500*(cl+1)]

j.splitter = SplitByFiles(
    filesPerJob=12,
    ignoremissing=True
    )
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
