import argparse
parser = argparse.ArgumentParser(description="Ganga job for Data")

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')

parser.add_argument('year', type = int,
                    choices = [2015, 2016, 2017, 2018],
                    help = 'Year of data-taking to run over')

parser.add_argument('cluster', type = int,
                    choices = range(25),
                    help = 'Will run jobs [cluster*5000:(cluster+1)*5000]')

args = parser.parse_args()

year = args.year
polarity = args.polarity
cl = args.cluster


j = Job()

j.name = 'Norm_'+polarity+'_'+str(year)+'_'+str(cl)
j.comment = 'Norm '+polarity+' '+str(year)+' '+str(cl)

myApp = GaudiExec()
if year < 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v41r4p5"
    j.application = myApp
    j.application.platform = 'x86_64-slc6-gcc49-opt'
elif year == 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v42r8p1"
    j.application = myApp
    j.application.platform = 'x86_64-centos7-gcc62-opt'
elif year == 2018:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v44r9"
    j.application = myApp
    j.application.platform = 'x86_64-centos7-gcc7-opt'


j.application.options = ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DataFiles/Norm'+str(year)+'.py']
j.application.options += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DecOptions/Run2NormDecOpt.py']
j.inputfiles += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/Weights/GammaMVA_BDT.weights.xml']

if year == 2015:
    data=BKQuery(
        '/LHCb/Collision15/Beam6500GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco15a/Stripping24r1/90000000/DIMUON.DST',
        dqflag=['OK']).getDataset()
elif year == 2016:
    data=BKQuery(
        '/LHCb/Collision16/Beam6500GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco16/Stripping28r1/90000000/DIMUON.DST',
        dqflag=['OK']).getDataset()
elif year == 2017:
    data=BKQuery(
        '/LHCb/Collision17/Beam6500GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST',
        dqflag=['OK']).getDataset()
elif year == 2018:
    data=BKQuery(
        '/LHCb/Collision18/Beam6500GeV-VeloClosed-Mag'+polarity+'/Real Data/Reco18/Stripping34/90000000/DIMUON.DST',
        dqflag=['OK']).getDataset()
    for f in data.files: #remove faulty LFN
        if f.lfn == '/lhcb/LHCb/Collision18/DIMUON.DST/00075559/0000/00075559_00003327_1.dimuon.dst' or f.lfn == '/lhcb/LHCb/Collision18/DIMUON.DST/00075559/0001/00075559_00017779_1.dimuon.dst':
            print("Removing one file from dataset!!!")
            j.comment += " CHECK nevts!"
            data.files.remove(f)



j.inputdata = data[5000*cl:5000*(cl+1)]
#j.inputdata = data[100*cl:100*(cl+1)]
#j.name = 'Norm_'+polarity+'_'+str(year)+'_'+str(cl)+'_TEST'
#j.comment = 'Norm '+polarity+' '+str(year)+' '+str(cl)+' TEST'

j.splitter = SplitByFiles(
    filesPerJob=50,
    ignoremissing=False
)


j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
