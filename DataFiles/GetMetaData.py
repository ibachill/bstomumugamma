import argparse
parser = argparse.ArgumentParser(description="Get TCK luminosities for a given year")

parser.add_argument('year', type = int,
                    choices = [2015, 2016, 2017, 2018],
                    help = 'Year of data-taking to run over')

args = parser.parse_args()

year = args.year

strippings = {
    2015: "/LHCb/Collision15/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco15a/Stripping24r1p1/90000000/LEPTONIC.MDST",
    2016: "/LHCb/Collision16/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco16/Stripping28r1p1/90000000/LEPTONIC.MDST",
    2017: "/LHCb/Collision17/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco17/Stripping29r2/90000000/LEPTONIC.MDST",
    2018: "/LHCb/Collision18/Beam6500GeV-VeloClosed-Mag{}/Real Data/Reco18/Stripping34/90000000/LEPTONIC.MDST"
}


pathu, pathd = (strippings[year].format('Up'), strippings[year].format('Down'))

datasetMu = BKQuery(pathu, dqflag=['OK']).getDatasetMetadata()
datasetMd = BKQuery(pathd, dqflag=['OK']).getDatasetMetadata()

LumiperTCK = {'TOT':0}

for metadata in datasetMu['Value'].values()+datasetMd['Value'].values():
    TCK = metadata['TCK']
    if TCK in LumiperTCK:
        LumiperTCK[TCK] += metadata['Luminosity']
    else:
        LumiperTCK[TCK] = metadata['Luminosity']
    LumiperTCK['TOT'] += metadata['Luminosity']


print "--- Dataset = {}".format(year)
print "TCK, Lumi [pb^-1]"
for (k, v) in LumiperTCK.iteritems():
    print k, v/1000000
