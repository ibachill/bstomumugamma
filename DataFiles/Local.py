from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = False
dv.EvtMax = 1000#-1
dv.DataType = "2012"
dv.InputType = 'DST'
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/Full_2012_Down.dst'
    ]
dv.Lumi = not dv.Simulation
# MC Production Conditions; can be find with
#  > export PATH=$PATH:/afs/cern.ch/user/i/ibelyaev/public/scripts
#  > lhcb-proxy-init
#  > get_bookkeeping_info 13142201
dv.CondDBtag = 'cond-20141107'
dv.DDDBtag = 'dddb-20130929-1'

dv.TupleFile = "Ntuple_Data_Down.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
