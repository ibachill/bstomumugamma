from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = False
dv.EvtMax = -1
dv.DataType = "2017"
dv.InputType = 'mDST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'cond-20170724'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "Ntuple_Data.root"
