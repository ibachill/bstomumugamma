from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = False
dv.EvtMax = 5000#-1
dv.DataType = "2016"
dv.InputType = 'DST'
#Mag Down
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/DataGPhi_2016_Down.dst'
    ]
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'cond-20161004'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "Ntuple_GPhi_Down.root"
