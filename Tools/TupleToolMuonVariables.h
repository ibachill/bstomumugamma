// $Id: TupleToolMuonVariables.h 
#ifndef TUPLETOOLMUONVARIABLES_H 
#define TUPLETOOLMUONVARIABLES_H 1

// Include files
#include "Kernel/IParticleTupleTool.h" 
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IRelatedPVFinder.h"
#include "Event/RecVertex.h"
#include "LoKi/ParticleCuts.h"
#include "LoKi/AParticleCuts.h"
#include "LoKi/ParticleContextCuts.h"
#include "LoKi/AParticles.h"
#include "TH1D.h"

#include "Kernel/Track2MC.h"
#include "Relations/IRelationWeighted2D.h"


using namespace std;

/** @class TupleToolMuonVariables TupleToolMuonVariables.h

 * 
 *  @author Fatima Soomro
 *  @date   2011-08-08
 */

class IDistanceCalculator;
class IDVAlgorithm;

class TupleToolMuonVariables : public TupleToolBase, virtual public IParticleTupleTool {
 public: 
  /// Standard constructor
  TupleToolMuonVariables( const std::string& type, 
			  const std::string& name,
			  const IInterface* parent );
  /// Loop over differnt conesizes and fill the variables into the tuple
  virtual StatusCode fill( const LHCb::Particle*
                           , const LHCb::Particle*
                           , const std::string&
                           , Tuples::Tuple& ) override;
  
  
  StatusCode fillTrackHitInfo(const LHCb::Particle *part, std::string, Tuples::Tuple&);
  StatusCode fillTrackInfo(Tuples::Tuple& tuple );
  StatusCode fillVeloTracksInfo(Tuples::Tuple& tuple);
  StatusCode fillPVList(Tuples::Tuple&);//fill the primary vertices of the event
  StatusCode fillBposmomCov(const LHCb::Particle*, std::string, Tuples::Tuple&);//fill 7x7 B pos-mom Cov matrix

  void trkPVassociator(std::vector<double>&,std::vector<double>&,std::vector<int>&,std::string);
  int getAssociatedPV(const LHCb::Track*,const std::vector<double>&,const std::vector<double>&, const std::vector<int>&);

  const LHCb::MCParticle* originof( const LHCb::MCParticle* ) ;
  
  virtual ~TupleToolMuonVariables( ); ///< Destructor
  
  virtual StatusCode initialize() override;    ///< Algorithm initialization
  virtual StatusCode finalize () override;
  
 protected:

 private:

  bool m_isMC;
  double m_typeOrigin;
  double m_endVertices;
  int m_tracktype;


  //Particle2MCLinker* m_pLinker;
  std::string m_particlePaths;
  
  std::string m_TracksPath;
  std::string m_ParticlePath;
  std::string m_PVInputLocation;		\


  IDVAlgorithm* m_dva;
  IPVReFitter* m_pvReFitter;
  const IDistanceCalculator* m_dist;

  void dumpIsoVariables(const LHCb::Particle* B, Tuples::Tuple& tuple );  
  void IsHltGood(Gaudi::XYZPoint o, Gaudi::XYZVector p, Gaudi::XYZPoint o_mu,Gaudi::XYZVector p_mu, Gaudi::XYZPoint PV, bool& hltgood, double& fc);
  double pointer (Gaudi::XYZVector vtx, Gaudi::XYZVector P_tr,  Gaudi::XYZVector P_mu);
  double ratio( double p1, double p2);
  void InCone(Gaudi::XYZPoint o1, Gaudi::XYZVector p1,Gaudi::XYZPoint o2,
              Gaudi::XYZVector p2, Gaudi::XYZPoint& vtx, double& doca, double& angle);
  void closest_point(Gaudi::XYZPoint o,Gaudi::XYZVector p, Gaudi::XYZPoint o_mu,Gaudi::XYZVector p_mu, 
                     Gaudi::XYZPoint& close1, Gaudi::XYZPoint& close2, Gaudi::XYZPoint& vertex, bool& fail);
  double arcosine (Gaudi::XYZVector, Gaudi::XYZVector);



  void get_decayTree(const LHCb::MCParticle* mcp,vector<int>& key_vec);
  std::vector<int> _mup_mcdecaytree_vec;
  std::vector<int> _mum_mcdecaytree_vec;
  bool isNonIso(const LHCb::MCParticle* mcp,TString mu_flag);
  
};



#endif // TUPLETOOLMUONVARIABLES_H
