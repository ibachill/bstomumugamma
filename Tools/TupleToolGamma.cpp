// $Id: TupleToolGamma.cpp
// Include files

// local
#include "Event/Particle.h"
#include "TupleToolGamma.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
//#include "TMVA/Config.h"
#include <iostream>

using namespace Gaudi;
using namespace LHCb;
using namespace TMVA;

// Declaration of the Tool Factory
//DECLARE_TOOL_FACTORY( TupleToolGamma )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolGamma::TupleToolGamma( const std::string& type,
				const std::string& name,
				const IInterface* parent )
: TupleToolBase ( type, name , parent ),
  m_PhotonID(22)
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("BDTFileName", m_BDTFileName="./GammaMVA_BDT.weights.xml");
  declareProperty("Veto", m_veto);
}

StatusCode TupleToolGamma::initialize()
{
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  // m_check = tool<IParticleVeto>("ParticleVeto","ParticleVeto" ,this );

  reader = new TMVA::Reader("V:True", 1);
  
  reader->AddVariable("isNotH := gamma_CaloHypo_isNotH",		&isNotH );
  reader->AddVariable("isNotE := gamma_CaloHypo_isNotE",		&isNotE );
  reader->AddVariable("Ecl_log := log(gamma_CaloHypo_isPhoton_Ecl)",	&Ecl_log );
  reader->AddVariable("Fr2_log := log(gamma_CaloHypo_isPhoton_Fr2)",	&Fr2_log );
  reader->AddVariable("PrsFr2 := gamma_CaloHypo_isPhoton_PrsFr2",	&PrsFr2 );
  reader->AddVariable("Fr2r4 := gamma_CaloHypo_isPhoton_Fr2r4",		&Fr2r4 );
  reader->AddVariable("Asym := fabs(gamma_CaloHypo_isPhoton_Asym)",	&Asym );
  reader->AddVariable("PrsAsym := fabs(gamma_CaloHypo_isPhoton_PrsAsym)",&PrsAsym );
  reader->AddVariable("Kappa := gamma_CaloHypo_isPhoton_Kappa",		&Kappa );
  reader->AddVariable("Eseed := gamma_CaloHypo_isPhoton_Eseed",		&Eseed );
  reader->AddVariable("E2 := gamma_CaloHypo_isPhoton_E2",		&E2 );
  reader->AddVariable("PrsM := gamma_CaloHypo_isPhoton_PrsM",		&PrsM );
  reader->AddVariable("PrsM15 := gamma_CaloHypo_isPhoton_PrsM15",	&PrsM15 );
  reader->AddVariable("PrsM30 := gamma_CaloHypo_isPhoton_PrsM30",	&PrsM30 );
  reader->AddVariable("PrsM45 := gamma_CaloHypo_isPhoton_PrsM45",	&PrsM45 );
  reader->AddVariable("PrsEmax := gamma_CaloHypo_isPhoton_PrsEmax",	&PrsEmax );
  reader->AddVariable("PrsE2 := gamma_CaloHypo_isPhoton_PrsE2",		&PrsE2 );
  reader->AddVariable("PE_log := log(gamma_PE)", 		&PE_log );
  reader->AddVariable("PT_log := log(gamma_PT)", 		&PT_log );
  reader->AddVariable("n_pi0 := gamma_n_pi0",			&n_pi0_f);
  reader->AddVariable("n_eta := gamma_n_eta",			&n_eta_f);
  reader->AddVariable("dm_pi0 := gamma_dm_pi0",			&dm_pi0);
  reader->AddVariable("dm_eta := gamma_dm_eta",			&dm_eta);

  reader->BookMVA( "GammaMVA_BDT",  m_BDTFileName);

  m_estimator = tool<ICaloHypoEstimator>("CaloHypoEstimator","CaloHypoEstimator",this);

  return sc;


}

//=============================================================================

StatusCode TupleToolGamma::fill( const Particle* ,
				 const Particle* P,
				 const std::string& head,
				 Tuples::Tuple& tuple )
{
  bool filltuple = true;
  if (P){
    if (P->particleID().pid() == m_PhotonID &&  isPureNeutralCalo(P)) {
      
      const LHCb::ProtoParticle* proto = P->proto();
      if(NULL == proto)return StatusCode::SUCCESS;

      // bool vetoPi0 = false;
      // bool vetoEta = false;      
      // for ( std::map<std::string,std::vector<std::string> >::const_iterator i = m_veto.begin(); m_veto.end() != i; ++i ) {
      // 	const std::string&              flag = i->first;
      // 	const std::vector<std::string>& cont = i->second;
      // 	const bool veto = m_check->foundOverlap( P, cont );
      // 	if (flag == "Pi0") vetoPi0 = veto;
      // 	if (flag == "Eta") vetoEta = veto;
      // }
      
      double pt = P->pt();
      double pe = P->momentum().E();
      PT_log = pt > 0 ? log(pt) : -99.;
      PE_log = pe > 0 ? log(pe) : -99.;



      // gamma_CL = P->confLevel();
      // gamma_VetoPi0 = vetoPi0;
      // gamma_VetoEta = vetoEta;

      using namespace CaloDataType;
      const SmartRefVector<LHCb::CaloHypo>& hypos = proto->calo();
      const LHCb::CaloHypo* hypo   = NULL;
      for( SmartRefVector<CaloHypo>::const_iterator ih = hypos.begin() ; hypos.end() != ih ; ++ih)
	if( isPureNeutralCalo( P ) )
	  hypo = *ih;
      isNotH   = m_estimator->data(hypo, (DataType) 53 ,0.);
      isNotE   = m_estimator->data(hypo, (DataType) 54 ,0.);
      Ecl_log = log(m_estimator->data(hypo, (DataType) isPhotonEcl ,0.));
      Fr2_log = log(m_estimator->data(hypo, (DataType) isPhotonFr2 ,0.));
      PrsFr2 = m_estimator->data(hypo, (DataType) isPhotonPrsFr2 ,0.);
      Fr2r4 = m_estimator->data(hypo, (DataType) isPhotonFr2r4 ,0.);
      Asym = fabs(m_estimator->data(hypo, (DataType) isPhotonAsym ,0.));
      PrsAsym = fabs(m_estimator->data(hypo, (DataType) isPhotonPrsAsym ,0.));
      Kappa = m_estimator->data(hypo, (DataType) isPhotonKappa ,0.);
      Eseed = m_estimator->data(hypo, (DataType) isPhotonEseed ,0.);
      E2 = m_estimator->data(hypo, (DataType) isPhotonE2 ,0.);
      PrsM = m_estimator->data(hypo, (DataType) isPhotonPrsM ,0.);
      PrsM15 = m_estimator->data(hypo, (DataType) isPhotonPrsM15 ,0.);
      PrsM30 = m_estimator->data(hypo, (DataType) isPhotonPrsM30 ,0.);
      PrsM45 = m_estimator->data(hypo, (DataType) isPhotonPrsM45 ,0.);
      PrsEmax = m_estimator->data(hypo, (DataType) isPhotonPrsEmax ,0.);
      PrsE2 = m_estimator->data(hypo, (DataType) isPhotonPrsE2 ,0.);

      dm_pi0 = 60; //Mass Window of Pi02gg container
      dm_eta = 105;
      double dm_pi0_temp = 60;
      double dm_eta_temp = 105;
      double MM = 0; // Pi0 Measured Mass
      double Mean_dm_pi0 = 0;
      double Mean_dm_eta = 0;
      int n_pi0 = 0;
      int n_eta = 0;
      for ( std::map<std::string,std::vector<std::string> >::const_iterator i = m_veto.begin(); m_veto.end() != i; ++i ) {

	const std::string&              flag = i->first;
	const std::vector<std::string>& cont = i->second;
	std::vector<std::string>& m_cont = const_cast<std::vector<std::string>&>(cont);

	// Loop over list of containers
	for( std::vector<std::string>::iterator i=m_cont.begin();i!=m_cont.end();++i){
	  
	  std::string container = *i;
	  if( container != "" ){
	    if ( exist<LHCb::Particle::Range>( container ) ){
	      
	      LHCb::Particle::Range m_parts;
	      m_parts = get<LHCb::Particle::Range> ( container );
	      int i1=container.find("Phys/");
	      int i2=container.find("/Particles");
	      if ( i1 != (int) std::string::npos ) i1 +=5;
	      if ( i2 != (int) std::string::npos ) i2 -=12;
	      std::string cont = container.substr(i1,i2);

	      for( LHCb::Particle::Range::const_iterator j = m_parts.begin();m_parts.end() != j;++j){

		std::vector<const LHCb::Particle*> vp1 = getTree(P);
		std::vector<const LHCb::Particle*> vp2 = getTree(*j);
		
		for( std::vector<const LHCb::Particle*>::const_iterator pi = vp1.begin();vp1.end() != pi;++pi){    
		  for( std::vector<const LHCb::Particle*>::const_iterator pj = vp2.begin();vp2.end() != pj;++pj){

		    const LHCb::Particle* p1 = *pi;
		    const LHCb::Particle* p2 = *pj;
		    const LHCb::ProtoParticle* pp1 = p1->proto();
		    const LHCb::ProtoParticle* pp2 = p2->proto();
		    if( isPureNeutralCalo(p1) && isPureNeutralCalo(p2) ) {
		      int id1 = (int)pp1->info(LHCb::ProtoParticle::CaloNeutralID,0);
		      int id2 = (int)pp2->info(LHCb::ProtoParticle::CaloNeutralID,0);
		      if ( id1 == id2 && 0 != id1  ) {
			MM = (*j)->momentum().M();
			if ((flag == "Pi0") && (MM > 10)) { //avoid meaningless Pi0
			  n_pi0 +=1;
			  // std::cout << (*j)->particleID() << std::endl;
			  // std::cout << "  mass " << MM << std::endl;
			  dm_pi0_temp = fabs(MM - 134.9766); // Pi0_Mass
			  Mean_dm_pi0 += dm_pi0_temp;
			  if (dm_pi0_temp < dm_pi0) { dm_pi0 = dm_pi0_temp; }
			}
			if ((flag == "Eta") && (MM > 10)) {
			  n_eta += 1;
			  dm_eta_temp = fabs(MM - 547.862); // Eta_Mass
			  Mean_dm_eta += dm_eta_temp;
			  if (dm_eta_temp < dm_eta) { dm_eta = dm_eta_temp; }
			}
		      }
		    } else if ( !isPureNeutralCalo(p1) && !isPureNeutralCalo(p2) ) {
		      if ( pp1 == pp2 ) {
			MM = (*j)->momentum().M();
			if ((flag == "Pi0") && (MM > 10)) {
			  n_pi0 +=1;
			  dm_pi0_temp = fabs(MM - 134.9766); // Pi0_Mass
			  Mean_dm_pi0 += dm_pi0_temp;
			  if (dm_pi0_temp < dm_pi0) { dm_pi0 = dm_pi0_temp; } 
			}
			if ((flag == "Eta") && (MM > 10)) {
			  n_eta += 1;
			  dm_eta_temp = fabs(MM - 547.862); // Eta_Mass
			  Mean_dm_eta += dm_eta_temp;
			  if (dm_eta_temp < dm_eta) { dm_eta = dm_eta_temp; }
			}
		      }
		    } else if ( isPureNeutralCalo(p1) && !isPureNeutralCalo(p2) ) {
		      if( (pp2->calo()).empty() ) continue;
		      int id1 = (int)pp1->info(LHCb::ProtoParticle::CaloNeutralID,0);
		      int id2 = (int)pp2->info(LHCb::ProtoParticle::CaloNeutralID,0);
		      if ( id1 == id2 && 0 != id1  ) {
			MM = (*j)->momentum().M();
			if ((flag == "Pi0") && (MM > 10)) {
			  n_pi0 += 1;
			  dm_pi0_temp = fabs(MM - 134.9766); // Pi0_Mass
			  Mean_dm_pi0 += dm_pi0_temp;
			  if (dm_pi0_temp < dm_pi0) { dm_pi0 = dm_pi0_temp; }
			}
			if ((flag == "Eta") && (MM > 10)) {
			  n_eta += 1;
			  dm_eta_temp = fabs(MM - 547.862); // Eta_Mass
			  Mean_dm_eta += dm_eta_temp;
			  if (dm_eta_temp < dm_eta) { dm_eta = dm_eta_temp; }
			}
		      }
		    }	    
		  }
		}
	      }
	    }
	    // else {
	    //  std::cout << "Container " << container << " not found" << std::endl;
	    //  //continue;
	    // } 
	  }
	}
      }
      
      n_pi0_f = n_pi0;
      n_eta_f = n_eta;
      mean_dm_pi0_f = ( Mean_dm_pi0 == 0) ? 0 : Mean_dm_pi0/n_pi0;
      mean_dm_eta_f = ( Mean_dm_eta == 0) ? 0 : Mean_dm_eta/n_eta;

      Double_t BDT_value = -9999.;
      filltuple &= tuple->column( head+"_isNotH", isNotH );
      filltuple &= tuple->column( head+"_isNotE", isNotE );
      filltuple &= tuple->column( head+"_Ecl_log", Ecl_log );
      filltuple &= tuple->column( head+"_Fr2_log", Fr2_log );
      filltuple &= tuple->column( head+"_PrsFr2", PrsFr2 );
      filltuple &= tuple->column( head+"_Fr2r4", Fr2r4 );
      filltuple &= tuple->column( head+"_Asym", Asym );
      filltuple &= tuple->column( head+"_PrsAsym", PrsAsym );
      filltuple &= tuple->column( head+"_Kappa", Kappa );
      filltuple &= tuple->column( head+"_Eseed", Eseed );
      filltuple &= tuple->column( head+"_E2", E2 );
      filltuple &= tuple->column( head+"_PrsM", PrsM );
      filltuple &= tuple->column( head+"_PrsM15", PrsM15 );
      filltuple &= tuple->column( head+"_PrsM30", PrsM30 );
      filltuple &= tuple->column( head+"_PrsM45", PrsM45 );
      filltuple &= tuple->column( head+"_PrsEmax", PrsEmax );
      filltuple &= tuple->column( head+"_PrsE2", PrsE2 );
      filltuple &= tuple->column( head+"_PE_log", PE_log );
      filltuple &= tuple->column( head+"_PT_log", PT_log );
      filltuple &= tuple->column( head+"_n_pi0", n_pi0_f );
      filltuple &= tuple->column( head+"_n_eta", n_eta_f );
      filltuple &= tuple->column( head+"_dm_pi0", dm_pi0 );
      filltuple &= tuple->column( head+"_dm_eta", dm_eta );
      filltuple &= tuple->column( head+"_mean_dm_pi0", mean_dm_pi0_f );
      filltuple &= tuple->column( head+"_mean_dm_eta", mean_dm_eta_f );

      BDT_value = reader->EvaluateMVA("GammaMVA_BDT");
      filltuple &= tuple->column( head+"_GammaMVA", BDT_value );

	
    }
  }
  return StatusCode(filltuple);
}


std::vector<const LHCb::Particle*> TupleToolGamma::getTree(const LHCb::Particle* P){
  std::vector<const LHCb::Particle*> tree;
  if( P->proto() ){
    tree.push_back( P );
    return tree;
  }else{
    const LHCb::Particle::ConstVector& daughters = P->daughtersVector();
    for( LHCb::Particle::ConstVector::const_iterator d=daughters.begin();daughters.end()!=d;++d){
      if( (*d)->proto() )tree.push_back( *d );
      else{        
        const std::vector<const LHCb::Particle*>& t=getTree( *d );
        for (std::vector<const LHCb::Particle*>::const_iterator i=t.begin();t.end()!=i;++i){
          tree.push_back( *i );
        } 
      }
    }
  }
  return tree;
}
