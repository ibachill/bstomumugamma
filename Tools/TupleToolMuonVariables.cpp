// Include files
//#include "GaudiKernel/ToolFactory.h" 
#include "Event/Particle.h"  
// kernel 
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/IParticle2MCAssociator.h"
#include <Kernel/IDistanceCalculator.h>
#include "Kernel/IPVReFitter.h"
//#include "Kernel/IOnOffline.h"
#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>

// MC stuff
#include "Event/GenHeader.h" 
#include "Event/MCHeader.h" 

#include "TupleToolMuonVariables.h"
#include "TMath.h"
#include "TFile.h"
#include "TString.h"
#include <TROOT.h>
#include <TObject.h>
#include "TH1D.h"
#include "Kernel/IRelatedPVFinder.h"
#include "Math/Boost.h"

//#include "GaudiKernel/DeclareFactoryEntries.h"

#include <iostream>


using namespace std;
using namespace LHCb;

 
//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolMuonVariables
//
// 2011-08-08 : Fatima Soomro 
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
//DECLARE_TOOL_FACTORY( TupleToolMuonVariables )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolMuonVariables::TupleToolMuonVariables( const std::string& type,
						const std::string& name,
						const IInterface* parent) 
: TupleToolBase ( type, name , parent )
  , m_dva(0)
  , m_dist(0)
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty( "tracktype", m_tracktype = 3,
                   "Set the type of tracks which are considered inside the cone (default = 3)");

  declareProperty("ParticlePath",   m_ParticlePath="/Event/Phys/StdAllNoPIDsPions/Particles");
  declareProperty("PVInputLocation",m_PVInputLocation = LHCb::RecVertexLocation::Primary);
  declareProperty("TrackContainer",   m_TracksPath = LHCb::TrackLocation::Default); // default is "Rec/Track/Best "


  
}
//=============================================================================
// Destructor
//=============================================================================
TupleToolMuonVariables::~TupleToolMuonVariables(){} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode TupleToolMuonVariables::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  
  if ( sc.isFailure() ) return sc;
  
  
  m_dva = Gaudi::Utils::getIDVAlgorithm ( contextSvc() ) ;
  if (0==m_dva) return Error("Couldn't get parent DVAlgorithm", 
                             StatusCode::FAILURE);  

  
  m_dist = m_dva->distanceCalculator ();
  if( !m_dist ){
    return Error("Unable to retrieve the IDistanceCalculator tool");
  }
  

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  return StatusCode::SUCCESS;

}


//=============================================================================
// Fill the tuple add comments here ... 
//=============================================================================
StatusCode TupleToolMuonVariables::fill( const LHCb::Particle *, const LHCb::Particle *part,
					 const std::string &  	head, Tuples::Tuple &  	tuple	) {


  const std::string prefix=fullName(head);
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Fill" << endmsg;
  

  if ( abs(part->particleID().pid()) ==13 
       || abs( part->particleID().pid() )  == 11 )  // only for muons/electrons
    {  
      StatusCode scTrackInfo = fillTrackHitInfo(part, prefix, tuple);
      if(!scTrackInfo) return scTrackInfo;
    }
  
  if ( abs(part->particleID().pid()) >500) {
     
    //Dump LT tracks info  
    StatusCode fillTrack = fillTrackInfo( tuple ); 
    if (!fillTrack) return fillTrack;

    StatusCode fillVeloTrack = fillVeloTracksInfo( tuple );	
    if (!fillVeloTrack) return fillVeloTrack;

    //MR: Dump primary vertices info
    StatusCode fillPVList_ = fillPVList(tuple);
    if (!fillPVList_) return fillPVList_;

    //MR: Dump B pos and mom covariance matrix
    StatusCode fillBposmonCov_ = fillBposmomCov(part, prefix, tuple);
    if (!fillBposmonCov_) return fillBposmonCov_;

    //    StatusCode fillIsoVar = 
dumpIsoVariables(part, tuple);
//if (!fillIsoVar) return fillIsoVar;
  }  
 
  return StatusCode::SUCCESS;
  
}





//=============================================================================
// Track info, for muons only
//=============================================================================
StatusCode TupleToolMuonVariables::fillTrackHitInfo(const LHCb::Particle *part,
						    const std::string prefix,
						    Tuples::Tuple& tuple ){

  if( !part->isBasicParticle() )    return StatusCode::SUCCESS;
  //std::cout<<" Track info, for muons only "<<std::endl;
  if( abs(part->particleID().pid()) == 13 
      || abs( part->particleID().pid() )  == 11 ) {
    debug()<<"particle is "<<part->particleID().pid()<<endmsg; }
  else return StatusCode::SUCCESS;
  
  const LHCb::ProtoParticle* protop = part->proto();
  if(!protop) return StatusCode::SUCCESS;
  const LHCb::Track* track = protop->track();
  if(!track) return StatusCode::SUCCESS;
  int nVeloIDs(0), nITIDs(0), nOTIDs(0), nTTIDs(0);
  if(track){
    const std::vector<LHCb::LHCbID>& vids = track->lhcbIDs(); 

    for (std::vector<LHCb::LHCbID>::const_iterator iter = vids.begin() ; 
         iter != vids.end() ; ++iter)
      {
	if( iter->isVelo()) nVeloIDs++;
	if( iter->isTT()) nTTIDs++;
	if( iter->isIT()) nITIDs++;
	if( iter->isOT()) nOTIDs++;
      }
  }// if track
  
  StatusCode test = tuple->column( prefix+"_nVeloIDs", nVeloIDs);
  test &= tuple->column( prefix+"_nITIDs", nITIDs);
  test &= tuple->column( prefix+"_nTTIDs", nTTIDs);
  test &= tuple->column( prefix+"_nOTIDs", nOTIDs);
  
  if(test) return StatusCode::SUCCESS;
  else return StatusCode::FAILURE;
  
}




StatusCode TupleToolMuonVariables::fillTrackInfo(Tuples::Tuple& tuple ){
  bool test =true;
  const LHCb::MCParticle* MCp = NULL;
  const LHCb::MCParticle* ancestor =NULL;
  
  const int NMaxEVBpart = 500;

  //Here's where I get my hands on the LHCbs particles
  LHCb::Particles*  parts = get<LHCb::Particles>(m_ParticlePath);
  if (!parts) {
    error() << " Failed to get particles container " << endmsg;
    return StatusCode::SUCCESS;
  }
  
  int howManyParts(0);
  bool matched = kFALSE;
  bool hasMother = kFALSE;
  bool hasGranny = kFALSE;
  bool hasMotGranny = kFALSE;
  bool hasGraGranny = kFALSE;
  
  vector<double> par_pos_x, par_pos_y, par_pos_z, par_chg;
  vector<double> par_fmpos_x, par_fmpos_y, par_fmpos_z;//fm=first measurement
  vector<double> par_pos_covxx,par_pos_covyy,par_pos_covzz,par_pos_covxy,par_pos_covxz,par_pos_covyz;
  //pos vs mom Cov matrix of tracks (6x6)
  vector<double> par_px_cov00,par_px_cov11,par_px_cov22,par_px_cov33,par_px_cov44,par_px_cov55;
  vector<double> par_px_cov01,par_px_cov02,par_px_cov12,par_px_cov34,par_px_cov35,par_px_cov45;
  vector<double> par_px_cov03,par_px_cov04,par_px_cov05,par_px_cov13,par_px_cov14,par_px_cov15,par_px_cov23,par_px_cov24,par_px_cov25;

  vector<double> par_px, par_py, par_pz, par_pt, par_p;
  vector<double> par_slope_covxx,par_slope_covyy,par_slope_covxy;//NB: slope c.v. derivable from mom vec

  vector<double> par_NNe,par_NNmu,par_NNpi,par_NNk,par_NNp,par_NNghost;//MR: PID ProbNN
  vector<double> par_DLLe,par_DLLmu,par_DLLk,par_DLLp;//MR: PID DLL
  vector<double> par_isMuon;//MR: isMuon
  
  vector<int> par_typ;
  vector<double> par_gho, par_chi, par_clo, par_ip, par_ips;
  vector<bool> par_hlt;

  vector<int> par_PVind;
  //initialize the vectors for track<-->PV association
  std::vector<double> trkPV_pz, trkPV_gho;
  std::vector<int> trkPV_num;
  trkPVassociator(trkPV_pz,trkPV_gho,trkPV_num,"long");

  
  vector<bool> MC_match, MC_par_hasMother, 
    MC_mot_hasMother, MC_gra_hasMother,
    MC_mogra_hasMother;
  
  vector<double> MC_origin_id, MC_ori_vtx_y, MC_ori_vtx_z, MC_ori_vtx_x;
  vector<int> MC_ori_type;
  vector<double> MC_par_py, MC_par_pz, MC_par_px, MC_par_M, MC_par_E;
  //vector<double> MC_par_trueID;
  vector<int> MC_par_trueID,MC_par_isnoniso_mup,MC_par_isnoniso_mum;
  
  vector<double> MC_mot_ori_vtx_y, MC_mot_ori_vtx_z, MC_mot_ori_vtx_x;
  vector<double> MC_mot_py, MC_mot_pz, MC_mot_px, MC_mot_M, MC_mot_E;
  //vector<double> MC_mot_trueID;
  vector<int> MC_mot_trueID;

  vector<double> MC_gra_ori_vtx_y, MC_gra_ori_vtx_z, MC_gra_ori_vtx_x;
  vector<double> MC_gra_py, MC_gra_pz, MC_gra_px, MC_gra_M, MC_gra_E;
  //vector<double> MC_gra_trueID;
  vector<int> MC_gra_trueID;

  vector<double> MC_mogra_ori_vtx_y, MC_mogra_ori_vtx_z, MC_mogra_ori_vtx_x;
  vector<double> MC_mogra_py, MC_mogra_pz, MC_mogra_px, MC_mogra_M, MC_mogra_E;
  //vector<double> MC_mogra_trueID;
  vector<int> MC_mogra_trueID;

  vector<double> MC_grgra_ori_vtx_y, MC_grgra_ori_vtx_z, MC_grgra_ori_vtx_x;
  vector<double> MC_grgra_py, MC_grgra_pz, MC_grgra_px, MC_grgra_M, MC_grgra_E;
  vector<int> MC_grgra_trueID;

  LHCb::Particles::const_iterator ipp;
  for(ipp= parts->begin();ipp!=parts->end();ipp++){



    howManyParts++;


    Gaudi::XYZPoint track_pos;
    Gaudi::XYZVector track_mom;
    Gaudi::SymMatrix6x6 track_posmom_cov;
    const LHCb::Track* track=(*ipp)->proto()->track();

    (*ipp)->proto()->track()->positionAndMomentum(track_pos,track_mom,track_posmom_cov);
    par_pos_x.push_back(track_pos.x());
    par_pos_y.push_back(track_pos.y());
    par_pos_z.push_back(track_pos.z());


    const LHCb::State* fmState = track->stateAt( LHCb::State::FirstMeasurement );
    if (fmState){
      par_fmpos_x.push_back(fmState->x());
      par_fmpos_y.push_back(fmState->y());
      par_fmpos_z.push_back(fmState->z());
    }
    else{
      par_fmpos_x.push_back(0);
      par_fmpos_y.push_back(0);
      par_fmpos_z.push_back(0);
    }
    
    par_pos_covxx.push_back(track_posmom_cov(0,0));
    par_pos_covyy.push_back(track_posmom_cov(1,1));
    par_pos_covzz.push_back(track_posmom_cov(2,2));
    par_pos_covxy.push_back(track_posmom_cov(0,1));
    par_pos_covxz.push_back(track_posmom_cov(0,2));
    par_pos_covyz.push_back(track_posmom_cov(1,2));
    
    par_px_cov00.push_back(track_posmom_cov(0,0));
    par_px_cov11.push_back(track_posmom_cov(1,1));
    par_px_cov22.push_back(track_posmom_cov(2,2));
    par_px_cov33.push_back(track_posmom_cov(3,3));
    par_px_cov44.push_back(track_posmom_cov(4,4));
    par_px_cov55.push_back(track_posmom_cov(5,5));
    par_px_cov01.push_back(track_posmom_cov(0,1));
    par_px_cov02.push_back(track_posmom_cov(0,2));
    par_px_cov12.push_back(track_posmom_cov(1,2));
    par_px_cov34.push_back(track_posmom_cov(3,4));
    par_px_cov35.push_back(track_posmom_cov(3,5));
    par_px_cov45.push_back(track_posmom_cov(4,5));
    
    par_px_cov03.push_back(track_posmom_cov(0,3));
    par_px_cov04.push_back(track_posmom_cov(0,4));
    par_px_cov05.push_back(track_posmom_cov(0,5));
    par_px_cov13.push_back(track_posmom_cov(1,3));
    par_px_cov14.push_back(track_posmom_cov(1,4));
    par_px_cov15.push_back(track_posmom_cov(1,5));
    par_px_cov23.push_back(track_posmom_cov(2,3));
    par_px_cov24.push_back(track_posmom_cov(2,4));
    par_px_cov25.push_back(track_posmom_cov(2,5));
    
    par_px.push_back(track_mom.x());
    par_py.push_back(track_mom.y());
    par_pz.push_back(track_mom.z());
    par_pt.push_back((*ipp)->proto()->track()->pt());
    par_p.push_back((*ipp)->proto()->track()->p());
    par_chg.push_back((*ipp)->proto()->track()->charge());

    //take slope and its cov. matrix, and fill the cov matrix (x,y)-subspace
    Gaudi::XYZVector track_slope;
    Gaudi::SymMatrix3x3 track_slope_cov;
    (*ipp)->proto()->track()->slopes(track_slope,track_slope_cov);
    par_slope_covxx.push_back(track_slope_cov(0,0));
    par_slope_covyy.push_back(track_slope_cov(1,1));
    par_slope_covxy.push_back(track_slope_cov(0,1));

    par_NNe.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::ProbNNe,-9999));
    par_NNmu.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::ProbNNmu,-9999));
    par_NNpi.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::ProbNNpi,-9999));
    par_NNk.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::ProbNNk,-9999));
    par_NNp.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::ProbNNp,-9999));
    par_NNghost.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::ProbNNghost,-9999));
    par_DLLe.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::CombDLLe,-9999));
    par_DLLmu.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::CombDLLmu,-9999));
    par_DLLk.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::CombDLLk,-9999));
    par_DLLp.push_back((*ipp)->proto()->info(LHCb::ProtoParticle::CombDLLp,-9999));
    int ismuon_flag=0;
    if ((*ipp)->proto()->muonPID()) ismuon_flag=(*ipp)->proto()->muonPID()->IsMuon()?1:0;
    par_isMuon.push_back(ismuon_flag);
    
    par_typ.push_back((*ipp)->proto()->track()->type());
    par_gho.push_back((*ipp)->proto()->track()->ghostProbability());
    par_chi.push_back((*ipp)->proto()->track()->chi2PerDoF());
    par_clo.push_back((*ipp)->proto()->track()->info(LHCb::Track::CloneDist, 100));
    //Find the impact parameter significance of the track
    double imp=0.;
    double impchi2=0.;
    double ips;
    ips=100000.;



    LHCb::RecVertex::Container* verts = NULL;
    if(exist<LHCb::RecVertex::Container>(m_PVInputLocation)) verts = get<LHCb::RecVertex::Container>(m_PVInputLocation);
    LHCb::RecVertex::Container::const_iterator iv;      
    for ( iv = verts->begin(); iv != verts->end(); iv++) {
      m_dist->distance(&(*(*ipp)),(*iv),imp,impchi2);
      if (impchi2<ips) ips = impchi2;
    }      
    ips=sqrt(ips);
    par_ip.push_back(imp);
    par_ips.push_back(ips);

    //PV association
    par_PVind.push_back(getAssociatedPV(track,trkPV_pz,trkPV_gho,trkPV_num));

    matched = kFALSE;
    hasMother = kFALSE;
    hasGranny = kFALSE;
    hasMotGranny = kFALSE;
    hasGraGranny = kFALSE;
    
    //MC truth info
    if (m_isMC){
      if (!MCp) {
	debug()<<" Didn't find MCP "<<endmsg;
	MC_ori_vtx_x.push_back(-9999);
	MC_ori_vtx_y.push_back(-9999);
	MC_ori_vtx_z.push_back(-9999);
	MC_ori_type.push_back(-9999);
	MC_par_trueID.push_back(-9999);
	MC_par_isnoniso_mup.push_back(-9999);
	MC_par_isnoniso_mum.push_back(-9999);
	MC_par_px.push_back(-9999);
	MC_par_py.push_back(-9999);
	MC_par_pz.push_back(-9999);
	MC_par_M.push_back(-9999);
	MC_par_E.push_back(-9999);
	MC_origin_id.push_back(-9999);
      }      
      else {
	matched = kTRUE;
	std::vector<int> key_vec;
	key_vec.push_back(MCp->key());
	get_decayTree(MCp,key_vec);
	
	//MC particle info.
	//Origin vtx      
	MC_ori_vtx_x.push_back(MCp->originVertex()->position().x());
	MC_ori_vtx_y.push_back(MCp->originVertex()->position().y());
	MC_ori_vtx_z.push_back(MCp->originVertex()->position().z());
	MC_ori_type.push_back(MCp->originVertex()->type());
	
	MC_par_trueID.push_back(MCp->particleID().pid());
	MC_par_isnoniso_mup.push_back(isNonIso(MCp,"mu+"));
	MC_par_isnoniso_mum.push_back(isNonIso(MCp,"mu-"));
	MC_par_px.push_back(MCp->momentum().Px());
	MC_par_py.push_back(MCp->momentum().Py());
	MC_par_pz.push_back(MCp->momentum().Pz());
	MC_par_M.push_back(MCp->momentum().M());
	MC_par_E.push_back(MCp->momentum().E());
	
	m_typeOrigin =  MCp->originVertex()->type();
	
	//Ancestor info
	ancestor = originof(MCp) ;
	MC_origin_id.push_back(ancestor->particleID().pid());
	debug()<<" Filled ancestor PID "<<endmsg;      
	
	//Number of end vertices: not tupled for now
	SmartRefVector< LHCb::MCVertex >::const_iterator iVtx;
	if (0==MCp->endVertices().size())
	  {
	    debug() << " vertex failed " << endmsg;
	    m_endVertices  = -100;
	  }
	else 
	  {
	    m_endVertices = MCp->endVertices().size();
	  }
	
	//MC mother info
	const LHCb::MCParticle* mother = MCp->mother();
	if(mother) {
	  hasMother = kTRUE;
	  MC_par_hasMother.push_back(kTRUE);
	  MC_mot_ori_vtx_x.push_back(mother->originVertex()->position().x());
	  MC_mot_ori_vtx_y.push_back(mother->originVertex()->position().y());
	  MC_mot_ori_vtx_z.push_back(mother->originVertex()->position().z());
	  
	  MC_mot_trueID.push_back(mother->particleID().pid());
	  MC_mot_px.push_back(mother->momentum().Px());
	  MC_mot_py.push_back(mother->momentum().Py());
	  MC_mot_pz.push_back(mother->momentum().Pz());
	  MC_mot_M.push_back(mother->momentum().M());
	  MC_mot_E.push_back(mother->momentum().E());
	  
	  //MC grandmother info
	  const LHCb::MCParticle* granny = mother->mother();
	  
	  if(granny) {
	    hasGranny = kTRUE;
	    MC_mot_hasMother.push_back(kTRUE);
	    
	    MC_gra_ori_vtx_x.push_back(granny->originVertex()->position().x());
	    MC_gra_ori_vtx_y.push_back(granny->originVertex()->position().y());
	    MC_gra_ori_vtx_z.push_back(granny->originVertex()->position().z());
	    
	    MC_gra_trueID.push_back(granny->particleID().pid());
	    MC_gra_px.push_back(granny->momentum().Px());
	    MC_gra_py.push_back(granny->momentum().Py());
	    MC_gra_pz.push_back(granny->momentum().Pz());
	    MC_gra_M.push_back(granny->momentum().M());
	    MC_gra_E.push_back(granny->momentum().E());
	    
	    //MC mother of grandmother info
	    const LHCb::MCParticle* motgranny = granny->mother();
	    
	    if(motgranny) {
	      hasMotGranny = kTRUE;
	      MC_gra_hasMother.push_back(kTRUE);
	      MC_mogra_ori_vtx_x.push_back(motgranny->originVertex()->position().x());
	      MC_mogra_ori_vtx_y.push_back(motgranny->originVertex()->position().y());
	      MC_mogra_ori_vtx_z.push_back(motgranny->originVertex()->position().z());
	      
	      MC_mogra_trueID.push_back(motgranny->particleID().pid());
	      MC_mogra_px.push_back(motgranny->momentum().Px());
	      MC_mogra_py.push_back(motgranny->momentum().Py());
	      MC_mogra_pz.push_back(motgranny->momentum().Pz());
	      MC_mogra_M.push_back(motgranny->momentum().M());
	      MC_mogra_E.push_back(motgranny->momentum().E());
	      
	      //MC grandmother of grandmother info
	      const LHCb::MCParticle* gragranny = motgranny->mother();
	      
	      if(gragranny) {
		hasGraGranny = kTRUE;
		MC_mogra_hasMother.push_back(kTRUE);
		MC_grgra_ori_vtx_x.push_back(gragranny->originVertex()->position().x());
		MC_grgra_ori_vtx_y.push_back(gragranny->originVertex()->position().y());
		MC_grgra_ori_vtx_z.push_back(gragranny->originVertex()->position().z());
		
		MC_grgra_trueID.push_back(gragranny->particleID().pid());
		MC_grgra_px.push_back(gragranny->momentum().Px());
		MC_grgra_py.push_back(gragranny->momentum().Py());
		MC_grgra_pz.push_back(gragranny->momentum().Pz());
		MC_grgra_M.push_back(gragranny->momentum().M());
		MC_grgra_E.push_back(gragranny->momentum().E());
	      } else {
		hasGraGranny = kFALSE;
	      } //Granny of granny
	    } else {
	      hasMotGranny = kFALSE;
	    } //Mother of granny
	  } else {
	    hasGranny = kFALSE;
	  } //Granny
	} else {
	  hasMother = kFALSE;
	} //Mother
      }// i.e. we have an MCp
      if(!hasMother) {      
	MC_par_hasMother.push_back(kFALSE);
	MC_mot_ori_vtx_x.push_back(-9999);
	MC_mot_ori_vtx_y.push_back(-9999);
	MC_mot_ori_vtx_z.push_back(-9999);
	
	MC_mot_trueID.push_back(-9999);
	MC_mot_px.push_back(-9999);
	MC_mot_py.push_back(-9999);
	MC_mot_pz.push_back(-9999);
	MC_mot_M.push_back(-9999);
	MC_mot_E.push_back(-9999);
      }
      if(!hasGranny) {
	MC_mot_hasMother.push_back(kFALSE);
	MC_gra_ori_vtx_x.push_back(-9999);
	MC_gra_ori_vtx_y.push_back(-9999);
	MC_gra_ori_vtx_z.push_back(-9999);
	
	MC_gra_trueID.push_back(-9999);
	MC_gra_px.push_back(-9999);
	MC_gra_py.push_back(-9999);
	MC_gra_pz.push_back(-9999);
	MC_gra_M.push_back(-9999);
	MC_gra_E.push_back(-9999);
      }
      if(!hasMotGranny) {
	MC_gra_hasMother.push_back(kFALSE);
	MC_mogra_ori_vtx_x.push_back(-9999);
	MC_mogra_ori_vtx_y.push_back(-9999);
	MC_mogra_ori_vtx_z.push_back(-9999);
	
	MC_mogra_trueID.push_back(-9999);
	MC_mogra_px.push_back(-9999);
	MC_mogra_py.push_back(-9999);
	MC_mogra_pz.push_back(-9999);
	MC_mogra_M.push_back(-9999);
	MC_mogra_E.push_back(-9999);
      }
      
      if(!hasGraGranny) {
	MC_mogra_hasMother.push_back(kFALSE);
	MC_grgra_ori_vtx_x.push_back(-9999);
	MC_grgra_ori_vtx_y.push_back(-9999);
	MC_grgra_ori_vtx_z.push_back(-9999);
	
	MC_grgra_trueID.push_back(-9999);
	MC_grgra_px.push_back(-9999);
	MC_grgra_py.push_back(-9999);
	MC_grgra_pz.push_back(-9999);
	MC_grgra_M.push_back(-9999);
	MC_grgra_E.push_back(-9999);
      }
      MC_match.push_back(matched);
    }//end m_isMC cond
  }//end loop over particles

  test &= tuple->column( "EVB_Nparts", howManyParts);
  test &= tuple->farray( "EVB_par_pos_x", par_pos_x.begin(), par_pos_x.end(), "pbIdx", NMaxEVBpart );//position at 'closestToBeam'
  test &= tuple->farray( "EVB_par_pos_y", par_pos_y.begin(), par_pos_y.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_pos_z", par_pos_z.begin(), par_pos_z.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_pos_covxx", par_pos_covxx.begin(), par_pos_covxx.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_pos_covyy", par_pos_covyy.begin(), par_pos_covyy.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_pos_covxy", par_pos_covxy.begin(), par_pos_covxy.end(), "pbIdx", NMaxEVBpart );

  test &= tuple->farray( "EVB_par_fmpos_x", par_fmpos_x.begin(), par_fmpos_x.end(), "pbIdx", NMaxEVBpart );//position at 'firstMeasurement'
  test &= tuple->farray( "EVB_par_fmpos_y", par_fmpos_y.begin(), par_fmpos_y.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_fmpos_z", par_fmpos_z.begin(), par_fmpos_z.end(), "pbIdx", NMaxEVBpart );
  
  //Cov mat xp (6x6)
  test &= tuple->farray( "EVB_par_xp_cov00", par_px_cov00.begin(), par_px_cov00.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov11", par_px_cov11.begin(), par_px_cov11.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov22", par_px_cov22.begin(), par_px_cov22.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov33", par_px_cov33.begin(), par_px_cov33.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov44", par_px_cov44.begin(), par_px_cov44.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov55", par_px_cov55.begin(), par_px_cov55.end(), "pbIdx", NMaxEVBpart );

  test &= tuple->farray( "EVB_par_xp_cov01", par_px_cov01.begin(), par_px_cov01.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov02", par_px_cov02.begin(), par_px_cov02.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov12", par_px_cov12.begin(), par_px_cov12.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov34", par_px_cov34.begin(), par_px_cov34.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov35", par_px_cov35.begin(), par_px_cov35.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov45", par_px_cov45.begin(), par_px_cov45.end(), "pbIdx", NMaxEVBpart );

  test &= tuple->farray( "EVB_par_xp_cov03", par_px_cov03.begin(), par_px_cov03.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov04", par_px_cov04.begin(), par_px_cov04.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov05", par_px_cov05.begin(), par_px_cov05.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov13", par_px_cov13.begin(), par_px_cov13.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov14", par_px_cov14.begin(), par_px_cov14.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov15", par_px_cov15.begin(), par_px_cov15.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov23", par_px_cov23.begin(), par_px_cov23.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov24", par_px_cov24.begin(), par_px_cov24.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_xp_cov25", par_px_cov25.begin(), par_px_cov25.end(), "pbIdx", NMaxEVBpart );

  test &= tuple->farray( "EVB_par_px", par_px.begin(), par_px.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_py", par_py.begin(), par_py.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_pz", par_pz.begin(), par_pz.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_pt", par_pt.begin(), par_pt.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_p", par_p.begin(), par_p.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_chg", par_chg.begin(), par_chg.end(), "pbIdx", NMaxEVBpart );

  test &= tuple->farray( "EVB_par_typ", par_typ.begin(), par_typ.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_gho", par_gho.begin(), par_gho.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_chi", par_chi.begin(), par_chi.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_clo", par_clo.begin(), par_clo.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_ip", par_ip.begin(), par_ip.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_ips", par_ips.begin(), par_ips.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_hlt", par_hlt.begin(), par_hlt.end(), "pbIdx", NMaxEVBpart );
  
  test &= tuple->farray( "EVB_par_slope_covxx", par_slope_covxx.begin(), par_slope_covxx.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_slope_covyy", par_slope_covyy.begin(), par_slope_covyy.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_slope_covxy", par_slope_covxy.begin(), par_slope_covxy.end(), "pbIdx", NMaxEVBpart );

  test &= tuple->farray( "EVB_par_NNe", par_NNe.begin(), par_NNe.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_NNmu", par_NNmu.begin(), par_NNmu.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_NNpi", par_NNpi.begin(), par_NNpi.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_NNk", par_NNk.begin(), par_NNk.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_NNp", par_NNp.begin(), par_NNp.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_NNghost", par_NNghost.begin(), par_NNghost.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_DLLe", par_DLLe.begin(), par_DLLe.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_DLLmu", par_DLLmu.begin(), par_DLLmu.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_DLLk", par_DLLk.begin(), par_DLLk.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_DLLp", par_DLLp.begin(), par_DLLp.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_isMuon", par_isMuon.begin(), par_isMuon.end(), "pbIdx", NMaxEVBpart );
  test &= tuple->farray( "EVB_par_PVind", par_PVind.begin(), par_PVind.end(), "pbIdx", NMaxEVBpart );

  
  // if (m_isMC){
  //   //MC truth info
  //   test &= tuple->farray( "EVB_isMCMatched", MC_match.begin(), MC_match.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_Origin_ID", MC_origin_id.begin(), MC_origin_id.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_part_Ori_Vtx_x", MC_ori_vtx_x.begin(), MC_ori_vtx_x.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_Ori_Vtx_y", MC_ori_vtx_y.begin(), MC_ori_vtx_y.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_Ori_Vtx_z", MC_ori_vtx_z.begin(), MC_ori_vtx_z.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_Ori_type", MC_ori_type.begin(), MC_ori_type.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_part_ID", MC_par_trueID.begin(), MC_par_trueID.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_isnoniso_mup", MC_par_isnoniso_mup.begin(), MC_par_isnoniso_mup.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_isnoniso_mum", MC_par_isnoniso_mum.begin(), MC_par_isnoniso_mum.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_Px", MC_par_px.begin(), MC_par_px.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_Py", MC_par_py.begin(), MC_par_py.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_Pz", MC_par_pz.begin(), MC_par_pz.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_E", MC_par_E.begin(), MC_par_E.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_part_M", MC_par_M.begin(), MC_par_M.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_part_hasMother", MC_par_hasMother.begin(), MC_par_hasMother.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_moth_Ori_Vtx_x", MC_mot_ori_vtx_x.begin(), MC_mot_ori_vtx_x.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_moth_Ori_Vtx_y", MC_mot_ori_vtx_y.begin(), MC_mot_ori_vtx_y.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_moth_Ori_Vtx_z", MC_mot_ori_vtx_z.begin(), MC_mot_ori_vtx_z.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_moth_ID", MC_mot_trueID.begin(), MC_mot_trueID.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_moth_Px", MC_mot_px.begin(), MC_mot_px.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_moth_Py", MC_mot_py.begin(), MC_mot_py.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_moth_Pz", MC_mot_pz.begin(), MC_mot_pz.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_moth_E", MC_mot_E.begin(), MC_mot_E.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_moth_M", MC_mot_M.begin(), MC_mot_M.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_moth_hasMother", MC_mot_hasMother.begin(), MC_mot_hasMother.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_gran_Ori_Vtx_x", MC_gra_ori_vtx_x.begin(), MC_gra_ori_vtx_x.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_gran_Ori_Vtx_y", MC_gra_ori_vtx_y.begin(), MC_gra_ori_vtx_y.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_gran_Ori_Vtx_z", MC_gra_ori_vtx_z.begin(), MC_gra_ori_vtx_z.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_gran_ID", MC_gra_trueID.begin(), MC_gra_trueID.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_gran_Px", MC_gra_px.begin(), MC_gra_px.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_gran_Py", MC_gra_py.begin(), MC_gra_py.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_gran_Pz", MC_gra_pz.begin(), MC_gra_pz.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_gran_E", MC_gra_E.begin(), MC_gra_E.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_gran_M", MC_gra_M.begin(), MC_gra_M.end(), "pbIdx", NMaxEVBpart );
     
  //   test &= tuple->farray( "EVB_true_gran_hasMother", MC_gra_hasMother.begin(), MC_gra_hasMother.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_mogr_Ori_Vtx_x", MC_mogra_ori_vtx_x.begin(), MC_mogra_ori_vtx_x.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_mogr_Ori_Vtx_y", MC_mogra_ori_vtx_y.begin(), MC_mogra_ori_vtx_y.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_mogr_Ori_Vtx_z", MC_mogra_ori_vtx_z.begin(), MC_mogra_ori_vtx_z.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_mogr_ID", MC_mogra_trueID.begin(), MC_mogra_trueID.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_mogr_Px", MC_mogra_px.begin(), MC_mogra_px.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_mogr_Py", MC_mogra_py.begin(), MC_mogra_py.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_mogr_Pz", MC_mogra_pz.begin(), MC_mogra_pz.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_mogr_E", MC_mogra_E.begin(), MC_mogra_E.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_mogr_M", MC_mogra_M.begin(), MC_mogra_M.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_mogr_hasMother", MC_mogra_hasMother.begin(), MC_mogra_hasMother.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_grgr_Ori_Vtx_x", MC_grgra_ori_vtx_x.begin(), MC_grgra_ori_vtx_x.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_grgr_Ori_Vtx_y", MC_grgra_ori_vtx_y.begin(), MC_grgra_ori_vtx_y.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_grgr_Ori_Vtx_z", MC_grgra_ori_vtx_z.begin(), MC_grgra_ori_vtx_z.end(), "pbIdx", NMaxEVBpart );
    
  //   test &= tuple->farray( "EVB_true_grgr_ID", MC_grgra_trueID.begin(), MC_grgra_trueID.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_grgr_Px", MC_grgra_px.begin(), MC_grgra_px.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_grgr_Py", MC_grgra_py.begin(), MC_grgra_py.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_grgr_Pz", MC_grgra_pz.begin(), MC_grgra_pz.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_grgr_E", MC_grgra_E.begin(), MC_grgra_E.end(), "pbIdx", NMaxEVBpart );
  //   test &= tuple->farray( "EVB_true_grgr_M", MC_grgra_M.begin(), MC_grgra_M.end(), "pbIdx", NMaxEVBpart );
  // }//end m_isMC cond
  
  if(test) return StatusCode::SUCCESS;
  else return StatusCode::FAILURE; 
  
}//end fillTrackInfo


  

//==============================
// MC Ancestor Finding Routine:
//==============================

const LHCb::MCParticle* TupleToolMuonVariables::originof( const LHCb::MCParticle* product ) {
  const LHCb::MCParticle* mother = product->mother();
  //std::cout<<" MC Ancestor Finding Routine: "<<std::endl;
  
  if ( (!mother) || product->particleID().hasBottom() ) return product;
  else return originof( mother );
}





//=============================================================================
// Finalize
//=============================================================================


StatusCode TupleToolMuonVariables::finalize(){
  
  return TupleToolBase::finalize();
  

}


void TupleToolMuonVariables::trkPVassociator(std::vector<double>& trkPV_pz, std::vector<double>& trkPV_gho,std::vector<int>& trkPV_num,std::string trk_type){
  if (trkPV_pz.size()!=trkPV_gho.size() || trkPV_pz.size()!=trkPV_num.size())
    { cout<<"ERROR in TupleToolMuonVariables::trkPVassociator: trkPV_pz, trkPV_gho, trkPV_num siz ="<<trkPV_pz.size()<<" "<<trkPV_gho.size()<<" " <<trkPV_num.size()<<endl; return; }
  if (trkPV_pz.size()>0) return;//the vectors have been already filled
  
  //Get the list of primary vertices and loop over them
  int pv_index(0);
  LHCb::RecVertex::Container* verts = NULL;
  if(exist<LHCb::RecVertex::Container>(m_PVInputLocation)) verts = get<LHCb::RecVertex::Container>(m_PVInputLocation);
  LHCb::RecVertex::Container::const_iterator iv;
  for ( iv = verts->begin(); iv != verts->end(); iv++) {
    //get the tracks associated to the PV and loop over them
    for (SmartRefVector< LHCb::Track >::const_iterator it_ptrk=(*iv)->tracks().begin(); it_ptrk!=(*iv)->tracks().end(); ++it_ptrk){
      const Track* track=(*it_ptrk);
      int type=track->type();
      if (!( (trk_type=="velo" && (type==1 || type==2)) ||
	     (trk_type=="long" && type==3) ||
	     (trk_type=="upstream" && type==4) )) continue; 
      trkPV_pz.push_back(track->momentum().z());
      trkPV_gho.push_back(track->ghostProbability());
      trkPV_num.push_back(pv_index);
    }//end it_ptrk!=(*iv)->tracks.end() loop
    pv_index++;
  }//end iv!=verts->end()

  return;

}//end trkPVassociator


int TupleToolMuonVariables::getAssociatedPV(const LHCb::Track* track, const std::vector<double>& trkPV_pz, const std::vector<double>& trkPV_gho, const std::vector<int>& trkPV_num){
  int output=0;
  if (trkPV_pz.size()!=trkPV_gho.size() || trkPV_pz.size()!=trkPV_num.size())
    { cout<<"ERROR in TupleToolMuonVariables::trkPVassociator: trkPV_pz, trkPV_gho, trkPV_num siz ="<<trkPV_pz.size()<<" "<<trkPV_gho.size()<<" " <<trkPV_num.size()<<endl; return output; }
  double pz_trk=track->momentum().z();
  double gho_trk=track->ghostProbability();
  for (int i=0;i<(int)trkPV_pz.size();i++){
    if (trkPV_pz.at(i)==pz_trk && trkPV_gho.at(i)==gho_trk){ 
      output+=pow(2,trkPV_num.at(i));
      //cout<<"match: pz = "<<pz_trk<<" trkPV_num.at(i) = "<<trkPV_num.at(i)<<" output = "<<output<<endl;
    }
  }
  return output;
}//end getAssociatedPV



void TupleToolMuonVariables::get_decayTree(const LHCb::MCParticle* mcp,vector<int>& key_vec){
  const LHCb::MCParticle* mother=mcp->mother();
  if (mother){
    key_vec.push_back(mother->key());
    get_decayTree(mother,key_vec);
  }
  else return;

}//end get_decayTree


StatusCode TupleToolMuonVariables::fillPVList(Tuples::Tuple& tuple){
  //Get the list of primary vertices and store their vtx info in ntuple
  LHCb::RecVertex::Container* verts = NULL;
  if(exist<LHCb::RecVertex::Container>(m_PVInputLocation)) verts = get<LHCb::RecVertex::Container>(m_PVInputLocation);
  LHCb::RecVertex::Container::const_iterator iv;
  vector<double> pv_x,pv_y,pv_z;
  vector<double> pv_covxx,pv_covyy,pv_covzz,pv_covxy,pv_covxz,pv_covyz;
  vector<double> pv_chi2ndof;
  vector<int> pv_ndof,pv_ntrk;
  for ( iv = verts->begin(); iv != verts->end(); iv++) {
    Gaudi::XYZPoint PosPV = (*iv)->position();
    const Gaudi::SymMatrix3x3 & vtx = (*iv)->covMatrix();
    pv_x.push_back(PosPV.x());
    pv_y.push_back(PosPV.y());
    pv_z.push_back(PosPV.z());
    pv_covxx.push_back(vtx(0,0));
    pv_covyy.push_back(vtx(1,1));
    pv_covzz.push_back(vtx(2,2));
    pv_covxy.push_back(vtx(0,1));
    pv_covxz.push_back(vtx(0,2));
    pv_covyz.push_back(vtx(1,2));
    pv_chi2ndof.push_back((*iv)->chi2PerDoF());
    pv_ndof.push_back((*iv)->nDoF());
    //std::vector<const LHCb::Particle*> outgoing_part_vec=(*iv)->outgoingParticlesVector();
    const SmartRefVector<LHCb::Track> outgoing_part_vec=(*iv)->tracks();
    pv_ntrk.push_back((int)outgoing_part_vec.size());
  }
  bool test=true;
  int PV_maxnum=30;
  test &= tuple->farray("PV_x",pv_x.begin(),pv_x.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_y",pv_y.begin(),pv_y.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_z",pv_z.begin(),pv_z.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_covxx",pv_covxx.begin(),pv_covxx.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_covyy",pv_covyy.begin(),pv_covyy.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_covzz",pv_covzz.begin(),pv_covzz.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_covxy",pv_covxy.begin(),pv_covxy.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_covxz",pv_covxz.begin(),pv_covxz.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_covyz",pv_covyz.begin(),pv_covyz.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_chi2PerDoF",pv_chi2ndof.begin(),pv_chi2ndof.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_nDoF",pv_ndof.begin(),pv_ndof.end(),"PV_num",PV_maxnum);
  test &= tuple->farray("PV_ntrk",pv_ntrk.begin(),pv_ntrk.end(),"PV_num",PV_maxnum);

  if (test) return StatusCode::SUCCESS;
  else return StatusCode::FAILURE;

}//end fillPVList


StatusCode TupleToolMuonVariables::fillBposmomCov(const LHCb::Particle* B, std::string prefix, Tuples::Tuple& tuple ){
  const Gaudi::SymMatrix7x7 & covMat = B->covMatrix ();
  bool test=true;
  // std::cout<<"Fill cov"<<std::endl;
  for (int i=0;i<7;i++)
    for (int j=i;j<7;j++){
      char* name=new char[100]; sprintf(name,"_cov%i%i",i,j);
      test &= tuple->column(prefix+name,covMat(i,j));
      delete[] name;
    }
  if (test) return StatusCode::SUCCESS;
  else return StatusCode::FAILURE;

}//end fillBposmomCov


void  TupleToolMuonVariables::dumpIsoVariables(const LHCb::Particle* B, Tuples::Tuple& tuple ){

  const int NMaxIsopart = 500;

  //If B has not 2+1 daughters, exit
  const LHCb::Particle::ConstVector parts = B->daughtersVector();
  if(parts.size() != 3) {
    error()<<"Number of B daughters != 2 + 1: "<<parts.size()<<endmsg;
    return;
  }

  //Get the particle list (longtracks). It if has a null pointer, exit
  LHCb::Particles* allparts = get<LHCb::Particles>(m_ParticlePath);
  if (!allparts) {
    error()<<"Failed to get particles container "<<m_ParticlePath<<endmsg;
    return;
  }

  //Get the primary and secondary vertices, and their positions
  const LHCb::VertexBase *PV = m_dva->bestVertex(B);
  const LHCb::VertexBase *SV = B->endVertex();
  Gaudi::XYZPoint PosPV = PV->position();
  Gaudi::XYZPoint PosSV = SV->position();

  //Loop over the B candidate munon daughters and store their position, p and pt
  ROOT::Math::SMatrix<double, 3, 2> o_mu;
  ROOT::Math::SMatrix<double, 3, 2> p_mu;
  ROOT::Math::SVector<double, 2> ptmu;
  std::vector<const LHCb::Particle*> mucand_vec;
  std::vector<const LHCb::Track*> mutrack_vec;

  int i=0;
  LHCb::Particle::ConstVector::const_iterator ip_part;
  for ( ip_part = parts.begin(); ip_part != parts.end(); ip_part++) {
    if (abs((*ip_part)->particleID().pid()) != 13) continue;
    //const LHCb::ProtoParticle * proto =  (*ip_part)->proto();
    //const LHCb::Track* track = proto->track();
    const LHCb::Track* track = (*ip_part)->proto()->track();
    o_mu(0,i) = track->position().x();
    o_mu(1,i) = track->position().y();
    o_mu(2,i) = track->position().z();
    p_mu(0,i) = track->momentum().x();
    p_mu(1,i) = track->momentum().y();
    p_mu(2,i) = track->momentum().z();
    ptmu[i] = sqrt(pow(p_mu(0,i),2)+pow(p_mu(1,i),2));
    i++;
    mucand_vec.push_back(*ip_part);
    mutrack_vec.push_back((*ip_part)->proto()->track());
  }

  vector<double> fim_angle, fim_fc, fim_doca, fim_svdis, fim_pvdis;
  vector<double> fim_doca2, fim_docachi2;
  vector<double> fim_vtx_x,fim_vtx_y,fim_vtx_z;
  vector<double> sem_angle, sem_fc, sem_doca, sem_svdis, sem_pvdis;
  vector<double> sem_doca2, sem_docachi2;
  vector<double> sem_vtx_x,sem_vtx_y,sem_vtx_z;
  vector<double> trk_gho, trk_chi, trk_typ, trk_chg, trk_pt;
  vector<double> trk_ips, trk_clo;//MR 5feb14
  vector<bool> trk_hlt;

  //Loop over the two B daughter muon tracks. For each muon track, loop over the
  //tracks of the event and build the (muon,track) quantities used by the track
  //isolation. Store these quantities in 3 sets of arrays. fim_xxx, sem_xxx, trk_xxx.
  // fim_xxx contains the variables related to the 'first muon' (ch=??) and the tracks
  // sem_xxx contains the variables related to the 'second muon' (ch=??) and the tracks
  // trk_xxx contains the variables related to the tracks properties
  for (int i=0;i<2;i++){
    bool hltgood = false;
    double fc = 0.;
    Gaudi::XYZPoint omu(o_mu(0,i),o_mu(1,i),o_mu(2,i));
    Gaudi::XYZVector pmu(p_mu(0,i),p_mu(1,i),p_mu(2,i));
    const LHCb::Particle* mupart=mucand_vec[i];

    //Loop over all (long) tracks of the event.
    LHCb::Particles::const_iterator ip;
    for ( ip = allparts->begin(); ip != allparts->end() ; ++ip) {

      const LHCb::Track* track = (*ip)->proto()->track();
      const LHCb::Particle*  cand = (*ip);
      Gaudi::XYZPoint o(track->position());
      Gaudi::XYZVector p(track->momentum());
      double pt = p.Rho();

      if (track->type()!=m_tracktype)   continue;

      //if (ratio(pt, ptmu[0]) < 0.0001 || ratio(pt,ptmu[1]) <0.0001) {
      if (track==mutrack_vec[0] || track==mutrack_vec[1]) {
        if (!(ratio(pt, ptmu[0]) < 0.0001 || ratio(pt,ptmu[1]) <0.0001)) {
          cout<<"ATTENTION!!: ratio(pt,ptmu[0]) = "<<ratio(pt,ptmu[0])<<" ratio(pt,ptmu[1]) = "<<ratio(pt,ptmu[1])<<endl;
        }
        if(i==0) {
          //if track = first muon, fill the fim_xxx block with a dummy value
          fim_angle.push_back(0);
          fim_fc.push_back(0);
          fim_doca.push_back(0);
          fim_doca2.push_back(0);
          fim_docachi2.push_back(0);
          fim_svdis.push_back(0);
          fim_pvdis.push_back(0);
          fim_vtx_x.push_back(0);
          fim_vtx_y.push_back(0);
          fim_vtx_z.push_back(0);
          //The track quantities are independent on muons (hence filled once)
          trk_gho.push_back(0);
          trk_chi.push_back(0);
          trk_chg.push_back(0);
          trk_pt.push_back(0);
          trk_typ.push_back(0);
          trk_clo.push_back(0);
          trk_ips.push_back(0);
          trk_hlt.push_back(0);
        }
        else {
          //if track = second muon, fill the fim_xxx block with a dummy value
          sem_angle.push_back(0);
          sem_fc.push_back(0);
          sem_doca.push_back(0);
          sem_doca2.push_back(0);
          sem_docachi2.push_back(0);
          sem_svdis.push_back(0);
          sem_pvdis.push_back(0);
          sem_vtx_x.push_back(0);
          sem_vtx_y.push_back(0);
          sem_vtx_z.push_back(0);
        }
        //then jump to the next track
        continue;
      }//end "check of track pt vs the 2 muons pt"

      //Find hltgood and fc between the muon and the track
      //  omu,pmu=position and p vectors of the current muon
      //  o,p=position and p vectors of the current track
      //  IsHltGood takes as input o,p,omu,pmu,PosPV and fill hltgood and fc.
      //DOM: come e' def. hltgood? VEDERE
      IsHltGood(o, p, omu, pmu ,PosPV, hltgood, fc);

      //Find doca and angle between the muona and the track
      Gaudi::XYZPoint vtx(0.,0.,0.);
      double doca(-1.);
      double angle(-1.);
      InCone(omu,pmu,o,p,vtx,doca,angle);
      double doca_precise(-1);
      double doca_chi2(-1);
      m_dist->distance(mupart,cand,doca_precise,doca_chi2);

      //Find the impact parameter significance of the track and the distance between (muon,track) and the secondary and primary vertices
      double imp=0.;
      double impchi2=0.;
      double ips,pvdis,svdis;
      ips=100000.;

      LHCb::RecVertex::Container* verts = NULL;
      if(exist<LHCb::RecVertex::Container>(m_PVInputLocation)) verts = get<LHCb::RecVertex::Container>(m_PVInputLocation);
      LHCb::RecVertex::Container::const_iterator iv;
      for ( iv = verts->begin(); iv != verts->end(); iv++) {
        m_dist->distance(&(*cand),(*iv),imp,impchi2);
        if (impchi2<ips) ips = impchi2;
      }
      ips=sqrt(ips);
      int sign_pv=vtx.z()-PosPV.z()>=0?1:-1;
      int sign_sv=vtx.z()-PosSV.z()>=0?1:-1;
      pvdis = sign_pv*(vtx-PosPV).R();
      svdis = sign_sv*(vtx-PosSV).R();
      //pvdis = (vtx.z()-PosPV.z())/fabs(vtx.z()-PosPV.z())*(vtx-PosPV).R();
      //svdis = (vtx.z()-PosSV.z())/fabs(vtx.z()-PosSV.z())*(vtx-PosSV).R();

      if(i==0) {
        //First muon
        fim_angle.push_back(angle);
        fim_fc.push_back(fc);
        fim_doca.push_back(doca);
        fim_doca2.push_back(doca_precise);
        fim_docachi2.push_back(doca_chi2);
        fim_svdis.push_back(svdis);
        fim_pvdis.push_back(pvdis);
        fim_vtx_x.push_back(vtx.x());
        fim_vtx_y.push_back(vtx.y());
        fim_vtx_z.push_back(vtx.z());
        //The track quantities are independent on muons (hence filled once)
        trk_gho.push_back(track->ghostProbability());
        trk_chi.push_back(track->chi2PerDoF());
        trk_chg.push_back(track->charge());
        trk_pt.push_back(track->pt());
        trk_typ.push_back(track->type());
        trk_clo.push_back(track->info(LHCb::Track::CloneDist, 100));
        trk_ips.push_back(ips);
        trk_hlt.push_back(hltgood);
      }
      else {
        //Second muon
        sem_angle.push_back(angle);
        sem_fc.push_back(fc);
        sem_doca.push_back(doca);
        sem_doca2.push_back(doca_precise);
        sem_docachi2.push_back(doca_chi2);
        sem_svdis.push_back(svdis);
        sem_pvdis.push_back(pvdis);
        sem_vtx_x.push_back(vtx.x());
        sem_vtx_y.push_back(vtx.y());
        sem_vtx_z.push_back(vtx.z());
      }

    }//end loop on all tracks
  }//end i<2 for loop (loop on the 2 muons)

  tuple->farray( "IsoST_trk_typ", trk_typ.begin(), trk_typ.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_trk_chi", trk_chi.begin(), trk_chi.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_trk_chg", trk_chg.begin(), trk_chg.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_trk_pt", trk_pt.begin(), trk_pt.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_trk_gho", trk_gho.begin(), trk_gho.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_trk_clo", trk_clo.begin(), trk_clo.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_trk_ips", trk_ips.begin(), trk_ips.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_trk_hlt", trk_hlt.begin(), trk_hlt.end(), "IsoSTIdx", NMaxIsopart );

  tuple->farray( "IsoST_fim_angle", fim_angle.begin(), fim_angle.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_fc", fim_fc.begin(), fim_fc.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_doca", fim_doca.begin(), fim_doca.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_doca2", fim_doca2.begin(), fim_doca2.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_docachi2", fim_docachi2.begin(), fim_docachi2.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_svdis", fim_svdis.begin(), fim_svdis.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_pvdis", fim_pvdis.begin(), fim_pvdis.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_vtxx", fim_vtx_x.begin(), fim_vtx_x.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_vtxy", fim_vtx_y.begin(), fim_vtx_y.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_fim_vtxz", fim_vtx_z.begin(), fim_vtx_z.end(), "IsoSTIdx", NMaxIsopart );

  tuple->farray( "IsoST_sem_angle", sem_angle.begin(), sem_angle.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_fc", sem_fc.begin(), sem_fc.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_doca", sem_doca.begin(), sem_doca.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_doca2", sem_doca2.begin(), sem_doca2.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_docachi2", sem_docachi2.begin(), sem_docachi2.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_svdis", sem_svdis.begin(), sem_svdis.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_pvdis", sem_pvdis.begin(), sem_pvdis.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_vtxx", sem_vtx_x.begin(), sem_vtx_x.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_vtxy", sem_vtx_y.begin(), sem_vtx_y.end(), "IsoSTIdx", NMaxIsopart );
  tuple->farray( "IsoST_sem_vtxz", sem_vtx_z.begin(), sem_vtx_z.end(), "IsoSTIdx", NMaxIsopart );

  return;

}//end dumpIsoVariables

StatusCode TupleToolMuonVariables::fillVeloTracksInfo(Tuples::Tuple& tuple){
  bool test=true;
  vector<double> par_pos_x, par_pos_y, par_pos_z;
  vector<double> par_fmpos_x, par_fmpos_y, par_fmpos_z;//fm=first measurement
  //pos vs mom Cov matrix of tracks (6x6)
  vector<double> par_px_cov00,par_px_cov11,par_px_cov22,par_px_cov33,par_px_cov44,par_px_cov55;
  vector<double> par_px_cov01,par_px_cov02,par_px_cov12,par_px_cov34,par_px_cov35,par_px_cov45;
  vector<double> par_px_cov03,par_px_cov04,par_px_cov05,par_px_cov13,par_px_cov14,par_px_cov15,par_px_cov23,par_px_cov24,par_px_cov25;

  vector<double> par_px, par_py, par_pz, par_pt, par_p, par_chg;
  vector<double> par_slope_covxx,par_slope_covyy,par_slope_covxy;//NB: slope values derivable from mom vec
  vector<double> par_gho, par_chi, par_clo;
  vector<int> par_typ;
  vector<int> par_PVind;
  vector<int> par_isback;

  //initialize the vectors for track<-->PV association
  std::vector<double> trkPV_pz, trkPV_gho;
  std::vector<int> trkPV_num;
  trkPVassociator(trkPV_pz,trkPV_gho,trkPV_num,"velo");

  LHCb::Tracks* tracks = get<LHCb::Tracks>(LHCb::TrackLocation::Default);
  typedef std::vector<const LHCb::Track*> TrackContainer ;
  int VEL_maxnum=500;

  // //clear the MC truth vectors and get the track->MC table
  // ClearTruthMatchedVectors();
  // LHCb::Track2MC2D* table2d  = get<LHCb::Track2MC2D>(LHCb::Track2MCLocation::Default);

  for( LHCb::Tracks::const_iterator it = tracks->begin() ; it != tracks->end(); ++it) {
    const Track* track=(*it);
    if (track->type()!=1 && track->type()!=2) continue;//skip tracks != Velo or VeloR

    // //fill the MC truth vectors 
    // fillTruthMatchedInfo(track,table2d);
    
    Gaudi::XYZPoint track_pos;
    Gaudi::XYZVector track_mom;
    Gaudi::SymMatrix6x6 track_posmom_cov;
    track->positionAndMomentum(track_pos,track_mom,track_posmom_cov);
    //pos e cov matrix
    par_pos_x.push_back(track_pos.x());
    par_pos_y.push_back(track_pos.y());
    par_pos_z.push_back(track_pos.z());
    
    const LHCb::State* fmState = track->stateAt( LHCb::State::FirstMeasurement );
    if (fmState){
      par_fmpos_x.push_back(fmState->x());
      par_fmpos_y.push_back(fmState->y());
      par_fmpos_z.push_back(fmState->z());
    }
    else{
      par_fmpos_x.push_back(0);
      par_fmpos_y.push_back(0);
      par_fmpos_z.push_back(0);
    }
    
    par_px_cov00.push_back(track_posmom_cov(0,0));
    par_px_cov11.push_back(track_posmom_cov(1,1));
    par_px_cov22.push_back(track_posmom_cov(2,2));
    par_px_cov33.push_back(track_posmom_cov(3,3));
    par_px_cov44.push_back(track_posmom_cov(4,4));
    par_px_cov55.push_back(track_posmom_cov(5,5));
    par_px_cov01.push_back(track_posmom_cov(0,1));
    par_px_cov02.push_back(track_posmom_cov(0,2));
    par_px_cov12.push_back(track_posmom_cov(1,2));
    par_px_cov34.push_back(track_posmom_cov(3,4));
    par_px_cov35.push_back(track_posmom_cov(3,5));
    par_px_cov45.push_back(track_posmom_cov(4,5));

    par_px_cov03.push_back(track_posmom_cov(0,3));
    par_px_cov04.push_back(track_posmom_cov(0,4));
    par_px_cov05.push_back(track_posmom_cov(0,5));
    par_px_cov13.push_back(track_posmom_cov(1,3));
    par_px_cov14.push_back(track_posmom_cov(1,4));
    par_px_cov15.push_back(track_posmom_cov(1,5));
    par_px_cov23.push_back(track_posmom_cov(2,3));
    par_px_cov24.push_back(track_posmom_cov(2,4));
    par_px_cov25.push_back(track_posmom_cov(2,5));

    //momentum and charge
    par_px.push_back(track_mom.x());
    par_py.push_back(track_mom.y());
    par_pz.push_back(track_mom.z());
    par_pt.push_back(track->pt());
    par_p.push_back(track->p());
    par_chg.push_back(track->charge());
    
    Gaudi::XYZVector track_slope;
    Gaudi::SymMatrix3x3 track_slope_cov;
    track->slopes(track_slope,track_slope_cov);
    par_slope_covxx.push_back(track_slope_cov(0,0));
    par_slope_covyy.push_back(track_slope_cov(1,1));
    par_slope_covxy.push_back(track_slope_cov(0,1));

    par_isback.push_back(track->checkFlag(LHCb::Track::Backward)?1:0);
    
    //other info
    par_typ.push_back(track->type());
    par_gho.push_back(track->ghostProbability());
    par_chi.push_back(track->chi2PerDoF());
    par_clo.push_back(track->info(LHCb::Track::CloneDist, 100));

    //PV association
    par_PVind.push_back(getAssociatedPV(track,trkPV_pz,trkPV_gho,trkPV_num));
  }
  
  test &= tuple->farray( "VEL_par_pos_x", par_pos_x.begin(), par_pos_x.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_pos_y", par_pos_y.begin(), par_pos_y.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_pos_z", par_pos_z.begin(), par_pos_z.end(), "VEL_num", VEL_maxnum );
  
  test &= tuple->farray( "VEL_par_fmpos_x", par_fmpos_x.begin(), par_fmpos_x.end(), "VEL_num", VEL_maxnum );//position at 'firstMeasurement'
  test &= tuple->farray( "VEL_par_fmpos_y", par_fmpos_y.begin(), par_fmpos_y.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_fmpos_z", par_fmpos_z.begin(), par_fmpos_z.end(), "VEL_num", VEL_maxnum );
  
  test &= tuple->farray( "VEL_par_xp_cov00", par_px_cov00.begin(), par_px_cov00.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov11", par_px_cov11.begin(), par_px_cov11.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov22", par_px_cov22.begin(), par_px_cov22.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov33", par_px_cov33.begin(), par_px_cov33.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov44", par_px_cov44.begin(), par_px_cov44.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov55", par_px_cov55.begin(), par_px_cov55.end(), "VEL_num", VEL_maxnum );

  test &= tuple->farray( "VEL_par_xp_cov01", par_px_cov01.begin(), par_px_cov01.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov02", par_px_cov02.begin(), par_px_cov02.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov12", par_px_cov12.begin(), par_px_cov12.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov34", par_px_cov34.begin(), par_px_cov34.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov35", par_px_cov35.begin(), par_px_cov35.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov45", par_px_cov45.begin(), par_px_cov45.end(), "VEL_num", VEL_maxnum );

  test &= tuple->farray( "VEL_par_xp_cov03", par_px_cov03.begin(), par_px_cov03.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov04", par_px_cov04.begin(), par_px_cov04.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov05", par_px_cov05.begin(), par_px_cov05.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov13", par_px_cov13.begin(), par_px_cov13.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov14", par_px_cov14.begin(), par_px_cov14.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov15", par_px_cov15.begin(), par_px_cov15.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov23", par_px_cov23.begin(), par_px_cov23.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov24", par_px_cov24.begin(), par_px_cov24.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_xp_cov25", par_px_cov25.begin(), par_px_cov25.end(), "VEL_num", VEL_maxnum );

  test &= tuple->farray( "VEL_par_px", par_px.begin(), par_px.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_py", par_py.begin(), par_py.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_pz", par_pz.begin(), par_pz.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_pt", par_pt.begin(), par_pt.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_p", par_p.begin(), par_p.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_chg", par_chg.begin(), par_chg.end(), "VEL_num", VEL_maxnum );

  test &= tuple->farray( "VEL_par_slope_covxx", par_slope_covxx.begin(), par_slope_covxx.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_slope_covyy", par_slope_covyy.begin(), par_slope_covyy.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_slope_covxy", par_slope_covxy.begin(), par_slope_covxy.end(), "VEL_num", VEL_maxnum );

  test &= tuple->farray( "VEL_par_back", par_isback.begin(), par_isback.end(), "VEL_num", VEL_maxnum );

  test &= tuple->farray( "VEL_par_typ", par_typ.begin(), par_typ.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_gho", par_gho.begin(), par_gho.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_chi", par_chi.begin(), par_chi.end(), "VEL_num", VEL_maxnum );
  test &= tuple->farray( "VEL_par_clo", par_clo.begin(), par_clo.end(), "VEL_num", VEL_maxnum );

  test &= tuple->farray( "VEL_par_PVind", par_PVind.begin(), par_PVind.end(), "VEL_num", VEL_maxnum );

  // //fill the truth-match info in the ntuple
  // if (m_isMC){
  //   fillTruthMatchedNtuple(tuple,"VEL",VEL_maxnum);
  // }
  if(test) return StatusCode::SUCCESS;
  else return StatusCode::FAILURE; 

}//end fillVeloTracksInfo



//=============================================================================
// IsHLTGood method,used by isolation calculation
//=============================================================================
void  TupleToolMuonVariables::IsHltGood(Gaudi::XYZPoint o,Gaudi::XYZVector p,
					Gaudi::XYZPoint o_mu,Gaudi::XYZVector 
					p_mu, Gaudi::XYZPoint PV, bool& hltgood,
					double& fc) {
  
  Gaudi::XYZVector rv;
  Gaudi::XYZPoint vtx;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;
  bool fail(false);

  closest_point(o,p,o_mu,p_mu,close,close_mu,vtx,fail);

  if (fail) {
    fc = -1.;
    hltgood = -1;
  }
  else {
    double pete = p.Rho();
    rv = vtx - PV;
    double DOCA_b = (close-close_mu).R();
    double ZZ = rv.z();
    fc = pointer(rv,p,p_mu);
    hltgood=( (DOCA_b<0.2) && (ZZ>0.) && (ZZ<30.) && (fc<0.4) && (pete>2.) );
  }
}


double TupleToolMuonVariables::pointer (Gaudi::XYZVector vertex,
					Gaudi::XYZVector p, Gaudi::XYZVector
					p_mu)  {
  double pt=p.Rho()+p_mu.Rho();
  Gaudi::XYZVector ptot(p+p_mu);
  double temp = arcosine(vertex,ptot);
  double  num=ptot.R()*sin(temp);
  double  den=(num+pt);
  double fc = num/den;
  return fc;
}






//=============================================================================
double TupleToolMuonVariables::ratio( double p1, double p2){  
  //std::cout<<":ratio"<<std::endl;
  return fabs(p1 -p2)*(1./fabs(p1+p2)); 
}


//=============================================================================
void TupleToolMuonVariables::closest_point(Gaudi::XYZPoint o,Gaudi::XYZVector p,
					   Gaudi::XYZPoint o_mu,Gaudi::XYZVector p_mu, 
					   Gaudi::XYZPoint& close1, 
					   Gaudi::XYZPoint& close2, 
					   Gaudi::XYZPoint& vertex, bool& fail) {
  
  
  Gaudi::XYZVector v0(o - o_mu);
  Gaudi::XYZVector v1(p.unit());
  Gaudi::XYZVector v2(p_mu.unit());
  Gaudi::XYZPoint temp1(0.,0.,0.);
  Gaudi::XYZPoint temp2(0.,0.,0.);
  fail = false;
  double  d02 = v0.Dot(v2); 
  double  d21 = v2.Dot(v1); 
  double  d01 = v0.Dot(v1);
  double  d22 = v2.Dot(v2);
  double  d11 = v1.Dot(v1); 
  double  denom = d11 * d22 - d21 * d21; 
  if (fabs(denom) <= 0.) {
    close1 = temp1;
    close2 = temp2;
    fail = true;
  }
  else {
    double numer = d02 * d21 - d01 * d22; 
    double mu1 = numer / denom;            
    double mu2 = (d02 + d21 * mu1) / d22; 
    close1 = o+v1*mu1;
    close2 = o_mu+v2*mu2;
  }
  vertex = (close1+(close2-close1)*0.5);
}



double TupleToolMuonVariables::arcosine(Gaudi::XYZVector p1,Gaudi::XYZVector p2) {
  
  double num    = (p1.Cross(p2)).R();
  double den    = p1.R()*p2.R();
  double seno   = num/den;
  double coseno = p1.Dot(p2)/den;
  double alpha  = asin(fabs(seno));
  if (coseno < 0 ) {
    alpha = ROOT::Math::Pi() - alpha;
  }
  //std::cout<<"closest_point"<<std::endl;
  return alpha;
}



void TupleToolMuonVariables::InCone(Gaudi::XYZPoint o1, 
				    Gaudi::XYZVector p1,Gaudi::XYZPoint o2, 
				    Gaudi::XYZVector p2,
				    Gaudi::XYZPoint& vtx, double& 
				    doca, double& angle){
  
  Gaudi::XYZPoint rv;
  Gaudi::XYZPoint close;
  Gaudi::XYZPoint close_mu;
  bool fail(false);
  //std::cout<<"InCone"<<std::endl;
  closest_point(o1,p1,o2,p2,close,close_mu,vtx, fail);
  if (fail) {
    doca =-1.;
    angle=-1.;
  } 
  else {  
    doca = (close-close_mu).R();
    angle = arcosine(p1,p2);
  }
}

bool TupleToolMuonVariables::isNonIso(const LHCb::MCParticle* mcp,TString mu_flag){
  //fill the mcp decay vec
  std::vector<int> decay_vec;
  get_decayTree(mcp,decay_vec);
  //fill the muon decay vec
  std::vector<int> muon_vec;
  if (mu_flag=="mu+") muon_vec=_mup_mcdecaytree_vec;
  if (mu_flag=="mu-") muon_vec=_mum_mcdecaytree_vec;
  //check if the two vectors have common entries (non-iso condition)
  for (int i=0;i<(int) decay_vec.size();i++)
    for (int j=0;j<(int) muon_vec.size();j++) if (decay_vec[i]==muon_vec[j]) return true;
  
  return false;

}//end isNonIso
