// $Id: TupleToolGamma.h
#ifndef _TUPLETOOLGAMMA_H
#define _TUPLETOOLGAMMA_H 1

// Include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "TMVA/Reader.h"
#include "TMVA/IMethod.h"
#include "Kernel/IParticleTupleTool.h"
#include "Kernel/IParticleVeto.h"
#include "Event/ProtoParticle.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include <memory>

//============================================================================

class TupleToolGamma : public TupleToolBase, virtual public IParticleTupleTool 
{
  
public:

  /// Standard constructor
  TupleToolGamma( const std::string& type,
                  const std::string& name,
                  const IInterface* parent );

  /// Initialise
  StatusCode initialize() override;

  /// Destructor
  virtual ~TupleToolGamma( ){} 

public:

  virtual StatusCode fill( const LHCb::Particle*, 
                           const LHCb::Particle*,
                           const std::string&,
	                   Tuples::Tuple& ) override;
private:

  TMVA::Reader *reader;
  bool filltuple;
  float isNotH;
  float isNotE;
  float Ecl_log;
  float Fr2_log;
  float PrsFr2;
  float Fr2r4;
  float Asym;
  float PrsAsym;
  float Kappa;
  float Eseed;
  float E2;
  float PrsM;
  float PrsM15;
  float PrsM30;
  float PrsM45;
  float PrsEmax;
  float PrsE2;
  float PE_log;
  float PT_log;
  float n_pi0_f;
  float n_eta_f;
  float mean_dm_pi0_f;
  float mean_dm_eta_f;
  float dm_pi0;
  float dm_eta;
  // IParticleVeto* m_check;
  std::string m_BDTFileName;
  int m_PhotonID;
  std::map<std::string,std::vector<std::string> > m_veto;
  ICaloHypoEstimator* m_estimator;

  std::vector<const LHCb::Particle*> getTree(const LHCb::Particle* P);  


};

#endif // _TUPLETOOLGamma_H
