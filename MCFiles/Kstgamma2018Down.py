from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2018"
dv.InputType = 'DST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20190128-vc-md100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Kstgamma_2018_Down.root"
