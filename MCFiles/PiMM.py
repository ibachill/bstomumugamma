from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = 1000#-1
dv.DataType = "2016"
dv.InputType = 'DST'

#DecFile 11112401
#Decay Bd0 -> pi0 mu mu
#Mag UP

dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/11112401_2016_Up.dst'
    ]

dv.Lumi = not DaVinci().Simulation

# MC Production Conditions; can be find with
#  > export PATH=$PATH:/afs/cern.ch/user/i/ibelyaev/public/scripts
#  > lhcb-proxy-init
#  > get_bookkeeping_info 13142201

dv.CondDBtag = 'sim-20161124-2-vc-mu100'
dv.DDDBtag = 'dddb-20150724'

# dv.CondDBtag = 'sim-20130522-1-vc-mu100'
# dv.DDDBtag = 'dddb-20130929-1'

dv.TupleFile = "Ntuple_PiMM_Up.root"
