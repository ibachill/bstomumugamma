from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2012"
dv.InputType = 'DST'
#Mag UP
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20130522-1-vc-mu100'
dv.DDDBtag = 'dddb-20130929-1'

dv.TupleFile = "Ntuple_Signal.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
