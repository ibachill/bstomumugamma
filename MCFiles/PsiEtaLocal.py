from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2012"
dv.InputType = 'DST'
#DecFile 13142401
#Decay Bs0 -> Psi (Jpsi X) eta

dv.Input = [
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526165100.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194099.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194100.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194101.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194102.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194103.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194104.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194105.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194106.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194107.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194108.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194109.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194110.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194111.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194112.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194113.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194114.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194115.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194116.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194117.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194118.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194119.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194120.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194121.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194122.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194123.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194124.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194125.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194126.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194127.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194128.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194129.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194130.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194131.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194132.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194133.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194134.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194135.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194136.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194137.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194138.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194139.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194140.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194141.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194142.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194143.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194144.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194145.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194146.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194147.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194148.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194149.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194150.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194151.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194152.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194153.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194154.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194155.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194156.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194157.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194158.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194159.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194160.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194161.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194162.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194163.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194164.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194165.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194166.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194167.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194168.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194169.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194170.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194171.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194172.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194173.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194174.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194175.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194176.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194177.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194178.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194179.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194180.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194181.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194182.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194183.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194184.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194185.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194186.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194187.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194188.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194189.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194190.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194191.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194192.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194193.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194194.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194195.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194196.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194197.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagDown/50evts_s21_526194198.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194000.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194001.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194002.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194003.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194004.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194005.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194006.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194007.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194008.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194009.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194010.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194011.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194012.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194013.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194014.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194015.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194016.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194017.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194018.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194019.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194020.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194021.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194022.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194023.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194024.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194025.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194026.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194027.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194028.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194029.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194030.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194031.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194032.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194033.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194034.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194035.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194036.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194037.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194038.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194039.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194040.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194041.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194042.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194043.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194044.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194045.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194046.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194047.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194048.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194049.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194050.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194051.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194052.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194053.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194054.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194055.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194056.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194057.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194058.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194059.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194060.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194061.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194062.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194063.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194064.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194065.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194066.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194067.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194068.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194069.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194070.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194071.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194072.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194073.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194074.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194075.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194076.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194077.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194078.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194079.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194080.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194081.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194082.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194084.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194085.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194086.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194087.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194088.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194089.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194090.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194091.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194092.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194093.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194094.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194095.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194096.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194097.dst",
"PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/SimulationJobs/13944401/2012/Sim09c/MagUp/50evts_s21_526194098.dst"
    ]

dv.Lumi = not DaVinci().Simulation

dv.CondDBtag = "sim-20160321-2-vc-md100"
dv.DDDBtag = "dddb-20170721-2"

dv.TupleFile = "PsiEta_Down_2012.root"

