j = Job(name='MyDaVinciNew')
j.comment = 'Bs -> mu mu Up'
myApp = GaudiExec()
myApp.directory = "~/cmtuser/DaVinciDev_v41r3"
j.application = myApp
j.application.options = ['MMgrid.py', '../DecOptions/DecOpt.py']
j.inputfiles += ['../Weights/GammaMVA_BDT.weights.xml']

data=BKQuery('/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13112001/ALLSTREAMS.DST', dqflag=['OK']).getDataset()
j.inputdata = data[0:100]

j.splitter = SplitByFiles(
	filesPerJob=5,
	ignoremissing=True
	)
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
