from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = 10000#-1
dv.DataType = "2018"
dv.InputType = 'DST'
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/13102202_2018_Down.dst'
    ]
dv.Lumi = not dv.Simulation

dv.CondDBtag = 'sim-20190128-vc-md100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Ntuple_PhiGamma.root"
