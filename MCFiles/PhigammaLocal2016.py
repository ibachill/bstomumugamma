from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = 1000#-1
dv.DataType = "2016"
dv.InputType = 'DST'
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/13102202_2016_Down.dst'
    ]
dv.Lumi = not dv.Simulation

dv.CondDBtag = 'sim-20170721-2-vc-md100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Ntuple_PhiGamma.root"
