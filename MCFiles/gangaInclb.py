j = Job(name='MyDaVinciNew')
j.comment = 'Inclusive b2mumu Mag Up'
myApp = GaudiExec()
myApp.directory = "~/cmtuser/DaVinciDev_v41r3"
j.application = myApp
j.application.options = ['Inclbgrid.py', '../DecOptions/DecOpt.py']
j.inputfiles += ['../Weights/GammaMVA_BDT.weights.xml']

data=BKQuery('/MC/2012/Beam4000GeV-MayJune2012-MagUp-Nu2.5-EmNoCuts/Sim06b/Trig0x4097003dFlagged/Reco14/Stripping20Filtered/10012009/BS2MUMU.STRIP.DST', dqflag=['OK']).getDataset()

j.inputdata = data[0:100]

j.splitter = SplitByFiles(filesPerJob=1,ignoremissing=True)
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
