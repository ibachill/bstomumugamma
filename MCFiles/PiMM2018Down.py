from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2018"
dv.InputType = 'DST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20190430-vc-md100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "PiMM_2018_Up.root"

