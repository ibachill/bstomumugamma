from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = 10000#-1
dv.DataType = "2012"
dv.InputType = 'DST'
#DecFile 13142214 (!!not 100% sure...)
#Decay Bs0 -> (etap -> rho gamma) (Psi -> mu mu)
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/GMM/00027383_00000002_1.allstreams.dst'
    ]
dv.Lumi = not DaVinci().Simulation
# MC Production Conditions; can be find with
#  > export PATH=$PATH:/afs/cern.ch/user/i/ibelyaev/public/scripts
#  > lhcb-proxy-init
#  > get_bookkeeping_info 13142201
dv.CondDBtag = 'Sim08-20130503-1-vc-mu100'
dv.DDDBtag = 'Sim08-20130503-1'

dv.TupleFile = "Ntuple_Psietap_Up.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]

