j = Job(name='MyDaVinciNew')
j.comment = 'Bd -> K pi gamma Mag Up'
myApp = GaudiExec()
myApp.directory = "~/cmtuser/DaVinciDev_v41r3"
j.application = myApp
j.application.options = ['GKpigrid.py', '../DecOptions/GKpiDecOpt.py']
j.inputfiles += ['../Weights/GammaMVA_BDT.weights.xml']

data=BKQuery('/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08d/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/11102262/ALLSTREAMS.DST', dqflag=['OK']).getDataset()
j.inputdata = data[0:100]

j.splitter = SplitByFiles(filesPerJob=1,ignoremissing=True)
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
