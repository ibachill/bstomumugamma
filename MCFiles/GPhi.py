from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2012"
dv.InputType = 'DST'
#DecFile 13102201
#Decay Bs0 -> gamma (Phi -> K K)
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/13102201_Down.dst'
    ]
dv.Lumi = not DaVinci().Simulation
dv.CondDBtag = 'sim-20121025-vc-mu100'
dv.DDDBtag = 'dddb-20120831'

dv.TupleFile = "Ntuple_GPhi.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]

