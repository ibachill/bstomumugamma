from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2017"
dv.InputType = 'DST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20180411-vc-mu100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Phigamma_2017_Up.root"
