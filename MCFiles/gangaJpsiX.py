import argparse
parser = argparse.ArgumentParser(description="Ganga job for Data")

parser.add_argument('year', type = int,
                    choices = [2011, 2012, 2015, 2016, 2017],
                    help = 'Year of data-taking to run over')

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')





args = parser.parse_args()

year = args.year
polarity = args.polarity



j = Job(name='MyDaVinciNew')

j.name = 'Bs2JpsiX_'+polarity+'_'+str(year)
j.comment = 'Bs2JpsiX '+polarity+' '+str(year)

myApp = GaudiExec()
myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v41r3"

j.application = myApp
j.application.options = ['JpsiX'+str(year)+polarity+'.py']
j.application.platform = 'x86_64-slc6-gcc49-opt'
j.application.options += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DecOptions/Run2NormDecOpt.py']
j.inputfiles += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/Weights/GammaMVA_BDT.weights.xml']

if year==2012:
    data=BKQuery(
        '/MC/2012/Beam4000GeV-2012-Mag'+polarity+'-Nu2.5-Pythia8/Sim08i/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13442012/ALLSTREAMS.DST',
        dqflag=['OK']).getDataset()
elif year==2016:
    data = BKQuery(
        '/MC/2016/Beam6500GeV-2016-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/13442001/ALLSTREAMS.DST',
        dqflag=['OK']).getDataset()


j.inputdata = data

j.splitter = SplitByFiles(filesPerJob=20,ignoremissing=True)
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
