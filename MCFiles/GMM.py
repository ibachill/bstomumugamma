from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = 10000#-1
dv.DataType = "2012"
dv.InputType = 'DST'
#DecFile 13112201
#Decay Bs0 -> gamma mu mu
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/GMM/00043888_00000071_2.AllStreams.dst'
    ]
dv.Lumi = not DaVinci().Simulation
# MC Production Conditions; can be find with
#  > export PATH=$PATH:/afs/cern.ch/user/i/ibelyaev/public/scripts
#  > lhcb-proxy-init
#  > get_bookkeeping_info 13142201
dv.CondDBtag = 'sim-20130522-1-vc-mu100'
dv.DDDBtag = 'dddb-20130929-1'

dv.TupleFile = "Ntuple_GMM_Up.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
