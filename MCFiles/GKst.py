from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2012"
dv.InputType = 'DST'
#DecFile 11102201
#Decay Bd0 -> gamma Kst
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/GMM/00021616_00000026_1.allstreams.dst'
    ]
dv.Lumi = not DaVinci().Simulation
# MC Production Conditions; can be find with
#  > export PATH=$PATH:/afs/cern.ch/user/i/ibelyaev/public/scripts
#  > lhcb-proxy-init
#  > get_bookkeeping_info 13142201
dv.CondDBtag = 'sim-20121025-vc-mu100'
dv.DDDBtag = 'dddb-20120831'

dv.TupleFile = "Ntuple_GKst_Up.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
