from Configurables import (
    DaVinci,
    DecayTreeTuple,
    MCMatchObjP2MCRelator
)
from DecayTreeTuple import Configuration

# Work around for Turbo locations being included in the default list of
# relations table locations, which triggers Turbo unpacking and seg. faults
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc] 

dtt = DecayTreeTuple('TupleB0ToJpsiKstar')
dtt.Inputs = ['Phys/B2XMuMu_Line/Particles']
dtt.Decay = '[B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(K*(892)0 -> ^K+ ^pi-)]CC'
dtt.ToolList = [
    'TupleToolKinematic',
    'TupleToolEventInfo'
]

# Add MC truth information
dtt.ToolList += [
    'TupleToolMCBackgroundInfo'
]
# List of MC truth tools you want (can be empty)
mc_tools = [
    'MCTupleToolKinematic'
]
mctruth = dtt.addTupleTool('TupleToolMCTruth')
mctruth.ToolList = mc_tools
mctruth.addTool(MCMatchObjP2MCRelator)
mctruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs 

DaVinci().DataType = '2016'
DaVinci().InputType = 'MDST'
DaVinci().RootInTES = '/Event/B2XMuMu.Strip'
DaVinci().Simulation = True
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().UserAlgorithms = [dtt]
DaVinci().EvtMax = 1000
