from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2017"
dv.InputType = 'DST'

dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145401.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145402.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145403.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145404.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145405.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145406.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145407.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145408.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145409.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145410.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145411.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145412.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145413.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145414.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145415.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145416.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145417.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145418.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145419.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145420.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145421.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145422.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145423.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145424.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145425.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145426.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145427.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145428.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145429.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145430.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145431.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145432.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145433.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145434.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145435.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145436.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145437.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145438.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145439.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145440.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145441.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145442.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145443.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145444.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145445.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145446.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145447.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145448.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145449.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145450.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145451.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145452.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145453.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145454.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145455.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145456.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145457.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145458.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145459.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145460.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145461.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145462.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145463.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145464.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145465.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145466.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145467.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145468.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145469.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145470.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145471.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145472.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145473.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145474.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145475.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145476.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145477.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145478.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145479.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145480.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145481.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145482.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145483.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145484.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145485.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145486.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145487.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145488.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145489.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145490.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145491.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145492.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145493.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145494.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145495.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145496.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145497.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145498.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145499.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagDown/50evts_s29r2_624145500.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145301.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145302.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145303.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145304.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145305.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145306.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145307.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145308.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145309.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145310.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145311.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145312.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145313.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145314.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145315.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145316.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145317.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145318.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145319.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145320.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145321.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145322.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145323.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145324.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145325.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145326.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145327.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145328.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145329.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145330.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145331.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145332.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145333.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145334.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145335.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145336.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145337.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145338.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145339.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145340.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145341.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145342.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145343.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145344.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145345.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145346.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145347.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145348.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145349.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145350.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145351.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145352.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145353.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145354.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145355.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145356.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145357.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145358.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145359.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145360.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145361.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145362.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145363.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145364.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145365.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145366.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145367.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145368.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145369.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145370.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145371.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145372.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145373.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145374.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145375.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145376.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145377.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145378.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145379.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145380.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145381.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145382.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145383.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145384.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145385.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145386.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145387.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145388.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145389.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145390.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145391.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145392.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145393.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145394.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145395.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145396.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145397.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145398.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145399.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud//SimulationJobs/13112207/2017/Sim09h/MagUp/50evts_s29r2_624145400.dst',
    ]

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20190430-1-vc-md100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Signal_BinII_2017.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
