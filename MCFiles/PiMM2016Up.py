from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2016"
dv.InputType = 'DST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20161124-2-vc-mu100'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "PiMM_2016_Up.root"

