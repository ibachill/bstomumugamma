from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2018"
dv.InputType = 'MDST'
#Mag UP
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20190430-vc-md100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Ntuple_Signal.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
