j = Job(name='MyDaVinciNew')
j.comment = 'Bs -> Psi etap Mag Up'
myApp = GaudiExec()
myApp.directory = "~/cmtuser/DaVinciDev_v41r3"
j.application = myApp
j.application.options = ['Psietapgrid.py', '../DecOptions/DecOpt.py']
j.inputfiles += ['../Weights/GammaMVA_BDT.weights.xml']

data=BKQuery('/MC/2012/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13142214/ALLSTREAMS.DST', dqflag=['OK']).getDataset()

j.inputdata = data

j.splitter = SplitByFiles(filesPerJob=1,ignoremissing=True)
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
