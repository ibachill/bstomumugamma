import argparse
parser = argparse.ArgumentParser(description="Ganga job for Data")

parser.add_argument('year', type = int,
                    choices = [2011, 2012, 2015, 2016, 2017],
                    help = 'Year of data-taking to run over')

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')





args = parser.parse_args()

year = args.year
polarity = args.polarity



j = Job(name='MyDaVinciNew')

j.name = 'BuKstMM_'+polarity+'_'+str(year)
j.comment = 'BuKstMM '+polarity+' '+str(year)
myApp = GaudiExec()
myApp.directory = "~/cmtuser/DaVinciDev_v41r3"
j.application = myApp
j.application.platform = 'x86_64-slc6-gcc49-opt'
j.application.options = ['BuKstMM'+str(year)+polarity+'.py', '~/Bs2mumugamma/DecOptions/DecOpt.py']
j.inputfiles += ['~/Bs2mumugamma/Weights/GammaMVA_BDT.weights.xml']

if year==2011:
    data=BKQuery(
        '/MC/2011/Beam3500GeV-2011-Mag'+polarity+'-Nu2-Pythia8/Sim09a/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/12113100/ALLSTREAMS.DST',
        dqflag=['OK']).getDataset()
elif year==2012:
    data=BKQuery(
        '/MC/2012/Beam4000GeV-2012-Mag'+polarity+'-Nu2.5-Pythia8/Sim09a/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/12113100/ALLSTREAMS.DST',
        dqflag=['OK']).getDataset()


j.inputdata = data

j.splitter = SplitByFiles(filesPerJob=20,ignoremissing=True)
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
