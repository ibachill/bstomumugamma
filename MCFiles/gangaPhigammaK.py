import argparse
parser = argparse.ArgumentParser(description="Ganga job for GPhiK")





parser.add_argument('year', type = int,
                    choices = [2011, 2012, 2015, 2016, 2017, 2018],
                    help = 'Year of data-taking to run over')

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')

args = parser.parse_args()

year = args.year
polarity = args.polarity



j = Job()

j.name = 'Bu2PhigammaK_'+polarity+'_'+str(year)
j.comment = 'Bu2PhigammaK '+polarity+' '+str(year)

myApp = GaudiExec()
if year < 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v41r4p5"
    j.application = myApp
    j.application.platform = 'x86_64-slc6-gcc49-opt'
elif year == 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v42r8p1"
    j.application = myApp
    j.application.platform = 'x86_64-slc6-gcc62-opt'
elif year == 2018:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v44r9"
    j.application = myApp
    j.application.platform = 'x86_64-slc6-gcc62-opt'


j.application.options = ['PhigammaK'+str(year)+polarity+'.py']
j.application.options += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DecOptions/MCGPhiDecOpt.py']
j.inputfiles += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/Weights/GammaMVA_BDT.weights.xml']


if year==2016:
    data = BKQuery(
        '/MC/2016/Beam6500GeV-2016-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/12103202/LEPTONICRADIATIVE.STRIP.MDST',
        dqflag=['OK']).getDataset()


j.inputdata = data[0:1]
j.name += 'TEST'
j.comment += ' TEST'

j.splitter = SplitByFiles(
    filesPerJob = 50,
    ignoremissing=False
    )
j.outputfiles += [DiracFile('*.root')]
#j.backend = Dirac()
j.backend = Local()
j.submit()
