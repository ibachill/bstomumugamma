from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = 1000#-1
dv.DataType = "2012"
dv.InputType = 'DST'
#DecFile 10012009
#Decay Incl(b -> mu mu)
#Mag UP
dv.Input = [
#    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/GMM/00022063_00000001_1.bs2mumu.strip.dst'
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/10012013.dst'
    ]
dv.Lumi = not DaVinci().Simulation
# MC Production Conditions; can be find with
#  > export PATH=$PATH:/afs/cern.ch/user/i/ibelyaev/public/scripts
#  > lhcb-proxy-init
#  > get_bookkeeping_info 13142201
dv.CondDBtag = 'sim-20121025-vc-mu100'
dv.DDDBtag = 'dddb-20120831'

dv.TupleFile = "Ntuple_Inclb_Up.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
