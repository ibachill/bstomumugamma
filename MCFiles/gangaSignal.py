import argparse
parser = argparse.ArgumentParser(description="Ganga job for Signal")

parser.add_argument('year', type = int,
                    choices = [2011, 2012, 2015, 2016, 2017, 2018],
                    help = 'Year of data-taking to run over')

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')





args = parser.parse_args()

year = args.year
polarity = args.polarity



j = Job()

j.name = 'Signal_'+polarity+'_'+str(year)
j.comment = 'Signal '+polarity+' '+str(year)

myApp = GaudiExec()
if year < 2014:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v42r7p1"
    j.application = myApp

    # myApp = prepareGaudiExec('DaVinci','v42r7p3', myPath='~/cmtuser/')
elif year < 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v41r4p5"
    j.application = myApp
    j.application.platform = 'x86_64-slc6-gcc49-opt'
elif year == 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v42r8p1"
    j.application = myApp
    j.application.platform = 'x86_64-centos7-gcc62-opt'
elif year == 2018:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v45r1"
    j.application = myApp
    j.application.platform = 'x86_64-centos7-gcc8-opt'


j.application.options = ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/MCFiles/Signal'+str(year)+polarity+'.py']
if year < 2014:
    j.application.options += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DecOptions/Run1DecOpt.py']
    j.inputfiles += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/Weights/GammaMVA_BDT.weights.xml']
else:
    j.application.options += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DecOptions/Run2DecOpt.py']


if year==2012:
    data = BKQuery(
        '/MC/2012/Beam4000GeV-2012-Mag'+polarity+'-Nu2.5-Pythia8/Sim08g/Digi13/Trig0x409f0045/Reco14c/Stripping20NoPrescalingFlagged/13112203/ALLSTREAMS.DST',
        dqflag = ['OK']).getDataset()
elif year==2011:
    data = BKQuery(
        '/MC/2011/Beam3500GeV-2011-Mag'+polarity+'-Nu2-Pythia8/Sim09b/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/13112203/ALLSTREAMS.DST',
        dqflag = ['OK']).getDataset()
elif year==2015:
    data = BKQuery(
        '/MC/2015/Beam6500GeV-2015-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1p1Filtered/13112203/BS2MUMUGAMMA.STRIP.MDST',
        dqflag = ['OK']).getDataset()
elif year==2016:
    data = BKQuery(
        '/MC/2016/Beam6500GeV-2016-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1p1Filtered/13112203/BS2MUMUGAMMA.STRIP.MDST',
        dqflag = ['OK']).getDataset()
elif year==2017:
    data = BKQuery(
        '/MC/2017/Beam6500GeV-2017-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/13112203/BS2MUMUGAMMA.STRIP.MDST',
        dqflag = ['OK']).getDataset()
elif year==2018:
    data = BKQuery(
        '/MC/2018/Beam6500GeV-2018-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered/13112203/BS2MUMUGAMMA.STRIP.MDST',
        dqflag = ['OK']).getDataset()
    





j.inputdata = data

j.splitter = SplitByFiles(
    filesPerJob=50,
    ignoremissing=False
    )
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
