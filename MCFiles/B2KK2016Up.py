from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2016"
dv.InputType = 'DST'

dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20170721-2-vc-mu100'
dv.DDDBtag = 'dddb-20170721-3'



dv.TupleFile = "B2KK_2016_Up.root"
