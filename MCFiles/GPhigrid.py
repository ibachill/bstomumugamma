from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2012"
dv.InputType = 'DST'
#Mag UP
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20121025-vc-mu100'
dv.DDDBtag = 'dddb-20120831'

dv.TupleFile = "Ntuple_GPhi_Up.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
