from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2016"
dv.InputType = 'DST'
#Mag UP
dv.Input = [
    # 'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/13142401_2016_Down.dst'
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/00065040_00000282_7.AllStreams.dst'
    ]
dv.Lumi = not dv.Simulation

dv.CondDBtag = 'sim-20161124-2-vc-md100'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "Ntuple_Norm.root"
