from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2012"
dv.InputType = 'DST'
#Mag UP
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'Sim08-20130503-1-vc-mu100'
dv.DDDBtag = 'Sim08-20130503-1'

dv.TupleFile = "Ntuple_Psietap_Up.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]

