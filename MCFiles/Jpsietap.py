from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2012"
dv.InputType = 'DST'
#DecFile 13344402
#Decay Bs0 -> eta' (Jpsi -> mu mu)
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/00025865_00000004_1.allstreams.dst'
    ]
dv.Lumi = not DaVinci().Simulation
# MC Production Conditions; can be find with
#  > export PATH=$PATH:/afs/cern.ch/user/i/ibelyaev/public/scripts
#  > lhcb-proxy-init
#  > get_bookkeeping_info 13142201
dv.CondDBtag = 'Sim08-20130503-1-vc-mu100'
dv.DDDBtag = 'Sim08-20130503-1'

dv.TupleFile = "Data_Up_2012.root"

