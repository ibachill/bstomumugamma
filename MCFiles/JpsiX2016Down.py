from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2016"
dv.InputType = 'DST'
#Mag UP
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20161124-2-vc-md100'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "JpsiX_Down_2016.root"
