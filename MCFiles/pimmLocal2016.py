from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = -1
dv.DataType = "2016"
dv.InputType = 'DST'
#Mag UP
dv.Input = [
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/11112401_2016_Up.dst'
    ]
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20161124-2-vc-mu100'
dv.DDDBtag = 'dddb-20150724'

dv.TupleFile = "Ntuple_PiMM_2016.root"

# seq = GaudiSequencer('Bs0_Tuple_seq')
# dv.UserAlgorithms = [seq]
