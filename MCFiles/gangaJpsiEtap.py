import argparse
parser = argparse.ArgumentParser(description="Ganga job for Data")

parser.add_argument('year', type = int,
                    choices = [2011, 2012, 2015, 2016, 2017],
                    help = 'Year of data-taking to run over')

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')


args = parser.parse_args()

year = args.year
polarity = args.polarity






j = Job(name='MyDaVinciNew')

j.name = 'Bs2JpsiEtap_'+polarity+'_'+str(year)
j.comment = 'Bs2JpsiEtap '+polarity+' '+str(year)
myApp = GaudiExec()
myApp.directory = "~/cmtuser/DaVinciDev_v41r3"
j.application = myApp
j.application.platform = 'x86_64-slc6-gcc49-opt'
j.application.options = ['JpsiEtap'+str(year)+polarity+'.py', '../DecOptions/Run1DecOpt.py']
j.inputfiles += ['../Weights/GammaMVA_BDT.weights.xml']

data=BKQuery(
    '/MC/2012/Beam4000GeV-2012-Mag'+polarity+'-Nu2.5-Pythia8/Sim08a/Digi13/Trig0x409f0045/Reco14a/Stripping20NoPrescalingFlagged/13344402/ALLSTREAMS.DST',
    dqflag=['OK']).getDataset()

j.inputdata = data

j.splitter = SplitByFiles(
    filesPerJob=10,
    ignoremissing=True
    )
j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.submit()
