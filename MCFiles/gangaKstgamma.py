import argparse
parser = argparse.ArgumentParser(description="Ganga job for Kst gamma")





parser.add_argument('year', type = int,
                    choices = [2011, 2012, 2015, 2016, 2017, 2018],
                    help = 'Year of data-taking to run over')

parser.add_argument('polarity',
                    choices = ['Up', 'Down'],
                    help = 'Polarity of data-taking to run over')

args = parser.parse_args()

year = args.year
polarity = args.polarity



j = Job()

j.name = 'B2Kstgamma_'+polarity+'_'+str(year)
j.comment = 'B2Kst(Kpi)gamma '+polarity+' '+str(year)

myApp = GaudiExec()

if year < 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v41r4p5"
    j.application = myApp
    j.application.platform = 'x86_64-slc6-gcc49-opt'
elif year == 2017:
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v42r8p1"
    j.application = myApp
    j.application.platform = 'x86_64-slc6-gcc62-opt'
elif year == 2018:
    # myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v44r9"
    # j.application = myApp
    # j.application.platform = 'x86_64-slc6-gcc62-opt'
    myApp.directory = "/afs/cern.ch/user/m/mreboud/cmtuser/DaVinciDev_v45r1"
    j.application = myApp
    j.application.platform = 'x86_64-centos7-gcc8-opt'

j.application.options = ['Kstgamma'+str(year)+polarity+'.py']
j.application.options += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/DecOptions/GKstDecOpt.py']
j.inputfiles += ['/afs/cern.ch/user/m/mreboud/Bs2mumugamma/Weights/GammaMVA_BDT.weights.xml']




if year==2016:
    data = BKQuery(
        '/MC/2016/Beam6500GeV-2016-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping28NoPrescalingFlagged/11102201/ALLSTREAMS.DST',
        dqflag=['OK']).getDataset()
elif year==2017:
    data = BKQuery(
        '/MC/2017/Beam6500GeV-2017-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09f/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/11102202/ALLSTREAMS.DST',
        dqflag=['OK']).getDataset()
elif year==2018:
    data = BKQuery(
        '/MC/2018/Beam6500GeV-2018-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/11102202/ALLSTREAMS.DST',
        dqflag=['OK']).getDataset()

    
j.inputdata = data

j.splitter = SplitByFiles(
    filesPerJob = 50,
    ignoremissing=False
)

j.outputfiles += [DiracFile('*.root')]
j.backend = Dirac()
j.backend.settings['BannedSites'] = ['LCG.NCBJ.pl','LCG.UKI-LT2-IC-HEP.uk']
j.submit()
