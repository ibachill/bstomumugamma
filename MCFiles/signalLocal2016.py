from Configurables import DaVinci, GaudiSequencer

dv = DaVinci()

dv.Simulation = True
dv.EvtMax = 1000#-1
dv.DataType = "2016"
dv.InputType = 'mDST'
#Mag UP
dv.Input = [
#    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/00075127_00000011_1.bs2mumugamma.strip.mdst'
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/user/m/mreboud/13112203_2016_Up.mdst'
    ]
dv.Lumi = not dv.Simulation
dv.CondDBtag = 'sim-20170721-2-vc-mu100'
dv.DDDBtag = 'dddb-20170721-3'

dv.TupleFile = "Ntuple_Signal_10000.root"

seq = GaudiSequencer('Bs0_Tuple_seq')
dv.UserAlgorithms = [seq]
