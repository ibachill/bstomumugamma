#  $`B_s \rightarrow \mu \mu \gamma`$

The scripts for the ntuple production are stored in _MCFiles/_ and _DataFiles/_ and use the option files stored in _DecOptions/_. 

 - E.g. for Signal MC 2018 Down, do `ganga gangaSignal.py Down 2018`

 - For the Data, events are separated in clusters, for the 5000 firsts Up 2018 files, do `ganga gangaData.py Up 2018 0`

Normalization and Control channels are obtained the same way in separated Ntuples

TupleTools are in _Tools/_, they shuold be placed in _DaVinciDev_v45r8/Phys/DecayTreeTuple/src/_
